﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace iHos.MVC.Converter
{
    public static class IFilter
    {
        public static IQueryable<T> Proses<T>(IQueryable<T> proses, object filter)
        {
            foreach (var prop in filter.GetType().GetProperties())
            {
                var val = prop.GetValue(filter);
                var at = prop.GetCustomAttributes(typeof(CustomFilterAttribute), true);

                var pp = prop.GetCustomAttributes(typeof(ColumnAttribute), true);
                var propName = pp.Length > 0 ?
                    ((ColumnAttribute)pp.First()).Name : prop.Name;

                if (at.Length > 0)
                {
                    if (prop.PropertyType == typeof(string))
                        if (String.IsNullOrEmpty((string)val)) continue;
                    else
                        if (val == null) continue;
                    switch (((CustomFilterAttribute)at.First()).Comparison)
                    {
                        case IEnum.Comparison.Equals:
                            proses = proses.Where($"{propName} = @0", val);
                            break;
                        case IEnum.Comparison.GreaterThan:
                            proses = proses.Where($"{propName} > @0", val);
                            break;
                        case IEnum.Comparison.LessThan:
                            proses = proses.Where($"{propName} < @0", val);
                            break;
                        case IEnum.Comparison.GreaterThanOrEqualTo:
                            proses = proses.Where($"{propName} >= @0", val);
                            break;
                        case IEnum.Comparison.LessThanOrEqualTo:
                            proses = proses.Where($"{propName} <= @0", val);
                            break;
                        case IEnum.Comparison.NotEqualTo:
                            proses = proses.Where($"{propName} <> @0", val);
                            break;
                        case IEnum.Comparison.Custom:
                            break;
                    }
                }
                else
                {
                    if (prop.PropertyType == typeof(string))
                    {
                        if (!String.IsNullOrEmpty((string)val))
                            proses = proses.Where($"{propName}.Contains(@0)", val);
                    }
                    else if (Nullable.GetUnderlyingType(prop.PropertyType) != null)
                    {
                        if (val != null)
                        {
                            proses = proses.Where($"{propName} = @0", val);
                        }
                    }
                }
            }
            return proses;
        }

        public static DateTime? F_DateTime(this string t)
        {
            return (DateTime.TryParse(t, out DateTime x) ? x : (DateTime?)null);
        }

        public static int? F_int(this string t)
        {
            return (int.TryParse(t, out int x) ? x : (int?)null);
        }

        public static decimal? F_Decimal(this string t)
        {
            return (decimal.TryParse(t, out decimal x) ? x : (decimal?)null);
        }

        public static bool? F_bool(this string t)
        {
            if (t == "1") t = "true";
            else if (t == "0") t = "false";
            return (bool.TryParse(t, out bool x) ? x : (bool?)null);
        }

        public static bool? F_bool(this string t, string target)
        {
            return t.ToUpper().Trim() == "DIAMBIL" ? true : (bool?)null;
        }

        public static short? F_short(this string t)
        {
            return (short.TryParse(t, out short x) ? x : (short?)null);
        }
    }
}
