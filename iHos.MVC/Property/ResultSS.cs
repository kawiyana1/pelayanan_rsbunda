﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iHos.MVC.Property
{
    public class ResultSS
    {
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public object Data { get; set; }

        public object[] ListData { get; set; }

        public int Count { get; set; }

        public int? TotalCount { get; set; }

        public int? Page { get; set; }

        public ResultSS() { }

        public ResultSS(int count, object[] lsitdata, int totalcount, int page)
        {
            TotalCount = totalcount;
            Page = page;
            IsSuccess = true;
            ListData = lsitdata;
            Count = count;
        }

        public ResultSS(int count, object data)
        {
            IsSuccess = true;
            Data = data;
            Count = count;
        }

        public ResultSS(int[] count)
        {
            IsSuccess = true;
            ListData = new object[count.Count()];
            for (var i = 0; i < count.Count(); i++)
                ListData[i] = count[i];
            Count = count.Sum();
        }

        public ResultSS(int count)
        {
            IsSuccess = true;
            Count = count;
        }

        public ResultSS(decimal count)
        {
            IsSuccess = true;
            Count = (int)count;
            Data = count;
        }

        public ResultSS(string message)
        {
            IsSuccess = false;
            Message = message;
            Count = 0;
        }

        public ResultSS(SqlException ex)
        {
            IsSuccess = false;
            Message = ex.Message;
            Count = 0;
        }

        public ResultSS(Exception ex)
        {
            IsSuccess = false;
            Message = ex.Message;
            Count = 0;
        }
    }
}
