﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pelayanan.Startup))]
namespace Pelayanan
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
