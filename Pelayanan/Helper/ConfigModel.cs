﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Helper
{
    public class ConfigModel
    {
        public static string AppName { get { return "Pelayanan"; } }
        public static string HospitalName { get { return "Rumah Sakit Umum Bunda Negara"; } }
        public static string LoginBackground { get { return "background1.png"; } }
        public static string ErrorBackground { get { return ""; } }
        public static string LogoHospital { get { return "logo.png"; } }
        public static string CopyRight { get { return $" 2022. System {ConfigModel.AppName} {ConfigModel.HospitalName} "; } }
    }
}