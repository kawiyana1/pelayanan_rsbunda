//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pelayanan.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class mOrderPenunjang_Jenis
    {
        public int ID { get; set; }
        public string SectionID { get; set; }
        public Nullable<int> Nomor { get; set; }
        public string Nama { get; set; }
        public Nullable<bool> Aktif { get; set; }
    }
}
