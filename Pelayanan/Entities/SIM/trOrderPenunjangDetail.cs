//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pelayanan.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class trOrderPenunjangDetail
    {
        public string NoBukti { get; set; }
        public Nullable<int> UrutJenis { get; set; }
        public string NamaJenis { get; set; }
        public Nullable<int> UrutKelompok { get; set; }
        public int ID_Kelompok { get; set; }
        public string NamaKelompok { get; set; }
        public Nullable<int> UrutJasa { get; set; }
        public string NamaTindakan { get; set; }
        public string JasaID { get; set; }
        public Nullable<bool> Dipilih { get; set; }
        public Nullable<bool> Realisasi { get; set; }
        public Nullable<bool> DiReferal { get; set; }
    }
}
