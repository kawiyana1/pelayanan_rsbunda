//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pelayanan.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_mOrderPenunjang_ListJasaManual
    {
        public string JasaID { get; set; }
        public string JasaName { get; set; }
        public string Alias { get; set; }
        public string KeteranganTindakan { get; set; }
        public string SectionID { get; set; }
    }
}
