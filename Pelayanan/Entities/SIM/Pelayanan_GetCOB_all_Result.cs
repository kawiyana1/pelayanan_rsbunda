//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pelayanan.Entities.SIM
{
    using System;
    
    public partial class Pelayanan_GetCOB_all_Result
    {
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string SectionName { get; set; }
        public string NoReg { get; set; }
        public string NoBukti { get; set; }
        public Nullable<double> Total { get; set; }
        public Nullable<bool> COB { get; set; }
    }
}
