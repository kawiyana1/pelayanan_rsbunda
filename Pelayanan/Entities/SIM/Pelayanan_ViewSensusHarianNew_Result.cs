//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pelayanan.Entities.SIM
{
    using System;
    
    public partial class Pelayanan_ViewSensusHarianNew_Result
    {
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public System.DateTime Tanggal { get; set; }
        public int JmlBed { get; set; }
        public Nullable<int> JmlBedTerisi { get; set; }
        public string NoKamar { get; set; }
        public string NoBed { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public Nullable<bool> OK_All { get; set; }
        public Nullable<bool> Baru { get; set; }
        public Nullable<bool> OK_Baru { get; set; }
        public Nullable<bool> Lama { get; set; }
        public Nullable<bool> OK_Lama { get; set; }
        public Nullable<bool> Keluar { get; set; }
        public Nullable<bool> OK_Keluar { get; set; }
        public Nullable<int> Aktif { get; set; }
        public Nullable<bool> OK_Aktif { get; set; }
        public string StatusPasienBaru { get; set; }
        public string StatusPasienKeluar { get; set; }
    }
}
