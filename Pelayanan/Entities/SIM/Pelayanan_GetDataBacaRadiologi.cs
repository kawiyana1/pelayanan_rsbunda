//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pelayanan.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class Pelayanan_GetDataBacaRadiologi
    {
        public string NoBukti { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string DokterID { get; set; }
        public string NamaDOkter { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string JenisKelamin { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public Nullable<int> UmurThn { get; set; }
        public Nullable<int> UmurBln { get; set; }
        public Nullable<int> Hari { get; set; }
    }
}
