//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pelayanan.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class BILLOPTIK_RESEP
    {
        public string No_Bukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string NoTelp { get; set; }
        public string JenisKelamin { get; set; }
        public Nullable<int> UmurTahun { get; set; }
        public Nullable<int> UmurBulan { get; set; }
        public Nullable<int> UmurHari { get; set; }
        public Nullable<int> JenisKerjasamaID { get; set; }
        public string DokterID { get; set; }
        public string OD_Spheris { get; set; }
        public string OD_Cylinder { get; set; }
        public string OD_Prisma { get; set; }
        public string OD_Axis { get; set; }
        public string OS_Spheris { get; set; }
        public string OS_Cylinder { get; set; }
        public string OS_Prisma { get; set; }
        public string OS_Axis { get; set; }
        public string JenisLensa { get; set; }
        public string Addition { get; set; }
        public string PupilDistance { get; set; }
        public string WarnaKaca { get; set; }
        public string Untuk { get; set; }
        public Nullable<bool> Batal { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }
        public Nullable<int> User_ID { get; set; }
        public bool Realisasi { get; set; }
        public string SectionID { get; set; }
        public string Kode_Customer { get; set; }
    }
}
