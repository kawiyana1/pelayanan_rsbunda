﻿using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    public class FilmRadiologiController : Controller
    {
        // GET: FilmRadiologi
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new FIlmRadiologiViewDetail();
            var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
            using (var s = new SIM_Entities())
            {

                var filmlist = s.PemakaianFilm_GetDataList(id).ToList();
                if (filmlist != null)
                {
                    model.Detail_List = new ListDetail<FilmRadiologiDetailViewModel>();
                    var detail_1 = s.PemakaianFilm_GetDataList(id).Where(x => x.Nobukti == id).ToList();
                    foreach (var x in detail_1)
                    {

                        var y = IConverter.Cast<FilmRadiologiDetailViewModel>(x);
                        var namafilm = s.mFilm_Radiologi().FirstOrDefault(e => e.KodeFilm == y.KodeFilm);
                        if (namafilm != null)
                        {
                            y.NamaFilm = namafilm.KodeFilm;
                            y.NamaFilmNama = namafilm.NamaFilm;
                        }
                        model.Detail_List.Add(false, y);
                    }
                }
                model.NoBukti = id;
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new FIlmRadiologiViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var activity = "Film Radiologi";
                    if (item.Detail_List == null) item.Detail_List = new ListDetail<FilmRadiologiDetailViewModel>();
                    item.Detail_List.RemoveAll(x => x.Remove);
                    var d = s.PemakaianFilm_GetDataList(item.NoBukti).ToList();
                    foreach (var x in d)
                    {
                        //var model = s.EMR_trDPJP.FirstOrDefault(z => z.Id == item.Id);
                        var model = s.PemakaianFilm_GetDataList(item.Id).FirstOrDefault(z => z.ID == x.ID);
                        //delete
                        if (model == null)
                        {
                            s.PemakaianFilm_delete((int)x.ID);
                        }
                    }
                    foreach (var x in item.Detail_List)
                    {
                        var _d = d.FirstOrDefault(y => y.ID == x.Model.ID);
                        if (_d == null)
                        {
                            // new
                            var namafilminsert = s.mFilm_Radiologi().FirstOrDefault(y => y.KodeFilm == x.Model.NamaFilm);
                            s.PemakaianFilm_Insert(
                                    item.NoBukti,
                                    x.Model.NamaFilm,
                                    namafilminsert.NamaFilm,
                                    x.Model.Jumlah,
                                    x.Model.KeteranganFilm,
                                    item.UserId
                                );
                        }
                        else
                        {
                            // edit
                            var namafilminsert = s.mFilm_Radiologi().FirstOrDefault(y => y.KodeFilm == x.Model.NamaFilm);
                            s.PemakaianFilm_Update(
                                item.NoBukti,
                                x.Model.NamaFilm,
                                namafilminsert.NamaFilm,
                                x.Model.Jumlah,
                                x.Model.KeteranganFilm,
                                item.UserId,
                                (int)x.Model.ID
                            );
                        }

                    }

                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}