﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Pelayanan.Controllers
{
    public class RujukanPenunjangController : Controller
    {
        #region ==== CREATE
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg, string section, int nomor)
        {
            var model = new OrderPenunjangViewModel();
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.NoReg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;
            using (var s = new SIM_Entities())
            {
                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                if (m == null) return HttpNotFound();

                model.NoBukti = m.Memo;
                var order = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == m.Memo);
                if(order != null)
                {
                    model = IConverter.Cast<OrderPenunjangViewModel>(order);
                }

                ViewBag.DokterID = m.DokterID;
                ViewBag.NamaDokter = m.NamaDOkter;
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post(OrderPenunjangViewModel item)
        {
            try
            {
                item = new OrderPenunjangViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                if (item.KodePetugas == null) throw new Exception("Ruangan tidak boleh kosong");
                                if (item.KodeVendor == null) throw new Exception("Dokter tidak boleh kosong");

                                var user = User.Identity.GetUserId();
                                var nobukti = item.NoBukti;

                                item.JamSampling = item.JamSampling_Tgl.Value.Date + item.JamSampling_Jam.Value.TimeOfDay;

                                var order = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                                if(order == null) throw new Exception("Dokter tidak boleh kosong");

                                order.JamSampling = item.JamSampling;
                                order.KodeVendor = item.KodeVendor;
                                order.NamaVendor = item.NamaVendor;
                                order.KodePetugas = item.KodePetugas;
                                order.NamaPetugas = item.NamaPetugas;
                                order.SectionAsalID = item.SectionAsalID;
                                order.DokterID = item.DokterID;
                                order.DiagnosaIndikasi = item.DiagnosaIndikasi;

                                if (item.Jasa != null)
                                {
                                    var i = 1;
                                    foreach (var x in item.Jasa)
                                    {
                                        var orderDetail = s.trOrderPenunjangDetail.FirstOrDefault(e => e.NoBukti == item.NoBukti && e.JasaID == x.JasaID);
                                        if(orderDetail != null)
                                        {
                                            orderDetail.DiReferal = true;
                                        }

                                        i++;
                                    }
                                }

                                var r = s.SaveChanges();

                                dbContextTransaction.Commit();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"OrderPenunjang Create {item.Noreg} {item.SectionID}"
                                };
                                UserActivity.InsertUserActivity(userActivity);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });

                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new ArgumentNullException("connection", "Masih ada transaksi, silahkan simpan ulang");
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                                throw new Exception(ex.Message);
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }

                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== EDIT
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string nobukti)
        {
            var model = new OrderPenunjangViewModel();

            using (var s = new SIM_Entities())
            {
                var m = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();

                model = IConverter.Cast<OrderPenunjangViewModel>(m);

                var d = s.trOrderPenunjangDetailManual.Where(x => x.NoBukti == nobukti).ToList().ConvertAll(x => IConverter.Cast<OrderPenunjang_OrderManual>(x));
                foreach (var dd in d)
                {
                    var jj = s.SIMmListJasa.FirstOrDefault(x => x.JasaID == dd.JasaID);
                    if (jj != null)
                    {
                        dd.NamaJasa = jj.JasaName;
                    }
                    dd.Tarif_View = IConverter.ToMoney(decimal.Parse(dd.Tarif.ToString()));

                    var tt = s.mDokter.FirstOrDefault(x => x.DokterID == dd.DokterID);
                    if (tt != null)
                    {
                        dd.NamaDokter = tt.NamaDOkter;
                    }
                }
                model.OrderManual = d;

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                if (dokter != null)
                {
                    model.NamaDokter = dokter.NamaDOkter;
                }

                model.DokterID = m.DokterID;
                model.Noreg = m.Noreg;
                model.SectionID = m.SectionID;
                model.JamSampling_Tgl = m.JamSampling.Value.Date;
                model.JamSampling_Jam = m.JamSampling;

            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public string Edit_Post()
        {
            try
            {
                var item = new OrderPenunjangViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region === VALIDASI
                                if (item.SectionID == null) throw new Exception("Ruangan tidak boleh kosong");
                                //if (item.OrderManual == null && item.Jasa == null)
                                //{
                                //    throw new Exception("Salah satu detail jasa harus diisi");
                                //}
                                var Order = s.trOrderPenunjang.Where(x => x.NoBukti == item.NoBukti).FirstOrDefault();
                                if (Order == null)
                                {
                                    throw new Exception("Order Penunjang tidak ditemukan");
                                }
                                #endregion

                                #region === DELETE DETAIL
                                var trOrderPenunjangDetailManual = s.trOrderPenunjangDetailManual.Where(x => x.NoBukti == Order.NoBukti);
                                if (trOrderPenunjangDetailManual != null)
                                {
                                    s.trOrderPenunjangDetailManual.RemoveRange(trOrderPenunjangDetailManual);
                                }

                                var trOrderPenunjangDetail = s.trOrderPenunjangDetail.Where(x => x.NoBukti == Order.NoBukti);
                                if (trOrderPenunjangDetail != null)
                                {
                                    s.trOrderPenunjangDetail.RemoveRange(trOrderPenunjangDetail);
                                }
                                #endregion

                                var user = User.Identity.GetUserId();
                                var nobukti = Order.NoBukti;

                                Order.Hasil_Cito = item.Hasil_Cito;
                                Order.DiagnosaIndikasi = item.DiagnosaIndikasi;
                                Order.DokterID = item.DokterID;
                                Order.PemeriksaanTambahan = item.PemeriksaanTambahan;
                                Order.Hasil_Cito = item.Hasil_Cito;
                                Order.Hasil_Cito = item.Hasil_Cito;
                                Order.JamSampling = item.JamSampling_Tgl.Value.Date + item.JamSampling_Jam.Value.TimeOfDay;
                                Order.UserId = user;

                                if (item.OrderManual != null)
                                {
                                    var i = 1;
                                    foreach (var x in item.OrderManual)
                                    {
                                        var m = IConverter.Cast<trOrderPenunjangDetailManual>(item.OrderManual);
                                        m.NoBukti = nobukti;
                                        m.NoUrut = i;
                                        m.JasaID = x.JasaID;
                                        m.Qty = x.Qty;
                                        m.Tarif = x.Tarif;
                                        m.DokterID = x.DokterID;
                                        m.Keterangan = x.Keterangan;
                                        m.Realisasi = false;
                                        s.trOrderPenunjangDetailManual.Add(m);

                                        i++;
                                    }
                                }

                                if (item.Jasa != null)
                                {
                                    foreach (var x in item.Jasa)
                                    {
                                        var m = IConverter.Cast<trOrderPenunjangDetail>(item.Jasa);
                                        m.NoBukti = nobukti;
                                        m.UrutJenis = x.UrutJenis;
                                        m.NamaJenis = x.NamaJenis;
                                        m.UrutKelompok = x.UrutKelompok;
                                        m.ID_Kelompok = x.ID_Kelompok;
                                        m.NamaKelompok = x.NamaKelompok;
                                        m.UrutJasa = x.UrutJasa;
                                        m.NamaTindakan = x.NamaTindakan;
                                        m.JasaID = x.JasaID;
                                        m.Dipilih = true;
                                        m.Realisasi = false;
                                        s.trOrderPenunjangDetail.Add(m);
                                    }
                                }

                                var r = s.SaveChanges();

                                dbContextTransaction.Commit();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"OrderPenunjang Create {item.Noreg} {item.SectionID}"
                                };
                                UserActivity.InsertUserActivity(userActivity);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });

                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new ArgumentNullException("connection", "Masih ada transaksi, silahkan simpan ulang");
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                                throw new Exception(ex.Message);
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }

                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail_Get(string nobukti)
        {
            var model = new OrderPenunjangViewModel();

            using (var s = new SIM_Entities())
            {
                var m = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();

                model = IConverter.Cast<OrderPenunjangViewModel>(m);

                var d = s.trOrderPenunjangDetailManual.Where(x => x.NoBukti == nobukti).ToList().ConvertAll(x => IConverter.Cast<OrderPenunjang_OrderManual>(x));
                foreach (var dd in d)
                {
                    var jj = s.SIMmListJasa.FirstOrDefault(x => x.JasaID == dd.JasaID);
                    if (jj != null)
                    {
                        dd.NamaJasa = jj.JasaName;
                    }
                    dd.Tarif_View = IConverter.ToMoney(decimal.Parse(dd.Tarif.ToString()));

                    var tt = s.mDokter.FirstOrDefault(x => x.DokterID == dd.DokterID);
                    if (tt != null)
                    {
                        dd.NamaDokter = tt.NamaDOkter;
                    }
                }
                model.OrderManual = d;

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                if (dokter != null)
                {
                    model.NamaDokter = dokter.NamaDOkter;
                }

                model.DokterID = m.DokterID;
                model.Noreg = m.Noreg;
                model.SectionID = m.SectionID;

            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }
        #endregion

        #region ==== GET DETAIL
        [HttpGet]
        public string get_JenisKelompokJasa(string section, string nobukti = null)
        {
            try
            {
                var Jenis = new List<Vw_mOrderPenunjang_Jenis>();
                object Kelompok = new Vw_mOrderPenunjang_Kelompok();
                var JasaOper = new List<Object>();
                object JasaSave = new trOrderPenunjangDetail();
                using (var s = new SIM_Entities())
                {
                    var orderRujukan = s.trOrderPenunjang_DetailJasa_RujukanPenunjang(nobukti).ToList();
                    var saveKaracter = 0;
                    foreach (var x in orderRujukan)
                    {
                        var jj = s.Vw_mOrderPenunjang_Jenis.FirstOrDefault(e => e.ID == x.JenisID);
                        if (saveKaracter != jj.ID)
                        {
                            Jenis.Add(jj);
                            saveKaracter = jj.ID;
                        }
                    }
                    
                    Kelompok = s.Vw_mOrderPenunjang_Kelompok.ToList().ConvertAll(x => IConverter.Cast<OrderPenunjang_Kelompok>(x));
                    var Jasa = s.trOrderPenunjang_DetailJasa_RujukanPenunjang(nobukti).ToList();
                    foreach (var e in Jasa)
                    {
                        bool ref_status = false;
                        var referal = s.trOrderPenunjangDetail.FirstOrDefault(r => r.NoBukti == nobukti && r.JasaID == e.JasaID);
                        if(referal != null)
                        {
                            ref_status = (bool) (referal.DiReferal != true ? false : true);
                        }
                        JasaOper.Add(new
                        {
                            Nomor = e.Nomor,
                            Kelompok_ID = e.Kelompok_ID,
                            JasaID = e.JasaID,
                            NamaAlias = e.NamaAlias,
                            NamaJasa = e.NamaJasa,
                            JenisID = e.JenisID,
                            Referal = ref_status,
                        });
                    }
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Jenis = Jenis,
                    Kelompok = Kelompok,
                    Jasa = JasaOper,
                    Message = "-"
                });

            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string nobukti)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == nobukti);
                    if (m == null) throw new Exception("Data Tidak ditemukan");

                    s.trOrder_insert_batal(nobukti);
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Order Penunjang delete {nobukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);

                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== R E P O R T

        public ActionResult ExportPDF(string id)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Reports"), $"trOrderPenunjang_RujukanPenunjang.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();
                    var i = 0;
                    cmd.Add(new SqlCommand("trOrderPenunjang_RujukanPenunjang", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoBukti", id));

                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"select * from sReport", conn));
                    cmd[i].CommandType = CommandType.Text;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion
    }
}