﻿using Pelayanan.Entities.PPI;
using Pelayanan.Models.PPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using iHos.MVC.Property;
using System.Data.SqlClient;
using Pelayanan.Models;
using System.Data.Entity.Validation;
using iHos.MVC.Converter;
using Pelayanan.Entities.SIM;

namespace Pelayanan.Controllers.PPI
{
    [Authorize(Roles = "Pelayanan")]
    public class SurvailansController : Controller
    {
        // GET: Survailans
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            var user = (System.Security.Claims.ClaimsIdentity)User.Identity;
            var model = new SurvailansDataModel();

            model.Tanggal = DateTime.Today;
            model.UserName = user.FindFirstValue("FullName");
            model.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
            model.SectionName = Request.Cookies["SectionNamaPelayanan"].Value;

            using (var s = new PPI_Entities())
            {
                ViewData["SudahAdaData"] = s.PPI_trSurveilensHAIs.Where(x => x.Tanggal == model.Tanggal && x.SectionID == model.SectionID).Count() > 0;
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post()
        {
            try
            {
                var item = new SurvailansDataModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new PPI_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var o = IConverter.Cast<PPI_trSurveilensHAIs>(item);
                                o.Jam = DateTime.Now;
                                o.Tanggal = DateTime.Today;
                                o.UserIDWeb = User.Identity.GetUserId();
                                o.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                                s.PPI_trSurveilensHAIs.Add(o);
                                if (item.Detail_List == null) item.Detail_List = new ListDetail<SurveilensDetailDataModel>();

                                //var m = s.PPI_FtGetHAIsHarianRI(o.SectionID);
                                List<mICD> icds;
                                using (var t = new SIM_Entities())
                                {
                                    var xx = item.Detail_List.GroupBy(x => x.Model.KodeICD).Select(y => y.Key);
                                    icds = t.mICD.Where(x => xx.Contains(x.KodeICD)).ToList();
                                }

                                item.Detail_List.RemoveAll(x => x.Remove);

                                foreach (var x in item.Detail_List2)
                                {
                                    //var n = m.FirstOrDefault(y => y.NoReg == x.Noreg);
                                    
                                    var xx = new PPI_trSurveilensHAIs_Detail() {
                                        SectionID = o.SectionID,
                                        Tanggal = o.Tanggal,
                                        NoBukti = o.NoBukti,
                                        PasienBaru = x.PasienBaru,
                                        Suhu = x.Suhu,
                                        Infek_DEK = x.Infek_DEK,
                                        Infek_HAP = x.Infek_HAP,
                                        Infek_IADP = x.Infek_IADP,
                                        Infek_ISK = x.Infek_ISK,
                                        Infek_PLEB = x.Infek_PLEB,
                                        Infek_VAP = x.Infek_VAP,
                                        Tind_CVL = x.Tind_CVL,
                                        Tind_ETT = x.Tind_ETT,
                                        Tind_IVL = x.Tind_IVL,
                                        Tind_TB = x.Tind_TB,
                                        Tind_UC = x.Tind_UC,
                                        Tind_VEN = x.Tind_VEN,
                                        Kultur_Sample = x.Kultur_Sample,
                                        Antibiotik = x.Antibiotik,
                                        KodeICD = x.KodeICD,
                                        Jenis_Kelamin = x.Jenis_Kelamin, // ?? n?.JK,
                                        KetICD = icds.FirstOrDefault(z => z.KodeICD == x.KodeICD)?.Descriptions,
                                        Kultur_Ket = x.Kultur_Ket,
                                        NamaPasien = x.NamaPasien, // ?? n?.NamaPasien,
                                        Noreg = x.Noreg, // ?? n?.NoReg,
                                        NRM = x.NRM,
                                        Umur_Bln = x.Umur_Bln, //?? n?.UmurBln,
                                        Umur_Th = x.Umur_Th // ?? n?.UmurThn
                                    };
                                    s.PPI_trSurveilensHAIs_Detail.Add(xx);

                                    if (x.AR == null) x.AR = new List<SurveilensDetailDetailDataModel>();
                                    foreach (var d in x.AR)
                                    {
                                        var y = new PPI_trSurveilensHAIs_Detail_AB()
                                        {
                                            Dosis = d.Dosis,
                                            Id_AntiBiotik = d.Id,
                                            Noreg = x.Noreg,
                                            SectionID = o.SectionID,
                                            Tanggal = o.Tanggal
                                        };
                                        s.PPI_trSurveilensHAIs_Detail_AB.Add(y);
                                    }
                                }

                                s.SaveChanges();

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"PPI Survailans Create {item.Tanggal} & {item.SectionID}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string get_detail_surveilan()
        {
            try
            {
                var model = new SurvailansDataModel();

                model.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                model.Detail_List = new ListDetail<SurveilensDetailDataModel>();
                using (var p = new PPI_Entities())
                {
                    var m = p.PPI_FtGetHAIsHarianRI(model.SectionID);
                    if (m != null)
                    {
                        var row = 1;
                        foreach (var x in m.ToList())
                        {
                            model.Detail_List.Add(false,new SurveilensDetailDataModel()
                            {
                                Nomor = row,
                                Noreg = x.NoReg,
                                NamaPasien = x.NamaPasien,
                                Jenis_Kelamin = x.JK,
                                Umur = x.UmurThn+" Thn "+x.UmurBln+" Bln",
                                Umur_Bln = x.UmurBln,
                                Umur_Th = x.UmurThn,
                                KetICD = x.KetICD,
                                KodeICD = x.KodeICD,
                                PasienBaru = x.PasienBaru
                            });;
                            row++;
                        }
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = "",
                            Message = "Data tidak ditemukan"
                        });
                    }

                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model.Detail_List,
                    Message = ""
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        
        #region ===== D E T A I L

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(DateTime id)
        {
            SurvailansDataModel item;
            try
            {
                using (var s = new PPI_Entities())
                {
                    var sec = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = s.PPI_trSurveilensHAIs.FirstOrDefault(x => x.Tanggal == id && x.SectionID == sec);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<SurvailansDataModel>(m);

                    var n = s.PPI_FtTRHeader(1).FirstOrDefault(x => x.Tanggal == id);
                    item.SectionName = n.SectionName;
                    item.UserName = n.UserCreate;

                    var d = s.PPI_trSurveilensHAIs_Detail.Where(x => x.Tanggal == id && x.SectionID == m.SectionID).ToList();
                    item.Detail_List = new ListDetail<SurveilensDetailDataModel>();
                    var i = 1;
                    foreach (var x in d)
                    {
                        var y = IConverter.Cast<SurveilensDetailDataModel>(x);
                        y.Nomor = i;
                        y.Umur = y.Umur_Th + " Thn " + y.Umur_Bln + " Bln";

                        var ar = s.VW_PPI_trSurveilensHAIs_Detail_AB.Where(a => a.Noreg == x.Noreg && a.SectionID == m.SectionID && a.Tanggal == id).ToList();
                        y.AR = ar.ConvertAll(a => new SurveilensDetailDetailDataModel()
                        {
                            Dosis = a.Dosis,
                            Id = a.Id_AntiBiotik,
                            Nama = a.AntiBiotikaName
                        });

                        item.Detail_List.Add(false, y);
                        i++;
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        #endregion


        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new PPI_Entities())
                {
                    IQueryable<PPI_FtTRHeader_Result> proses = s.PPI_FtTRHeader(1).AsQueryable();
                    //if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(PPI_trSurvailansHAIs.Tanggal)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(PPI_FtTRHeader_Result.SectionID)}=@0", Request.Cookies["SectionIDPelayanan"].Value);
                    if (filter[14] != "True" && DateTime.Parse(filter[12]) != null && DateTime.Parse(filter[13]) != null)
                    {
                        proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).Date);
                        proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]).Date);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<SurvailansDataModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.Tanggal_View2 = x.Tanggal.ToString("yyyy-MM-dd");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        [HttpPost]
        public string ListAB(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new PPI_Entities())
                {
                    IQueryable<PPI_mAntiBiotika> proses = s.PPI_mAntiBiotika.AsQueryable();
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(PPI_mAntiBiotika.AntiBiotikaName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PPI_mAntiBiotika>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListPasien(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new PPI_Entities())
                {
                    IQueryable<PPI_mPasien> proses = s.PPI_mPasien.AsQueryable();
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(PPI_mPasien.NRM)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(PPI_mPasien.NamaPasien)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2]))
                    {
                        var d = DateTime.Parse(filter[2]);
                        proses = proses.Where($"{nameof(PPI_mPasien.TglLahir)} = @0", d);
                    }
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(PPI_mPasien.JenisKelamin)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(PPI_mPasien.Alamat)}.Contains(@0)", filter[4]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PPI_mPasien>(x));
                    foreach (var x in m) 
                    {
                        // titip
                        x.Email = x.TglLahir?.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}