﻿using Pelayanan.Entities.PPI;
using Pelayanan.Models.PPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using iHos.MVC.Property;
using System.Data.SqlClient;
using Pelayanan.Models;
using System.Data.Entity.Validation;
using iHos.MVC.Converter;
using Pelayanan.Entities.SIM;

namespace Pelayanan.Controllers.PPI
{
    [Authorize(Roles = "Pelayanan")]
    public class IDOController : Controller
    {
        public static List<SelectListItem> TipeOpList { 
            get 
            {
                return new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "Cito", Value = "Cito"},
                    new SelectListItem(){ Text = "Elektif", Value = "Elektif"},
                };
            } 
        }
        public static List<SelectListItem> ScoreAsaList
        {
            get
            {
                return new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "", Value = ""},
                    new SelectListItem(){ Text = "1", Value = "1"},
                    new SelectListItem(){ Text = "2", Value = "2"},
                    new SelectListItem(){ Text = "3", Value = "3"},
                    new SelectListItem(){ Text = "4", Value = "4"},
                    new SelectListItem(){ Text = "5", Value = "5"},
                };
            }
        }

        public static List<SelectListItem> DurasiOpList
        {
            get
            {
                return new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "", Value = ""},
                    new SelectListItem(){ Text = "<", Value = "<"},
                    new SelectListItem(){ Text = ">", Value = ">"},
                };
            }
        }

        public static List<SelectListItem> JenisKulturList
        {
            get
            {
                return new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "", Value = ""},
                    new SelectListItem(){ Text = "Darah", Value = "Darah"},
                    new SelectListItem(){ Text = "Urine", Value = "Urine"},
                    new SelectListItem(){ Text = "Dahak", Value = "Dahak"},
                    new SelectListItem(){ Text = "PUS/Usap Basah Luka", Value = "PUS/Usap Basah Luka"},
                };
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            var user = (System.Security.Claims.ClaimsIdentity)User.Identity;
            var model = new IDODataModel();

            model.Tanggal = DateTime.Today;
            model.UserName = user.FindFirstValue("FullName");
            model.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
            model.SectionName = Request.Cookies["SectionNamaPelayanan"].Value;

            using (var s = new PPI_Entities())
            {
                ViewData["SudahAdaData"] = s.PPI_trSurveilensIDO.Where(x => x.Tanggal == model.Tanggal && x.SectionID == model.SectionID).Count() > 0;
            }

            ViewBag.TipeOpList = TipeOpList;
            ViewBag.ScoreAsaList = ScoreAsaList;
            ViewBag.DurasiOpList = DurasiOpList;
            ViewBag.JenisKulturList = JenisKulturList;

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        //[ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new IDODataModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new PPI_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var o = IConverter.Cast<PPI_trSurveilensIDO>(item);
                                o.Jam = DateTime.Now;
                                o.Tanggal = DateTime.Today;
                                o.UserIDWeb = User.Identity.GetUserId();
                                o.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                                s.PPI_trSurveilensIDO.Add(o);
                                if (item.Detail_List2 == null) item.Detail_List2 = new List<IDODetailDataModel>();

                                //var m = s.PPI_FtGetHAIsHarianRI(o.SectionID);
                                List<mICD> icds;
                                using (var t = new SIM_Entities())
                                {
                                    var xx = item.Detail_List2.GroupBy(x => x.KodeICD).Select(y => y.Key);
                                    icds = t.mICD.Where(x => xx.Contains(x.KodeICD)).ToList();
                                }

                                //item.Detail_List2.RemoveAll(x => x.Remove);

                                foreach (var x in item.Detail_List2)
                                {
                                    var xx = new PPI_trSurveilensIDO_Detail()
                                    {
                                        SectionID = o.SectionID,
                                        Tanggal = o.Tanggal,
                                        KodeICD = x.KodeICD,
                                        KetICD = icds.FirstOrDefault(z => z.KodeICD == x.KodeICD)?.Descriptions,
                                        Kultur_Ket = x.Kultur_Ket,
                                        NRM = x.NRM,
                                        Antibiotik_DEF = x.Antibiotik_DEF,
                                        Antibiotik_EMP = x.Antibiotik_EMP,
                                        Antibiotik_Prof = x.Antibiotik_Prof,
                                        DrBedahID = x.DrBedahID,
                                        DrBedahNama = x.DrBedahNama,
                                        DurasiOperasi = x.DurasiOperasi,
                                        JenisOperasi = x.JenisOperasi,
                                        Kultur_Sample = x.Kultur_Sample,
                                        NamaPasien = x.NamaPasien,
                                        Noreg = x.Noreg,
                                        ScoreAsa = x.ScoreAsa,
                                        TglInfeksi = x.TglInfeksi,
                                        TglOp = x.TglOp,
                                        TipeOperasi = x.TipeOperasi,
                                        Total = x.Total,
                                    };
                                    s.PPI_trSurveilensIDO_Detail.Add(xx);

                                    if (x.Prof == null) x.Prof = new List<IDODetailAntibiotikDataModel>();
                                    foreach (var d in x.Prof) 
                                    {
                                        var y = new PPI_trSurveilensIDO_Detail_AB_Prof() 
                                        {
                                            Dosis = d.Dosis,
                                            Id_AntiBiotik_Prof = d.Id,
                                            Noreg = x.Noreg,
                                            SectionID = o.SectionID,
                                            Tanggal = o.Tanggal
                                        };
                                        s.PPI_trSurveilensIDO_Detail_AB_Prof.Add(y);
                                    }
                                    if (x.EMP == null) x.EMP = new List<IDODetailAntibiotikDataModel>();
                                    foreach (var d in x.EMP)
                                    {
                                        var y = new PPI_trSurveilensIDO_Detail_AB_EMP()
                                        {
                                            Dosis = d.Dosis,
                                            Id_AntiBiotik_EMP = d.Id,
                                            Noreg = x.Noreg,
                                            SectionID = o.SectionID,
                                            Tanggal = o.Tanggal
                                        };
                                        s.PPI_trSurveilensIDO_Detail_AB_EMP.Add(y);
                                    }
                                    if (x.DEF == null) x.DEF = new List<IDODetailAntibiotikDataModel>();
                                    foreach (var d in x.DEF)
                                    {
                                        var y = new PPI_trSurveilensIDO_Detail_AB_DEF()
                                        {
                                            Dosis = d.Dosis,
                                            Id_AntiBiotik_DEF = d.Id,
                                            Noreg = x.Noreg,
                                            SectionID = o.SectionID,
                                            Tanggal = o.Tanggal
                                        };
                                        s.PPI_trSurveilensIDO_Detail_AB_DEF.Add(y);
                                    }
                                }

                                s.SaveChanges();

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"PPI IDO Create {item.Tanggal} & {item.SectionID}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        
        [HttpPost]
        public string ListAntibiotika(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new PPI_Entities())
                {
                    IQueryable<PPI_mAntiBiotika> proses = s.PPI_mAntiBiotika.AsQueryable();
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(PPI_mAntiBiotika.AntiBiotikaName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PPI_mAntiBiotika>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        
        [HttpPost]
        public string ListAB(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new PPI_Entities())
                {
                    IQueryable<PPI_FtGetDataIDO_Result> proses = s.PPI_FtGetDataIDO().AsQueryable();
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(PPI_FtGetDataIDO_Result.NoReg)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(PPI_FtGetDataIDO_Result.NamaPasien)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(PPI_FtGetDataIDO_Result.DokterBedah)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(PPI_FtGetDataIDO_Result.Diagnosa)}.Contains(@0)", filter[1]);
                    //if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(PPI_FtGetDataIDO_Result.DokterBedah)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PPI_FtGetDataIDO_Result>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #region ===== D E T A I L

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(DateTime id)
        {
            IDODataModel item = new IDODataModel();
            try
            {
                using (var s = new PPI_Entities())
                {
                    var sec = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = s.PPI_trSurveilensIDO.FirstOrDefault(x => x.Tanggal == id && x.SectionID == sec);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<IDODataModel>(m);

                    var n = s.PPI_FtTRHeader(3).FirstOrDefault(x => x.Tanggal == id);
                    item.SectionName = n.SectionName;
                    item.UserName = n.UserCreate;

                    var d = s.PPI_trSurveilensIDO_Detail.Where(x => x.Tanggal == id && x.SectionID == m.SectionID).ToList();
                    item.Detail_List = new ListDetail<IDODetailDataModel>();
                    var i = 1;
                    foreach (var x in d)
                    {
                        var y = IConverter.Cast<IDODetailDataModel>(x);
                        y.Nomor = i;
                        var prof = s.VW_PPI_trSurveilensIDO_Detail_AB_Prof.Where(a => a.Noreg == x.Noreg && a.SectionID == m.SectionID && a.Tanggal == id).ToList();
                        y.Prof = prof.ConvertAll(a => new IDODetailAntibiotikDataModel()
                        {
                            Dosis = a.Dosis,
                            Id = a.Id_AntiBiotik_Prof,
                            Nama = a.AntiBiotikaName
                        });
                        var DEF = s.VW_PPI_trSurveilensIDO_Detail_AB_DEF.Where(a => a.Noreg == x.Noreg && a.SectionID == m.SectionID && a.Tanggal == id).ToList();
                        y.DEF = DEF.ConvertAll(a => new IDODetailAntibiotikDataModel()
                        {
                            Dosis = a.Dosis,
                            Id = a.Id_AntiBiotik_DEF,
                            Nama = a.AntiBiotikaName
                        });
                        var EMP = s.VW_PPI_trSurveilensIDO_Detail_AB_EMP.Where(a => a.Noreg == x.Noreg && a.SectionID == m.SectionID && a.Tanggal == id).ToList();
                        y.EMP = EMP.ConvertAll(a => new IDODetailAntibiotikDataModel()
                        {
                            Dosis = a.Dosis,
                            Id = a.Id_AntiBiotik_EMP,
                            Nama = a.AntiBiotikaName
                        });
                        item.Detail_List.Add(false, y);
                        i++;

                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        #endregion


        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new PPI_Entities())
                {
                    IQueryable<PPI_FtTRHeader_Result> proses = s.PPI_FtTRHeader(3).AsQueryable();
                    //if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(PPI_trIDOHAIs.Tanggal)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(PPI_FtTRHeader_Result.SectionID)}=@0", Request.Cookies["SectionIDPelayanan"].Value);
                    if (filter[14] != "True" && DateTime.Parse(filter[12]) != null && DateTime.Parse(filter[13]) != null)
                    {
                        proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).Date);
                        proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]).Date);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<IDODataModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.Tanggal_View2 = x.Tanggal.ToString("yyyy-MM-dd");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== S E L E C T 2

        [HttpPost]
        public string ListDokter(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mSupplier> proses = s.mSupplier.AsQueryable();
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(mSupplier.Nama_Supplier)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<mSupplier>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListJenisLuka(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmJenisLuka> proses = s.SIMmJenisLuka.AsQueryable();
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(SIMmJenisLuka.Deskripsi)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<SIMmJenisLuka>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListICD(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mICD> proses = s.mICD.AsQueryable();
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(mICD.Descriptions)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<mICD>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}