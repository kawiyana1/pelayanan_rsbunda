﻿using Pelayanan.Entities.PPI;
using Pelayanan.Models.PPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using iHos.MVC.Property;
using System.Data.SqlClient;
using Pelayanan.Models;
using System.Data.Entity.Validation;
using iHos.MVC.Converter;
using Pelayanan.Entities.SIM;

namespace Pelayanan.Controllers.PPI
{
    [Authorize(Roles = "Pelayanan")]
    public class TertusukJarumController : Controller
    {
        // GET: TertusukJarum
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            var user = (System.Security.Claims.ClaimsIdentity)User.Identity;
            var model = new TertusukJarumDataModel();

            model.Tanggal = DateTime.Today;
            model.UserName = user.FindFirstValue("FullName");
            model.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
            model.SectionName = Request.Cookies["SectionNamaPelayanan"].Value;

            using (var s = new PPI_Entities())
            {
                ViewData["SudahAdaData"] = s.PPI_trPemantauanTertusukJarum.Where(x => x.Tanggal == model.Tanggal && x.SectionID == model.SectionID).Count() > 0;
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new TertusukJarumDataModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new PPI_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var o = IConverter.Cast<PPI_trPemantauanTertusukJarum>(item);
                                o.Jam = DateTime.Now;
                                o.UserIDWeb = User.Identity.GetUserId();
                                o.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                                s.PPI_trPemantauanTertusukJarum.Add(o);
                                if (item.Detail_List == null) item.Detail_List = new ListDetail<TertusukJarumDetailDataModel>();

                                var m = s.PPI_FtGetHAIsHarianRI(o.SectionID);

                                item.Detail_List.RemoveAll(x => x.Remove);

                                foreach (var x in item.Detail_List)
                                {
                                    var xx = new PPI_trPemantauanTertusukJarum_Detail()
                                    {
                                        SectionID = o.SectionID,
                                        Tanggal = o.Tanggal,

                                        Keterangan = x.Model.Keterangan,
                                        KodePetugas = x.Model.KodePetugas,
                                        Kronologi = x.Model.Kronologi,
                                        NamaPetugas = x.Model.NamaPetugas,
                                        PenangananDengan = x.Model.PenangananDengan,
                                        TempatKejadian = x.Model.TempatKejadian,
                                        TempatTugas = x.Model.TempatTugas
                                    };
                                    s.PPI_trPemantauanTertusukJarum_Detail.Add(xx);
                                }

                                s.SaveChanges();

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"PPI TertusukJarum Create {item.Tanggal} & {item.SectionID}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListAB(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new PPI_Entities())
                {
                    IQueryable<PPI_FtGetDataTertusukJarum_Result> proses = s.PPI_FtGetDataTertusukJarum().AsQueryable();
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(PPI_FtGetDataTertusukJarum_Result.Kode_Supplier)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(PPI_FtGetDataTertusukJarum_Result.Nama_Supplier)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PPI_FtGetDataTertusukJarum_Result>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #region ===== D E T A I L

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(DateTime id)
        {
            TertusukJarumDataModel item;
            try
            {
                using (var s = new PPI_Entities())
                {
                    var sec = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = s.PPI_trPemantauanTertusukJarum.FirstOrDefault(x => x.Tanggal == id && x.SectionID == sec);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<TertusukJarumDataModel>(m);

                    var n = s.PPI_FtTRHeader(2).FirstOrDefault(x => x.Tanggal == id);
                    item.SectionName = n.SectionName;
                    item.UserName = n.UserCreate;

                    var d = s.PPI_trPemantauanTertusukJarum_Detail.Where(x => x.Tanggal == id && x.SectionID == m.SectionID).ToList();
                    item.Detail_List = new ListDetail<TertusukJarumDetailDataModel>();
                    var i = 1;
                    foreach (var x in d)
                    {
                        var y = IConverter.Cast<TertusukJarumDetailDataModel>(x);
                        y.Nomor = i;
                        item.Detail_List.Add(false, y);
                        i++;
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        #endregion


        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new PPI_Entities())
                {
                    IQueryable<PPI_FtTRHeader_Result> proses = s.PPI_FtTRHeader(2).AsQueryable();
                    //if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(PPI_trTertusukJarumHAIs.Tanggal)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(PPI_FtTRHeader_Result.SectionID)}=@0", Request.Cookies["SectionIDPelayanan"].Value);
                    if (filter[14] != "True" && DateTime.Parse(filter[12]) != null && DateTime.Parse(filter[13]) != null)
                    {
                        proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).Date);
                        proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]).Date);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<TertusukJarumDataModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.Tanggal_View2 = x.Tanggal.ToString("yyyy-MM-dd");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

    }
}