﻿using iHos.MVC.Converter;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    public class PasienKhususController : Controller
    {
        #region ===== P A S I E N  B E R M A S A L A H

        [HttpGet]
        public ActionResult PasienBermasalah(string id)
        {
            var item = new PasienBermasalahViewModel();
            try
            {
                using (var s = new SIM_Entities())
                {
                    var models = s.Vw_PasienBermasalah.Where(x => x.NRM == id);
                    if (models == null) return HttpNotFound();
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienBermasalahViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglInput_View = x.TglInput.ToString("dd/MM/yyyy HH:mm");
                    }
                    item.PasienBermasalahDetail = new List<PasienBermasalahViewModel>();
                    item.PasienBermasalahDetail = m;
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        #endregion
    }
}