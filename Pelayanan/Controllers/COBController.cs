﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Pelayanan.Controllers
{
    public class COBController : Controller
    {
        #region ==== C R E A T E
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg, string nrm, string namapasien)
        {
            var model = new COBViewModel();
            using (var s = new SIM_Entities())
            {
                model.NoReg = noreg;
                model.NRM = nrm;
                model.NamaPasien = namapasien;

                var cob = s.VW_Pelayanan_UpdatePasienCOB.FirstOrDefault(e => e.NoReg == noreg);
                if (cob != null)
                {
                    model = IConverter.Cast<COBViewModel>(cob);
                    var customerkerjasama = s.VW_CustomerKerjasama.FirstOrDefault(e => e.CustomerKerjasamaID == cob.PertanggunganKeduaCustomerKerjasamaID && e.Kode_Customer == cob.PertanggunganKeduaCompanyID);
                    if (customerkerjasama != null)
                    {
                        model.KelasID = customerkerjasama.KelasID;
                    }

                }
                
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post()
        {
            try
            {
                var item = new COBViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        if (item.PertanggunganKeduaCompanyID == "" || item.PertanggunganKeduaCompanyID == null) return JsonHelper.JsonMsgInfo("Perusahaan tidak boleh kosong");
                        if (item.PertanggunganKeduaNoKartu == "" || item.PertanggunganKeduaNoKartu == null) return JsonHelper.JsonMsgInfo("Nomor Kartu tidak boleh kosong");

                        var cus = s.SIMdAnggotaKerjasama.FirstOrDefault(x => x.NoAnggota == item.PertanggunganKeduaNoKartu);
                        if (cus == null)
                        {
                            return JsonHelper.JsonMsgInfo("Data Anggota belum terdaftar");

                        }

                        var customerkerjasama = s.VW_CustomerKerjasama.FirstOrDefault(e => e.Kode_Customer == item.PertanggunganKeduaCompanyID && e.KelasID == item.KelasID);
                        if (customerkerjasama != null)
                        {
                            item.PertanggunganKeduaCustomerKerjasamaID = Convert.ToInt32(customerkerjasama.CustomerKerjasamaID);
                        }

                        s.Pelayanan_UpdatePasienCOB(
                            item.NoReg,
                            true,
                            item.PertanggunganKeduaCompanyID,
                            item.PertanggunganKeduaCustomerKerjasamaID,
                            item.PertanggunganKeduaNoKartu
                        );

                        var insert = s.SaveChanges();

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Update COB {item.NoReg}"
                        };
                        UserActivity.InsertUserActivity(userActivity);

                        result = new ResultSS(1, insert);
                        

                    }

                    return new JavaScriptSerializer().Serialize(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion
    }
}