﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;   
using System.Web.Mvc;

namespace iHosComputer.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class SKLController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [ActionName("IndexList")]
        public ActionResult IndexList(string noreg, string section)
        {
            ViewBag.NoReg = noreg;
            ViewBag.Section = section;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();

        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
            
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new SKLViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        if (item.NamaSuami == null) throw new Exception("Nama ayah tidak boleh kosong");
                        if (item.DokterID == null) throw new Exception("Dokter Penolong tidak boleh kosong");

                        item.NoSKL = s.AutoNumber_Pelayanan_SIMtrSKL().FirstOrDefault();
                        var m = IConverter.Cast<SIMtrSKL>(item);
                        var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                        if (dokter != null) m.DokterPenolong = dokter.NamaDOkter;
                        m.UmurIstri_Thn = item.UmurIstri_Thn;
                        m.UmurIstri_Bln = item.UmurIstri_Bln;
                        s.SIMtrSKL.Add(m);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SKL Create {m.NoSKL}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (DbEntityValidationException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string code)
        {
            SKLViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_GetSKL.FirstOrDefault(x => x.NoSKL == code);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<SKLViewModel>(m);
                    var skl = s.SIMtrSKL.FirstOrDefault(x => x.NoSKL == code);
                    if (skl != null)
                    {
                        item.Alamat_Suami = skl.Alamat_Suami;
                        item.No_Identitas_Suami = skl.No_Identitas_Suami;
                    }
                    item.WukuIDNama = m.Wuku;
                    item.PancawaraIDNama = m.Pancawara;
                    item.Tanggal_View = item.Tanggal.ToString("dd/MM/yyyy");
                    item.JamLahir_View = item.JamLahir.ToString("HH") + ":" + item.JamLahir.ToString("mm");
                    item.Jam_View = item.Jam.ToString("HH") + ":" + item.JamLahir.ToString("mm");
                    item.TglLahir_View = item.TglLahir.ToString("dd/MM/yyyy");
                    item.TglReg_Istri_View = item.TglReg_Istri == null ? "" : item.TglReg_Istri.Value.ToString("dd/MM/yyyy");
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new SKLViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.SIMtrSKL.FirstOrDefault(x => x.NoSKL == item.NoSKL);
                        if (model == null) throw new Exception("Data Tidak ditemukan");
                        TryUpdateModel(model);
                        var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.DokterID);
                        model.DokterPenolong = dokter != null ? model.DokterPenolong = dokter.NamaDOkter : "";
                        model.UmurIstri_Thn = item.UmurIstri_Thn;
                        model.UmurIstri_Bln = item.UmurIstri_Bln;
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SKL Edit {model.NoSKL}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string code)
        {
            SKLViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_GetSKL.FirstOrDefault(x => x.NoSKL == code);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<SKLViewModel>(m);
                    var skl = s.SIMtrSKL.FirstOrDefault(x => x.NoSKL == code);
                    if (skl != null)
                    {
                        item.Alamat_Suami = skl.Alamat_Suami;
                        item.No_Identitas_Suami = skl.No_Identitas_Suami;
                    }
                    item.WukuIDNama = m.Wuku; 
                    item.PancawaraIDNama = m.Pancawara;
                    item.Tanggal_View = item.Tanggal.ToString("dd/MM/yyyy");
                    item.JamLahir_View = item.JamLahir.ToString("HH") + ":" + item.JamLahir.ToString("mm");
                    item.Jam_View = item.Jam.ToString("HH") + ":" + item.JamLahir.ToString("mm");
                    item.TglLahir_View = item.TglLahir.ToString("dd/MM/yyyy");
                    item.TglReg_Istri_View = item.TglReg_Istri == null ? "" : item.TglReg_Istri.Value.ToString("dd/MM/yyyy");
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string id)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        try
                        {
                            var m = s.SIMtrSKL.FirstOrDefault(x => x.NoSKL == id);
                            if (m == null) throw new Exception("Data Tidak ditemukan");
                            var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == m.NoRegAnak);
                            simtrreg.SKL = false;
                            s.SIMtrSKL.Remove(m);
                            result = new ResultSS(s.SaveChanges());
                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"SKL delete {id}"
                            };
                            UserActivity.InsertUserActivity(userActivity);
                            dbContextTransaction.Commit();
                        }
                        catch (SqlException ex)
                        {
                            dbContextTransaction.Rollback();
                            throw new Exception(ex.Message);
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            throw new Exception(ex.Message);
                        }
                    }
                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) ex = ex.InnerException;
                if (ex.InnerException != null) ex = ex.InnerException;
                return JsonHelper.JsonMsgError(ex);
            }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMtrSKL> proses = s.SIMtrSKL;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(SIMtrSKL.NoSKL)}.Contains(@0)", filter[0]);
                    if (IFilter.F_DateTime(filter[1]) != null) proses = proses.Where($"{nameof(SIMtrSKL.Tanggal)}=@0", IFilter.F_DateTime(filter[1]));
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(SIMtrSKL.NamaSuami)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(SIMtrSKL.NRM_Istri)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(SIMtrSKL.Nama_Istri)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[14])) proses = proses.Where($"{nameof(SIMtrSKL.NoReg)}.Contains(@0)", filter[14]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<SKLViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Jam_View = x.Jam.ToString("HH") + ":" + x.Jam.ToString("mm");
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListNoRegSKL(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_GetDataNoregSKL> proses = s.Pelayanan_GetDataNoregSKL;
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Pelayanan_GetDataNoregSKL.NoReg)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Pelayanan_GetDataNoregSKL.NRM)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Pelayanan_GetDataNoregSKL.NamaPasien)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Pelayanan_GetDataNoregSKL.JenisKelamin)}.Contains(@0)", filter[3]);
                    if (filter[9]=="True") proses.Where($"{nameof(Pelayanan_GetDataNoregSKL.SKL)}=@0", false);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<DataNoRegSKLViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglLahir_View = x.TglLahir == null ? "" : x.TglLahir.Value.ToString("yyyy-MM-dd");
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== G E T D A T A

        [HttpPost]
        public string GetData(string id)
        {
            try
            {
                SIMtrSKL m;
                using (var s = new SIM_Entities())
                {
                    m = s.SIMtrSKL.FirstOrDefault(x => x.NoSKL == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = IConverter.Cast<SKLViewModel>(m)
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C U S T O M
        [HttpGet]
        [ActionName("Input")]
        public ActionResult Input_Get(string noreg)
        {
            var item = new SKLViewModel();
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_GetSKL.FirstOrDefault(x => x.NoSKL == noreg);
                    if (m == null)
                    {
                        var dt = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg);
                        item.NoReg = noreg;
                        item.Nama_Istri = dt.NamaPasien;
                        item.TglReg_Istri_View = dt.TglReg.ToString("dd-MM-yyyy");
                        item.NRM_Istri = dt.NRM;
                        item.UmurIstri_Bln = (byte)dt.UmurBln;
                        item.UmurIstri_Thn = (short)dt.UmurThn;
                        item.Pekerjaan_Istri = "-";
                        item.Alamat_Istri = dt.Alamat;
                        item.Tanggal = DateTime.Today;
                        item.Jam = DateTime.Now;
                        ViewBag.Action = "Create";
                    }
                    else
                    {
                        ViewBag.Action = "Edit";
                        item = IConverter.Cast<SKLViewModel>(m);
                        item.Tanggal_View = item.Tanggal.ToString("dd/MM/yyyy");
                        item.JamLahir_View = item.JamLahir.ToString("HH") + ":" + item.JamLahir.ToString("mm");
                        item.Jam_View = item.Jam.ToString("HH") + ":" + item.JamLahir.ToString("mm");
                        item.TglLahir_View = item.TglLahir.ToString("dd/MM/yyyy");
                        item.TglReg_Istri_View = item.TglReg_Istri == null ? "" : item.TglReg_Istri.Value.ToString("dd/MM/yyyy");
                     
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        #endregion

        #region ==== R E P O R T

        public ActionResult PrintSKL(string id)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Reports"), $"Rpt_SKL.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_SKL", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoSKL", id));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_SKL;1"].SetDataSource(ds[i].Tables[0]);


                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        [HttpPost]
        public string ListPancawara(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmPancawara> proses = s.SIMmPancawara;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(SIMmPancawara.ID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(SIMmPancawara.Pancawara)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SIMmPancawara>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListWuku(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmWuku> proses = s.SIMmWuku;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(SIMmWuku.ID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(SIMmWuku.Wuku)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SIMmWuku>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}