﻿using CrystalDecisions.CrystalReports.Engine;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    public class PasienGejalaPinokController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            PasienGejalaPinokViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_Informasi_GejalaPinok.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<PasienGejalaPinokViewModel>(m);
                    item.Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy");
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new PasienGejalaPinokViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.SIMtrKasusGejalaPinok.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        if (model == null) throw new Exception("Data Tidak ditemukan");

                        model.JenisKasus = item.JenisKasus;
                        model.IndikasiKasus = item.IndikasiKasus;
                        model.Keterangan = item.Keterangan;

                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"PasienGejalaPinok Edit {model.NoBukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string id)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrKasusGejalaPinok.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    s.SIMtrKasusGejalaPinok.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"PasienGejalaPinok delete {id}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            PasienGejalaPinokViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_Informasi_GejalaPinok.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<PasienGejalaPinokViewModel>(m);
                    item.Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy");
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_Informasi_GejalaPinok> proses = s.Pelayanan_Informasi_GejalaPinok;
                    if (filter[10] != "True" && !string.IsNullOrEmpty(filter[11]) && !string.IsNullOrEmpty(filter[12]))
                    {
                        proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[11]));
                        proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[12]).AddDays(1));
                    }
                    if (!string.IsNullOrEmpty(filter[0]))  proses = proses.Where($"{nameof(Pelayanan_Informasi_GejalaPinok.NoBukti)}.Contains(@0)", filter[0]);
                    //proses = proses.Where($"{nameof(Pelayanan_Informasi_GejalaPinok.Tanggal)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Pelayanan_Informasi_GejalaPinok.JenisKerjasama)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Pelayanan_Informasi_GejalaPinok.NRM)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Pelayanan_Informasi_GejalaPinok.NamaPasien)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Pelayanan_Informasi_GejalaPinok.Alamat)}.Contains(@0)", filter[4]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_GejalaPinok.SectionID)}=@0", Request.Cookies["SectionIDPelayanan"].Value);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienGejalaPinokViewModel>(x));
                    foreach (var x in m) { x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy") + " " + x.Jam.TimeOfDay.ToString("hh") + ":" + x.Jam.TimeOfDay.ToString("mm"); }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}