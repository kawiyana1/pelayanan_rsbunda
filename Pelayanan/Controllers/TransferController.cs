﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class TransferController : Controller
    {
        public ActionResult Index(string noreg, string section, string statusbayar)
        {
            TransferViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    item = new TransferViewModel()
                    {
                        NoReg = noreg,
                        SectionId = section
                    };
                    var d = s.Pelayanan_Transfer_Get(section, noreg).ToList();
                    item.Detail_List = new ListDetail<TransferDetailViewModel>();
                    foreach (var x in d)
                    {
                        item.Detail_List.Add(false, new TransferDetailViewModel()
                        {
                            NamaDokter = x.NamaDokter,
                            NoUrut = x.NoUrut ?? 0,
                            SectionTujuanName = x.SectionName,
                            WaktuNama = x.Keterangan,
                            DokterId = x.DokterID,
                            SectionTujuanId = x.SectionTujuanID,
                            WaktuId = x.WaktuID ?? 0
                        });
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.StatusBayar = statusbayar;
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new TransferViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                if (item.Detail_List == null)
                                {
                                    throw new Exception("Detail tidak boleh kosong");
                                }
                                if (item.Detail_List.Count == 0)
                                {
                                    throw new Exception("Detail tidak boleh kosong");
                                }
                                #region Detail
                                if (item.Detail_List != null)
                                {
                                    item.Detail_List.RemoveAll(x => x.Remove);
                                    foreach (var x in item.Detail_List)
                                    {
                                        if (x.Model.RiOnly == "true")
                                        {
                                            if (x.Model.DokterIdSelect2 == "XX" || string.IsNullOrEmpty(x.Model.DokterIdSelect2))
                                                throw new Exception("RI harus input dokter");
                                            else
                                                x.Model.DokterId = x.Model.DokterIdSelect2;
                                        }
                                        s.Pelayanan_Transfer_Insert(
                                            item.SectionId,
                                            x.Model.SectionTujuanId,
                                            x.Model.DokterId,
                                            item.NoReg,
                                            x.Model.WaktuId,
                                            x.Model.WaktuNama
                                        );
                                    }
                                }
                                #endregion
                                result = new ResultSS(s.SaveChanges());

                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Transfer {item.NoReg} {item.SectionId}"
                                };
                                dbContextTransaction.Commit();
                            }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListTujuan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_Transfer_ListTujuan> proses = s.Pelayanan_Transfer_ListTujuan.Where(x => x.StatusAktif == true);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(Pelayanan_Transfer_ListTujuan.NamaSection)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[3]))
                        proses = proses.Where($"{nameof(Pelayanan_Transfer_ListTujuan.NamaDOkter)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[5]))
                        proses = proses.Where($"{nameof(Pelayanan_Transfer_ListTujuan.Keterangan)}.Contains(@0)", filter[5]);
                    if (IFilter.F_int(filter[6]) != null)
                        proses = proses.Where($"{nameof(Pelayanan_Transfer_ListTujuan.JmlAntrian)}=@0", IFilter.F_int(filter[6]) - 2);
                    proses = proses.Where($"{nameof(Pelayanan_Transfer_ListTujuan.SectionID)}<>@0", Request.Cookies["SectionIDPelayanan"].Value);
                    proses = proses.Where($"TipePelayanan=@0 OR TipePelayanan=@1", "RI", "RJ");
                    if (Request.Cookies["LoginTipe"].Value.ToUpper() == "RI")
                    {
                        proses = proses.Where($"{nameof(Pelayanan_Transfer_ListTujuan.RIOnly)}=@0", false);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList();//.ConvertAll(x => IConverter.Cast<Pelayanan_Transfer_ListTujuan>(x));
                    m.ForEach(x => x.JmlAntrian = x.JmlAntrian + 1);
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}