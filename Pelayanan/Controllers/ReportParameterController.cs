﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    public class ReportParameterController : Controller
    {
        // GET: ReportParameter
        [HttpPost]
        public string ListDokter(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mDokter> proses = s.mDokter;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(mDokter.Active)}=@0", true);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DokterViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListBarang(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_Barang_GetReport> proses = s.Vw_Barang_GetReport;
                    if (IFilter.F_int(filter[0]) != null)
                        proses = proses.Where($"{nameof(Vw_Barang_GetReport.Barang_ID)}=@0", IFilter.F_int(filter[0]));
                    proses = proses.Where($"{nameof(Vw_Barang_GetReport.Nama_Barang)}.Contains(@0)", filter[1]);
                    var secid = Request.Cookies["SectionIDPelayanan"].Value;
                    var msection = s.SIMmSection.FirstOrDefault(x => x.SectionID == secid);
                    proses = proses.Where($"{nameof(Vw_Barang_GetReport.Lokasi_ID)}=@0", msection.Lokasi_ID);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<Vw_Barang_GetReport>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListSection(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(SIMmSection.SectionID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(SIMmSection.SectionName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SIMmSection>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}