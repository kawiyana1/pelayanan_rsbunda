﻿using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using iHos.MVC.Converter;
using System.Data.SqlClient;
using iHos.MVC.Property;
using Newtonsoft.Json;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Data;
using System.Web.Script.Serialization;

namespace Pelayanan.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class MedicalRecordController : Controller
    {
        public ActionResult Index(string noreg, int nomor)
        {
            RegistrasiPasienViewModel model;
            using (var s = new SIM_Entities())
            {
                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                if (m == null) return HttpNotFound();
                model = IConverter.Cast<RegistrasiPasienViewModel>(m);
                model.TglReg_View = model.TglReg.ToString("dd/MM/yyyy");
                model.Tanggal_View = model.Tanggal.ToString("dd/MM/yyyy");
                model.Jam_View = model.Jam.ToString("HH\":\"mm");
            }
            return View(model);
        }

        #region ===== T A B L E

        [HttpPost]
        public string ListHistoriPenggajianAwal(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<EMR_VIEW_PENGKAJIAN_AWAL> proses = s.EMR_VIEW_PENGKAJIAN_AWAL;
                    proses = proses.Where($"{nameof(EMR_VIEW_PENGKAJIAN_AWAL.NoReg)}=@0", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(EMR_VIEW_PENGKAJIAN_AWAL.SectionID)}=@0", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<EMRHistoriPengkajianAwalViewModel>(x));
                    m.ForEach(x => x.TanggalUpdate_View = x.TanggalUpdate.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListPenggajianAwal(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMtrMasterDokumenRME> proses = s.SIMtrMasterDokumenRME;
                    proses = proses.Where($"{nameof(SIMtrMasterDokumenRME.SectionID)}=@0", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<EMRPenggajianAwalViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListHasilBacaRadiologi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<EMR_VIEW_RADIOLOGI_Result> proses = s.EMR_VIEW_RADIOLOGI(filter[11]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var n = models.ToList();
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<HasilBacaRadiologiViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListHasilBacaLaboratorium(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<EMR_VIEW_LABORATORIUM> proses = s.EMR_VIEW_LABORATORIUM;
                    proses = proses.Where($"{nameof(EMR_VIEW_LABORATORIUM.RegNo)}=@0", filter[11]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<HasilBacaLaboratoriumViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListViewObat(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<EMR_VIEW_OBAT> proses = s.EMR_VIEW_OBAT;
                    proses = proses.Where($"{nameof(EMR_VIEW_OBAT.NoReg)}=@0", filter[11]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ViewObatViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L

        [HttpGet]
        public ActionResult DetailHasilBacaHadiologi(string id, string noreg, int nomor)
        {
            HasilBacaRadiologiViewModel result;
            try
            {
                using (var service = new SIM_Entities())
                {
                    var m = service.EMR_VIEW_RADIOLOGI(noreg).Where(x => x.NoBukti == id && x.Nomor == nomor).FirstOrDefault();
                    var d = service.EMR_VIEW_RADIOLOGI_DETAIL(id).Where(x => x.RegNo == noreg && x.Nomor == nomor).ToList();
                    result = IConverter.Cast<HasilBacaRadiologiViewModel>(m);
                    var pasien = service.VW_DataPasienReg_Pelayanan.FirstOrDefault(e => e.NoReg == m.RegNo);
                    if(pasien != null)
                    {
                        result.NamaPasien = pasien.NamaPasien;
                        result.Gender = pasien.JenisKelamin + " / " + pasien.UmurThn;
                        result.NRM = pasien.NRM;
                        ViewBag.Tanggal = m.Tanggal.Value.ToString("dd-MM-yyyy");
                    }

                    result.Detail_List = new List<HasilBacaRadiologiDetailViewModel>();
                    foreach (var x in d)
                    {
                        var t = IConverter.Cast<HasilBacaRadiologiDetailViewModel>(x);
                        t.Tanggal_View = t.Tanggal.ToString("dd/MM/yyyy");
                        t.TglInput_View = t.TglInput != null ? t.TglInput.Value.ToString("dd/MM/yyyy") : "";
                        result.Detail_List.Add(t);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return PartialView(result);
        }

        [HttpGet]
        public ActionResult DetailLaboratorium(string id, string noreg)
        {
            HasilBacaLaboratoriumViewModel result;
            try
            {
                using (var service = new SIM_Entities())
                {
                    var m = service.EMR_VIEW_LABORATORIUM.Where(x => x.NoSystem == id && x.RegNo == noreg).FirstOrDefault();
                    var d = service.EMR_VIEW_LABORATORIUM_DETAIL.Where(x => x.NoSystem == id && x.RegNo == noreg).OrderBy(x => x.KategoriTestNama).ToList();
                    result = IConverter.Cast<HasilBacaLaboratoriumViewModel>(m);
                    result.Detail_List = new List<HasilBacaLaboratoriumDetailViewModel>();
                    foreach (var x in d) {
                        var t = IConverter.Cast<HasilBacaLaboratoriumDetailViewModel>(x);
                        t.Tanggal_View = t.Tanggal.ToString("dd/MM/yyyy");
                        result.Detail_List.Add(t);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return PartialView(result);
        }

        [HttpGet]
        public ActionResult DetailObat(string id, string noreg)
        {
            ViewObatViewModel result;
            try
            {
                using (var service = new SIM_Entities())
                {
                    var m = service.EMR_VIEW_OBAT.Where(x => x.NoBukti == id && x.NoReg == noreg).FirstOrDefault();
                    var d = service.EMR_VIEW_OBAT_DETAIL.Where(x => x.NoBukti == id && x.NoReg == noreg).ToList();
                    result = IConverter.Cast<ViewObatViewModel>(m);
                    result.Detail_List = new List<ViewObatDetailViewModel>();
                    foreach (var x in d)
                    {
                        var t = IConverter.Cast<ViewObatDetailViewModel>(x);
                        t.Tanggal_View = t.Tanggal.ToString("dd/MM/yyyy");
                        result.Detail_List.Add(t);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return PartialView(result);
        }

        #endregion

        #region ===== S O A P

        [HttpPost]
        public string ListSOAP(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_SIMtrSOAP> proses = s.Pelayanan_SIMtrSOAP;
                    proses = proses.Where($"{nameof(Pelayanan_SIMtrSOAP.NRM)}=@0", filter[10]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<SOAPViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TanggalInput_View = x.TanggalInput.ToString("HH") + ":" + x.TanggalInput.ToString("mm");
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string CreateSoap(string noreg, int nomor, string sectionid, string soapdokter, string soaps, string soapo, string soapa, string soapp)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        s.SIMtrSOAP.Add(new SIMtrSOAP()
                        {
                            A = soapa,
                            DokterID = soapdokter,
                            O = soapo,
                            P = soapp,
                            S = soaps,
                            SectionID = sectionid,
                            Tanggal = DateTime.Today,
                            TanggalInput = DateTime.Now,
                            Nomor = nomor,
                            NoReg = noreg
                        });
                        s.SaveChanges();
                    }
                    return new JavaScriptSerializer().Serialize(new
                    {
                        IsSuccess = true
                    });
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult SOAPExportPDF(string id)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Reports"), "SOAP.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_SOAP", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@id", id));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables["SIM_Rpt_SOAP;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", $"SOAP {id}.pdf");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpPost]
        public string SaveResumeMedis(List<SelectListItemViewModel> diagnosaawal, string noReg, Nullable<int> nomor, 
            Nullable<bool> sudahPeriksa, Nullable<bool> pX_Keluar_Sembuh, Nullable<bool> pX_Keluar_BelumSembuh, 
            Nullable<bool> pX_Keluar_Membaik, Nullable<bool> pxMeninggal, Nullable<bool> meninggalSblm48, 
            Nullable<bool> meninggalStl48, Nullable<System.DateTime> meninggalTgl, Nullable<System.DateTime> meninggalJam, 
            Nullable<bool> pxKeluar_Pulang, Nullable<bool> pxKeluar_Dirujuk, Nullable<bool> pxKeluar_PlgPaksa, 
            Nullable<bool> pX_Keluar_DirujukAtas, Nullable<bool> pX_Keluar_DirujukBawah, string dokterRawatID, 
            string diagnosaAkhirID, string diagnosaAwalID, string sectionID, string emergency,
            bool isemergency,
            TimeSpan? waktupemeriksaanmulai, 
            TimeSpan? waktupemeriksaanselesai, 
            bool belumkeluarrs, 
            string dirujukketerangantujuan,
            string dirujukketeranganalasan, 
            bool? potensioperasi)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    s.Pelayanan_ResumeMedis(
                        noReg,
                        nomor, 
                        sudahPeriksa, 
                        pX_Keluar_Sembuh, 
                        pX_Keluar_BelumSembuh, 
                        pX_Keluar_Membaik, 
                        pxMeninggal,
                        meninggalSblm48, 
                        meninggalStl48, 
                        meninggalTgl, 
                        meninggalJam, 
                        pxKeluar_Pulang, 
                        pxKeluar_Dirujuk, 
                        pxKeluar_PlgPaksa, 
                        pX_Keluar_DirujukAtas, 
                        pX_Keluar_DirujukBawah, 
                        dokterRawatID,
                        diagnosaAkhirID,
                        sectionID,
                        emergency,
                        waktupemeriksaanmulai != null ? DateTime.Today + waktupemeriksaanmulai.Value : (DateTime?)null,
                        potensioperasi,
                        isemergency,
                        waktupemeriksaanselesai != null ? DateTime.Today + waktupemeriksaanselesai.Value : (DateTime?)null,
                        dirujukketerangantujuan,
                        dirujukketeranganalasan
                    );

                    var _m = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == noReg && x.Nomor == nomor);
                    _m.PX_Keluar_BelumKeluarRS = belumkeluarrs;
                    //_m.PxKeluar_Dirujuk_Ket = dirujukketerangan;

                    s.SIMtrDiagnosa.RemoveRange(s.SIMtrDiagnosa.Where(x => x.NoReg == noReg && x.SectionID == sectionID));
                    if (diagnosaawal == null) {
                        throw new Exception("Diagnosa Tidak boleh kosong");
                    } else { 
                        foreach (var x in diagnosaawal)
                        {
                            if (x.Value == null)
                            {
                                throw new Exception("Diagnosa Tidak boleh kosong");
                            }
                            s.SIMtrDiagnosa.Add(new SIMtrDiagnosa()
                            {
                                NoReg = noReg,
                                Nomor_DataReg =(int)nomor,
                                SectionID = sectionID,
                                Ditanggung = false,
                                KodeICD = x.Value,
                                Keterangan = x.Text,
                                TipeDiagnosa = x.Data1,
                                NoBukti = "-",
                                Tanggal = DateTime.Today,
                                Jam = DateTime.Today + waktupemeriksaanmulai
                            });
                        }
                    }

                    s.BPJS_Task_Insert(
                        noReg,
                        "TaskNo_5",
                        DateTime.Now,
                        DateTime.Now,
                        DateTime.Now
                     );

                    s.SaveChanges();
                }
                return new JavaScriptSerializer().Serialize(new
                {
                    IsSuccess = true
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}