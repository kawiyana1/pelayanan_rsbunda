﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class SensusHarianController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            var item = new SensusHarianViewModel();
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            using (var s = new SIM_Entities())
            {
                string section = Request.Cookies["SectionIDPelayanan"].Value;
                var tgl = DateTime.Today;

                var vw = s.Pelayanan_ViewSensusHarian.FirstOrDefault(x => x.SectionID == section && x.Tanggal == tgl);
                if (vw != null)
                {
                    ViewBag.SudahPerTanggal = true;
                    if (Request.IsAjaxRequest())
                        return PartialView(item);
                    else
                        return View(item);
                }

                var data = s.Pelayanan_GetSensusHarian.FirstOrDefault(x => x.SectionID == section && x.StatusSensus == "Pasien Aktif");
                if (data == null)
                {
                    ViewBag.Error = "Tidak ada data harian";
                    if (Request.IsAjaxRequest())
                        return PartialView(item);
                    else
                        return View(item);
                }
                item.JmlBed = data.JmlBed ?? 0;
                item.JmlBedTerisi = data.Terisi ?? 0;

                var kamar = s.Pelayanan_GetSensusHarian.Where(x => x.SectionID == section && x.StatusSensus == "Pasien Aktif").ToList();
                var pasienbaru = s.Pelayanan_GetSensusHarian.Where(x => x.SectionID == section && x.StatusSensus == "Pasien Baru").ToList();
                var pasienlama = s.Pelayanan_GetSensusHarian.Where(x => x.SectionID == section && x.StatusSensus == "Pasien Lama").ToList();
                var pasienkeluar = s.Pelayanan_GetSensusHarian.Where(x => x.SectionID == section && x.StatusSensus == "Pasien Keluar").ToList();

                item.DetailKamar_List = new ListDetail<SensusHarianDetailViewModel>();
                item.DetailPasienBaru_List = new ListDetail<SensusHarianDetailViewModel>();
                item.DetailPasienKeluar_List = new ListDetail<SensusHarianDetailViewModel>();
                item.DetailPasienLama_List = new ListDetail<SensusHarianDetailViewModel>();

                foreach (var x in kamar)
                {
                    item.DetailKamar_List.Add(false, new SensusHarianDetailViewModel()
                    {
                        DokterRawatID = x.DokterRawatID,
                        KelasAsalID = x.KelasAsalID,
                        KelasID = x.KelasID,
                        NoBed = x.NoBed,
                        NoKamar = x.Kamar,
                        NoReg = x.NoReg,
                        OK = false,
                        RawatBersama = false,
                        RawatBersamaBayi = false,
                        SectionID = x.SectionID,
                        SpesialisID = x.SpesialisasiID,
                        StatusPasien = "",
                        NRM = x.NRM,
                        NamaPasien = x.NamaPasien,
                        TitipKelas = x.Titip
                    });
                }
                foreach (var x in pasienbaru)
                {
                    item.DetailPasienBaru_List.Add(false, new SensusHarianDetailViewModel()
                    {
                        DokterRawatID = x.DokterRawatID,
                        KelasAsalID = x.KelasAsalID,
                        KelasID = x.KelasID,
                        NoBed = x.NoBed,
                        NoKamar = x.Kamar,
                        NoReg = x.NoReg,
                        OK = false,
                        RawatBersama = false,
                        RawatBersamaBayi = false,
                        SectionID = x.SectionID,
                        SpesialisID = x.SpesialisasiID,
                        StatusPasien = "",
                        NRM = x.NRM,
                        NamaPasien = x.NamaPasien,
                        TitipKelas = x.Titip
                    });
                }
                foreach (var x in pasienkeluar)
                {
                    item.DetailPasienKeluar_List.Add(false, new SensusHarianDetailViewModel()
                    {
                        DokterRawatID = x.DokterRawatID,
                        KelasAsalID = x.KelasAsalID,
                        KelasID = x.KelasID,
                        NoBed = x.NoBed,
                        NoKamar = x.Kamar,
                        NoReg = x.NoReg,
                        OK = false,
                        RawatBersama = false,
                        RawatBersamaBayi = false,
                        SectionID = x.SectionID,
                        SpesialisID = x.SpesialisasiID,
                        StatusPasien = "",
                        NRM = x.NRM,
                        NamaPasien = x.NamaPasien,
                        TitipKelas = x.Titip
                    });
                }
                foreach (var x in pasienlama)
                {
                    item.DetailPasienLama_List.Add(false, new SensusHarianDetailViewModel()
                    {
                        DokterRawatID = x.DokterRawatID,
                        KelasAsalID = x.KelasAsalID,
                        KelasID = x.KelasID,
                        NoBed = x.NoBed,
                        NoKamar = x.Kamar,
                        NoReg = x.NoReg,
                        OK = false,
                        RawatBersama = false,
                        RawatBersamaBayi = false,
                        SectionID = x.SectionID,
                        SpesialisID = x.SpesialisasiID,
                        StatusPasien = "",
                        NRM = x.NRM,
                        NamaPasien = x.NamaPasien,
                        TitipKelas = x.Titip
                    });
                }
            }
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            var item = new SensusHarianViewModel();
            TryUpdateModel(item);
            if (ModelState.IsValid)
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        try
                        {
                            #region Validation
                            if (item.DetailKamar_List == null) item.DetailKamar_List = new ListDetail<SensusHarianDetailViewModel>();
                            if (item.DetailPasienBaru_List == null) item.DetailPasienBaru_List = new ListDetail<SensusHarianDetailViewModel>();
                            if (item.DetailPasienLama_List == null) item.DetailPasienLama_List = new ListDetail<SensusHarianDetailViewModel>();
                            if (item.DetailPasienKeluar_List == null) item.DetailPasienKeluar_List = new ListDetail<SensusHarianDetailViewModel>();
                            item.DetailKamar_List.RemoveAll(x => x.Remove);
                            item.DetailPasienBaru_List.RemoveAll(x => x.Remove);
                            item.DetailPasienLama_List.RemoveAll(x => x.Remove);
                            item.DetailPasienKeluar_List.RemoveAll(x => x.Remove);
                            #endregion

                            var error = 0;

                            string section = Request.Cookies["SectionIDPelayanan"].Value;
                            var data = s.Pelayanan_GetSensusHarian.FirstOrDefault(x => x.SectionID == section && x.StatusSensus == "Pasien Aktif");

                            var m = new SIMtrSensusHarian()
                            {
                                Tanggal = DateTime.Today,
                                Jam = DateTime.Now,
                                BOR = 0,
                                SectionID = section,
                                UserID = 1103,
                                JmlBed = data.JmlBed ?? 0,
                                JmlBedTerisi = data.Terisi ?? 0
                            };
                            s.SIMtrSensusHarian.Add(m);

                            #region Detail
                            var kamar = s.Pelayanan_GetSensusHarian.Where(x => x.SectionID == section && x.StatusSensus == "Pasien Aktif").ToList();
                            foreach (var x in kamar)
                            {
                                var OK = false;
                                var get_OK = item.DetailKamar_List.FirstOrDefault(y => y.Model.NoReg == x.NoReg);
                                if (get_OK != null) OK = get_OK.Model.OK;

                                var n = new SIMtrSensusHarianDetail()
                                {
                                    DokterRawatID = x.DokterRawatID == null ? "XX" : x.DokterRawatID,
                                    KelasAsalID = x.KelasAsalID,
                                    KelasID = x.KelasID,
                                    NoBed = x.NoBed,
                                    NoKamar = x.Kamar,
                                    NoReg = x.NoReg,
                                    RawatBersama = false,
                                    RawatBersamaBayi = false,
                                    SectionID = x.SectionID,
                                    SpesialisID = x.SpesialisasiID == null ? "XX" : x.SpesialisasiID,
                                    Tanggal = DateTime.Today,
                                    TitipKelas = x.Titip,
                                    OK = OK
                                };
                                s.SIMtrSensusHarianDetail.Add(n);
                            }
                            #endregion

                            #region Detail
                            var pasienbaru = s.Pelayanan_GetSensusHarian.Where(x => x.SectionID == section && x.StatusSensus == "Pasien Baru").ToList();
                            foreach (var x in pasienbaru)
                            {
                                var statusSesus = item.DetailPasienBaru_List.FirstOrDefault(y => y.Model.NoReg == x.NoReg).Model.StatusPasien;
                                if (statusSesus == null || statusSesus == "") error += 1;

                                var n = new SIMtrSensusHarianDetail_Baru()
                                {
                                    DokterRawatID = x.DokterRawatID == null ? "XX" : x.DokterRawatID,
                                    KelasAsalID = x.KelasAsalID,
                                    KelasID = x.KelasID,
                                    NoBed = x.NoBed,
                                    NoKamar = x.Kamar,
                                    NoReg = x.NoReg,
                                    RawatBersama = false,
                                    RawatBersamaBayi = false,
                                    SectionID = x.SectionID,
                                    SpesialisID = x.SpesialisasiID == null ? "XX" : x.SpesialisasiID,
                                    Tanggal = DateTime.Today,
                                    TitipKelas = x.Titip,
                                    OK = item.DetailPasienBaru_List.FirstOrDefault(y => y.Model.NoReg == x.NoReg).Model.OK,
                                    StatusPasien = item.DetailPasienBaru_List.FirstOrDefault(y => y.Model.NoReg == x.NoReg).Model.StatusPasien
                                };
                                s.SIMtrSensusHarianDetail_Baru.Add(n);
                            }
                            #endregion

                            #region Detail
                            var pasienlama = s.Pelayanan_GetSensusHarian.Where(x => x.SectionID == section && x.StatusSensus == "Pasien Lama").ToList();
                            foreach (var x in pasienlama)
                            {
                                var n = new SIMtrSensusHarianDetail_Lama()
                                {
                                    DokterRawatID = x.DokterRawatID == null ? "XX" : x.DokterRawatID,
                                    KelasAsalID = x.KelasAsalID,
                                    KelasID = x.KelasID,
                                    NoBed = x.NoBed,
                                    NoKamar = x.Kamar,
                                    NoReg = x.NoReg,
                                    RawatBersama = false,
                                    RawatBersamaBayi = false,
                                    SectionID = x.SectionID,
                                    SpesialisID = x.SpesialisasiID == null ? "XX" : x.SpesialisasiID,
                                    Tanggal = DateTime.Today,
                                    TitipKelas = x.Titip,
                                    OK = item.DetailPasienLama_List.FirstOrDefault(y => y.Model.NoReg == x.NoReg).Model.OK
                                };
                                s.SIMtrSensusHarianDetail_Lama.Add(n);
                            }
                            #endregion

                            #region Detail
                            var pasienkeluar = s.Pelayanan_GetSensusHarian.Where(x => x.SectionID == section && x.StatusSensus == "Pasien Keluar").ToList();
                            foreach (var x in pasienkeluar)
                            {
                                var statusSesus = item.DetailPasienKeluar_List.FirstOrDefault(y => y.Model.NoReg == x.NoReg).Model.StatusPasien;
                                if (statusSesus == null || statusSesus == "") error += 1;

                                var n = new SIMtrSensusHarianDetail_Keluar()
                                {
                                    DokterRawatID = x.DokterRawatID == null ? "XX" : x.DokterRawatID,
                                    KelasAsalID = x.KelasAsalID,
                                    KelasID = x.KelasID,
                                    NoBed = x.NoBed,
                                    NoKamar = x.Kamar,
                                    NoReg = x.NoReg,
                                    RawatBersama = false,
                                    RawatBersamaBayi = false,
                                    SectionID = x.SectionID,
                                    SpesialisID = x.SpesialisasiID == null ? "XX" : x.SpesialisasiID,
                                    Tanggal = DateTime.Today,
                                    TitipKelas = x.Titip,
                                    OK = item.DetailPasienKeluar_List.FirstOrDefault(y => y.Model.NoReg == x.NoReg).Model.OK,
                                    StatusPasien = item.DetailPasienKeluar_List.FirstOrDefault(y => y.Model.NoReg == x.NoReg).Model.StatusPasien
                                };
                                s.SIMtrSensusHarianDetail_Keluar.Add(n);
                            }
                            #endregion

                            if (error > 0)
                            {
                                return JsonHelper.JsonMsgInfo("Data status pasien baru atau keluar ada yang kosong.");
                            }

                            result = new ResultSS(s.SaveChanges());

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"Sensus Harian Create {DateTime.Today.ToString("yyyy-MM-dd")} {item.SectionID}"
                            };
                            UserActivity.InsertUserActivity(userActivity);
                            dbContextTransaction.Commit();
                        }
                        catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); return JsonHelper.JsonMsgError(StaticModel.DbEntityValidationExceptionToString(ex)); }
                        catch (SqlException ex) { dbContextTransaction.Rollback(); return JsonHelper.JsonMsgError(ex); }
                        catch (Exception ex) { dbContextTransaction.Rollback(); return JsonHelper.JsonMsgError(ex); }
                    }
                }
                return JsonHelper.JsonMsgCreate(result, -1);
            }
            else
                return JsonHelper.JsonMsgError(ViewData);
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(DateTime tanggal, string sectionid)
        {
            SensusHarianViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_ViewSensusHarianNew(tanggal, sectionid).FirstOrDefault(x => x.Tanggal == tanggal && x.SectionID == sectionid);
                    if (m == null) return HttpNotFound();
                    item = new SensusHarianViewModel()
                    {
                        JmlBed = m.JmlBed,
                        JmlBedTerisi = m.JmlBedTerisi,
                        Tanggal = m.Tanggal,
                        Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy")
                    };

                    var kamar = s.Pelayanan_ViewSensusHarianNew(tanggal, sectionid).Where(e => e.Aktif == 1).ToList();
                    item.DetailKamar_List = new ListDetail<SensusHarianDetailViewModel>();
                    foreach (var x in kamar)
                    {
                        item.DetailKamar_List.Add(false, new SensusHarianDetailViewModel()
                        {
                            NRM = x.NRM,
                            NamaPasien = x.NamaPasien,
                            NoKamar = x.NoKamar,
                            NoBed = x.NoBed,
                            NoReg = x.NoReg,
                            OK = (bool)x.OK_Aktif
                        });
                    }

                    var lama = s.Pelayanan_ViewSensusHarianNew(tanggal, sectionid).Where(x => x.Lama == true).ToList();
                    item.DetailPasienLama_List = new ListDetail<SensusHarianDetailViewModel>();
                    foreach (var x in lama)
                    {
                        item.DetailPasienLama_List.Add(false, new SensusHarianDetailViewModel()
                        {
                            NRM = x.NRM,
                            NamaPasien = x.NamaPasien,
                            NoKamar = x.NoKamar,
                            NoBed = x.NoBed,
                            NoReg = x.NoReg,
                            OK = x.OK_Lama ?? false,
                        });
                    }

                    var baru = s.Pelayanan_ViewSensusHarianNew(tanggal, sectionid).Where(x => x.Baru == true).ToList();
                    item.DetailPasienBaru_List = new ListDetail<SensusHarianDetailViewModel>();
                    foreach (var x in baru)
                    {
                        item.DetailPasienBaru_List.Add(false, new SensusHarianDetailViewModel()
                        {
                            NRM = x.NRM,
                            NamaPasien = x.NamaPasien,
                            NoKamar = x.NoKamar,
                            NoBed = x.NoBed,
                            NoReg = x.NoReg,
                            OK = x.OK_Baru ?? false,
                            StatusPasien = x.StatusPasienBaru
                        });
                    }

                    var keluar = s.Pelayanan_ViewSensusHarianNew(tanggal, sectionid).Where(x => x.Keluar == true).ToList();
                    item.DetailPasienKeluar_List = new ListDetail<SensusHarianDetailViewModel>();
                    foreach (var x in keluar)
                    {
                        item.DetailPasienKeluar_List.Add(false, new SensusHarianDetailViewModel()
                        {
                            NRM = x.NRM,
                            NamaPasien = x.NamaPasien,
                            NoKamar = x.NoKamar,
                            NoBed = x.NoBed,
                            NoReg = x.NoReg,
                            OK = x.OK_Keluar ?? false,
                            StatusPasien = x.StatusPasienKeluar
                        });
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(DateTime tanggal, string sectionid)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    if (tanggal != DateTime.Today) throw new Exception("Hanya boleh menghapus per tanggal sekarang");
                    var r = s.Pelayanan_SensusHarian_Delete(tanggal, sectionid);
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Sensus Harian delete {tanggal.ToString("yyyy-MM-dd")} {sectionid}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMtrSensusHarian> proses = s.SIMtrSensusHarian;
                    if (IFilter.F_bool(filter[10]) == false && IFilter.F_DateTime(filter[11]) != null && IFilter.F_DateTime(filter[12]) != null)
                    {
                        proses = proses.Where($"{nameof(SIMtrSensusHarian.Tanggal)}>=@0", IFilter.F_DateTime(filter[11]).Value.AddDays(-1));
                        proses = proses.Where($"{nameof(SIMtrSensusHarian.Tanggal)}<=@0", IFilter.F_DateTime(filter[12]));
                    }
                    string section = Request.Cookies["SectionIDPelayanan"].Value;
                    proses = proses.Where($"{nameof(SIMtrSensusHarian.SectionID)}=@0", section);

                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<SensusHarianViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.Tanggal_View2 = x.Tanggal.ToString("yyyy-MM-dd");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}