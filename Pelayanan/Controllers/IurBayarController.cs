﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;


namespace Pelayanan.Controllers
{
    public class IurBayarController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg)
        {
            var model = new IurBayarViewModel();
            using (var s = new SIM_Entities())
            {
                var c = s.trIURBayar.FirstOrDefault(x => x.NoReg == noreg);
                if(c == null)
                {
                    var m = s.GetDataIURBayar.FirstOrDefault(x => x.NoReg == noreg);
                    if (m == null) return HttpNotFound();
                    model = IConverter.Cast<IurBayarViewModel>(m);

                    ViewBag.Report = 0;
                }
                else
                {
                    var m = s.IURBayarTersimpan(noreg).FirstOrDefault();
                    if (m != null)
                    {
                        model = IConverter.Cast<IurBayarViewModel>(m);
                        model.NamaDiagnosaUtama = m.DiagnosaUtama;
                        model.NamaDiagnosaSkunder = m.DiagnosaSekunder;
                        model.NamaKodeTindakan = m.DiagnosaTindakan;
                        model.NilaiINACBGs_View = m.NilaiINACBGs.Value.ToMoney();
                        model.IURBayar_View = m.IURBayar.Value.ToMoney();
                    }
                    ViewBag.Report = 1;

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new IurBayarViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var c = s.trIURBayar.FirstOrDefault(x => x.NoReg == item.NoReg);
                        if(c == null)
                        {
                            var iur = IConverter.Cast<trIURBayar>(item);
                            iur.Tanggal = DateTime.Now;
                            iur.NilaiINACBGs = item.NilaiINACBGs_View.ToDecimal();
                            iur.IURBayar = item.IURBayar_View.ToDecimal();
                            s.trIURBayar.Add(iur);

                            var regis = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                            if (regis != null)
                            {
                                regis.IURBayar = item.IURBayar_View.ToDecimal();
                                regis.TarifINA = item.NilaiINACBGs_View.ToDecimal();
                            }
                            result = new ResultSS(s.SaveChanges());

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"Iur Bayar Create {item.NoReg}"
                            };
                            UserActivity.InsertUserActivity(userActivity);
                        }
                        else
                        {
                            c.NilaiINACBGs = item.NilaiINACBGs_View.ToDecimal();
                            c.IURBayar = item.IURBayar_View.ToDecimal();
                            c.KodeDiagnosaUtama = item.KodeDiagnosaUtama;
                            c.KodeDiagnosaSkunder = item.KodeDiagnosaSkunder;
                            c.KodeTindakan = item.KodeTindakan;
                            c.Presentase = item.Presentase;
                            c.JamKeluar = item.JamKeluar;
                            c.JumlahHariRawat = item.JumlahHariRawat;
                            c.LamaRawat = item.LamaRawat;
                            TryUpdateModel(item);

                            var regis = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                            if (regis != null)
                            {
                                regis.IURBayar = item.IURBayar_View.ToDecimal();
                                regis.TarifINA = item.NilaiINACBGs_View.ToDecimal();
                            }

                            result = new ResultSS(s.SaveChanges());

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"Iur Bayar Create {item.NoReg}"
                            };
                            UserActivity.InsertUserActivity(userActivity);
                        }

                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== G E T  B I A Y A  T A M B A H A N 
        [HttpPost]
        public string getDataBiayaTambahan(string kelas, decimal tarif, int lama_rawat, DateTime jam_pulang)
        {
            try
            {
                using (var s = new SIM_Entities())
                {

                    var m = s.HitungDataIURBayar(tarif, lama_rawat, kelas, jam_pulang).FirstOrDefault();
                    if (m != null)
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Data = m,
                            Message = ""
                        });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = "",
                            Message = "Data tidak ditemukan"
                        });
                    }

                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== R E P O R T

        public ActionResult ExportPDF(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Reports"), $"Rpt_GetDataIURBayar.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();
                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_GetDataIURBayar", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));

                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                    i++;

                    cmd.Add(new SqlCommand($"select * from sReport", conn));
                    cmd[i].CommandType = CommandType.Text;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion
    }

}