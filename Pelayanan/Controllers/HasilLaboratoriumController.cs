﻿using CrystalDecisions.CrystalReports.Engine;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    public class HasilLaboratoriumController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            HasilLaboratoriumViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.EMR_VIEW_LABORATORIUM_DETAIL.FirstOrDefault(x => x.NoSystem == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<HasilLaboratoriumViewModel>(m);
                    item.Tanggal_View = m.Tanggal == null ? "" : m.Tanggal.Value.ToString("dd/MM/yyyy");
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_Informasi_HasilLaboratorium> proses = s.Pelayanan_Informasi_HasilLaboratorium;
                    if (filter[10] != "True")
                    {
                        proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[11]));
                        proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[12]));
                    }
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_HasilLaboratorium.NoSystem)}.Contains(@0)", filter[0]);
                    //proses = proses.Where($"{nameof(Pelayanan_Informasi_HasilLaboratorium.Tanggal)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_HasilLaboratorium.NRM)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_HasilLaboratorium.NamaPasien)}.Contains(@0)", filter[3]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_HasilLaboratorium.Alamat)}.Contains(@0)", filter[4]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_HasilLaboratorium.JenisPasien)}.Contains(@0)", filter[5]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_HasilLaboratorium.DokterPengirim)}.Contains(@0)", filter[6]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<HasilLaboratoriumViewModel>(x));
                    foreach (var x in m) { x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"); }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}