﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    public class SplitBillController : Controller
    {
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg)
        {
            var model = new SplitBillViewModel();
            model.Reg = new RegistrasiPasienViewModel();
            using (var s = new SIM_Entities()) 
            {
                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg);
                if (m == null) return HttpNotFound();
                model.Reg = IConverter.Cast<RegistrasiPasienViewModel>(m);
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }


        [HttpGet]
        public string get_detailSplitBill(string noreg, bool mcu)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    var model = new List<getdetailSplitBilling>();
                    var m = s.Pelayanan_GetSplitBill_all(noreg).Where(x => x.SplitBill == mcu).ToList();
                    foreach(var x in m)
                    {
                        model.Add(new getdetailSplitBilling()
                        {
                            Tanggal_View = x.Tanggal.Value.ToString("dd/MM/yyyy"),
                            NoBukti = x.NoBukti,
                            SectionName = x.SectionName,
                            Total_View = Convert.ToDecimal(x.Total).ToMoney()
                        });
                    }
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            data = model,
                            countAll = model.Count()
                        },
                        Message = ""
                    });
                }
                
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string update_splitbill(List<insertSplitBilling> id)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    foreach (var x in id)
                    {
                        var m = s.Pelayanan_GetSplitBill_UpdateTransaksi(x.nobukti, x.mcu);
                    }
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = "",
                        Message = ""
                    });
                }

            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        //---------------


        [HttpGet]
        public string get_detailCOB(string noreg, bool mcu)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    var model = new List<getdetailSplitBilling>();
                    var m = s.Pelayanan_GetCOB_all(noreg).Where(x => x.COB == mcu).ToList();
                    foreach (var x in m)
                    {
                        model.Add(new getdetailSplitBilling()
                        {
                            Tanggal_View = x.Tanggal.Value.ToString("dd/MM/yyyy"),
                            NoBukti = x.NoBukti,
                            SectionName = x.SectionName,
                            Total_View = Convert.ToDecimal(x.Total).ToMoney()
                        });
                    }
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            data = model,
                            countAll = model.Count()
                        },
                        Message = ""
                    });
                }

            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string update_COB(List<insertSplitBilling> id)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    foreach (var x in id)
                    {
                        var m = s.Pelayanan_GetCOB_UpdateTransaksi(x.nobukti, x.mcu);
                    }
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = "",
                        Message = ""
                    });
                }

            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

    }
}