﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Pelayanan.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class AnggotaController : Controller
    {
        #region ===== C R E A T E
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string nrm, string namapasien, string perusahaan)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.NRM = nrm;
            ViewBag.perusahaan = perusahaan;
            ViewBag.NamaPasien = namapasien;
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post()
        {
            try
            {
                var item = new AnggotaInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.SIMdAnggotaKerjasama.FirstOrDefault(x => x.NoAnggota == item.NoKartu);
                        if (model != null)
                        {
                            return new JavaScriptSerializer().Serialize(new ResultStatus()
                            {
                                IsSuccess = false,
                                Message = "Nomor Kartu tersebut sudah digunakan atas Nama : " + model.Nama
                            });
                        }
                        else
                        {
                            var insert = s.Registrasi_AnggotaKerjasama_Create(
                            item.CustomerKerjasamaID,
                            item.NoKartu,
                            item.NamaPasien,
                            true,
                            item.Klp,
                            null,
                            null,
                            item.NRM_Anggota,
                            null,
                            null
                            );

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"Anggota Baru Create {item.NoKartu}"
                            };
                            UserActivity.InsertUserActivity(userActivity);

                            result = new ResultSS(1, insert);
                        }

                    }

                    return new JavaScriptSerializer().Serialize(new ResultStatus()
                    {
                        Data = new string[6] { item.NoKartu, item.KodePerusahaanAsal, item.NamaPerusahaanAsal, item.Klp, item.KelasID, item.NamaPasien },
                        IsSuccess = result.IsSuccess
                    });
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== E D I T
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string nomor)
        {
            var model = new AnggotaInsertViewModel();
            using (var s = new SIM_Entities())
            {
                var r = s.SIMdAnggotaKerjasama.FirstOrDefault(x => x.NoAnggota == nomor);
                if(r != null)
                {
                    model.NRM_Anggota = r.NRM;
                    model.NamaPasien = r.Nama;
                    model.CustomerKerjasamaID = r.CustomerKerjasamaID;
                    var customer = s.VW_CustomerKerjasama.FirstOrDefault(x => x.CustomerKerjasamaID == r.CustomerKerjasamaID);
                    if (customer != null)
                    {
                        model.KodePerusahaanAsal = customer.Kode_Customer;
                        model.NamaPerusahaanAsal = customer.Nama_Customer;
                        model.KodePerusahaan = customer.Kode_Customer;
                        model.NamaPerusahaan = customer.Nama_Customer;
                    }
                    model.Klp = r.Klp;
                    model.NoKartu = r.NoAnggota;
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.NRM = model.NRM_Anggota;
            ViewBag.perusahaan = model.KodePerusahaan;
            ViewBag.NamaPasien = model.NamaPasien;
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public string Edit_Post()
        {
            try
            {
                var item = new AnggotaInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.SIMdAnggotaKerjasama.FirstOrDefault(x => x.NoAnggota == item.NoKartu);
                        if (model != null)
                        {
                            model.Nama = item.NamaPasien;
                            model.CustomerKerjasamaID = item.CustomerKerjasamaID;
                            model.Klp = item.Klp;
                            var insert = s.SaveChanges();

                            var customer = s.VW_CustomerKerjasama.FirstOrDefault(x => x.CustomerKerjasamaID == item.CustomerKerjasamaID);
                            if (customer != null)
                            {
                                item.KelasID = customer.KelasID;
                            }

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"Anggota Edit {item.NoKartu}"
                            };
                            UserActivity.InsertUserActivity(userActivity);

                            result = new ResultSS(1, insert);
                        }
                        else
                        {
                            return new JavaScriptSerializer().Serialize(new ResultStatus()
                            {
                                IsSuccess = false,
                                Message = "Data angggota tidak ditemukan."
                            });
                        }

                    }

                    return new JavaScriptSerializer().Serialize(new ResultStatus()
                    {
                        Data = new string[6] { item.NoKartu, item.KodePerusahaanAsal, item.NamaPerusahaanAsal, item.Klp, item.KelasID, item.NamaPasien },
                        IsSuccess = result.IsSuccess
                    });
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== P E R U S A H A A N
        [HttpPost]
        public string ListCustomerKerjasama(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<VW_CustomerKerjasama> proses = s.VW_CustomerKerjasama;
                    proses = proses.Where($"{nameof(VW_CustomerKerjasama.Kode_Customer)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(VW_CustomerKerjasama.Nama_Customer)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(VW_CustomerKerjasama.NamaKelas)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(VW_CustomerKerjasama.JenisKerjasama)}.Contains(@0)", filter[3]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<CustomerViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion
    }
}