﻿using CrystalDecisions.CrystalReports.Engine;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class ResepController : Controller
    {
        #region ===== C R E A T E - O P T I K

        [HttpGet]
        [ActionName("CreateOptik")]
        public ActionResult CreateOptik_Get(string noreg, string section, int nomor)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;
            using (var s = new SIM_Entities())
            {
                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                if (m == null) return HttpNotFound();
                ViewBag.DokterID = m.DokterID;
                ViewBag.NamaDokter = m.NamaDOkter;
            }
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("CreateOptik")]
        [ValidateAntiForgeryToken]
        public string CreateOptik_Post()
        {
            try
            {
                var item = new ResepOptikViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        #region Validation
                        var datareg = s.VW_DataPasienReg.FirstOrDefault(x => x.NoReg == item.NoReg);
                        #endregion

                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var id = s.AutoNumber_Pelayanan_BillOptik_Resep().FirstOrDefault();
                                var model = IConverter.Cast<BILLOPTIK_RESEP>(item);
                                model.Tanggal = DateTime.Now;
                                model.No_Bukti = id;
                                model.NRM = datareg.NRM;
                                model.NamaPasien = datareg.NamaPasien;
                                model.Alamat = datareg.Alamat;
                                model.NoTelp = "-";
                                model.JenisKelamin = datareg.JenisKelamin;
                                model.UmurBulan = datareg.UmurBln;
                                model.UmurTahun = datareg.UmurThn;
                                model.UmurHari = datareg.UmurHr;
                                model.JenisKerjasamaID = datareg.JenisKerjasamaID;
                                model.User_ID = 490;
                                model.Batal = false;
                                model.Realisasi = false;
                                model.DateUpdate = DateTime.Now;
                                s.BILLOPTIK_RESEP.Add(model);

                                var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                                simtrreg.StatusResep = "MASUK";

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Resep optik Create {id}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg, string section, int nomor)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;
            using (var s = new SIM_Entities())
            {
                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                if (m == null) return HttpNotFound();
                ViewBag.DokterID = m.DokterID;
                ViewBag.NamaDokter = m.NamaDOkter;
            }
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new ResepViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        #region Validation
                        if (item.Detail_List == null) { throw new Exception("Detail tidak boleh kosong"); }

                        item.Detail_List.RemoveAll(x => x.Remove);
                        decimal jumlah = 0;
                        foreach (var x in item.Detail_List)
                        {
                            //var b2 = s.mBarang.FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                            var b2 = s.Pelayanan_GetListObat(item.Farmasi_SectionID, item.NoRegistrasi).FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                            //qty * harga
                            x.Model.Harga = b2.Harga_Jual ?? 0;
                            var _harga = b2.Harga_Jual ?? 0;
                            var qty = (decimal)x.Model.Qty;
                            var _subJumlah = qty * _harga;
                            jumlah += _subJumlah;
                        }
                        var datareg = s.VW_DataPasienReg.FirstOrDefault(x => x.NoReg == item.NoRegistrasi && x.Nomor == item.Nomor);
                        #endregion
                        
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var id = s.SIMtrResep_AutoID().FirstOrDefault();
                                var model = new SIMtrResep()
                                {
                                    NoResep = id,
                                    NoRegistrasi = item.NoRegistrasi,
                                    SectionID = item.SectionID,
                                    Tanggal = DateTime.Today,
                                    Jam = DateTime.Now,
                                    DokterID = item.DokterID,
                                    Jumlah = jumlah,
                                    KomisiDokter = 0,
                                    Cyto = item.Cyto,
                                    NoBukti = null,
                                    Farmasi_SectionID = item.Farmasi_SectionID,
                                    User_ID = 490,
                                    JenisKerjasamaID = datareg.JenisKerjasamaID.ToString(),
                                    CompanyID = datareg.COmpanyID,
                                    NRM = datareg.NRM,
                                    NoKartu = datareg.NoKartu,
                                    KelasID = datareg.KelasID,
                                    KTP = datareg.PasienKTP,
                                    KerjasamaID = datareg.JenisKerjasamaID.ToString(),
                                    //Realisasi = "",
                                    //PerusahaanID = "",
                                    RawatInap = datareg.RawatInap,
                                    //Batal = "",
                                    Paket = !string.IsNullOrEmpty(item.KodePaket),
                                    KodePaket = item.KodePaket,
                                    AmprahanRutin = false,
                                    IncludeJasa = false,
                                    //NoAntri = auto,
                                    UserNameInput = "",
                                    Keterangan = item.Keterangan,
                                    Puyer = item.ObatRacik,
                                    QtyPuyer = (int?)item.QtyRacik,
                                    SatuanPuyer = item.SatuanPuyer,
                                    CustomerKerjasamaID = (int?)datareg.CustomerKerjasamaID,
                                    RasioObat_ada = false,
                                    Rasio_KelebihanDibayarPasien = false,
                                    RasioUmum_Alert = false,
                                    RasioUmum_Blok = false,
                                    RasioSpesialis_Alert = false,
                                    RasioSPesialis_Blok = false,
                                    RasioSub_Alert = false,
                                    RasioSub_Blok = false,
                                    RasioUmum_nilai = 0,
                                    RasioSpesialis_Nilai = 0,
                                    RasioSub_Nilai = 0,
                                    BeratBadan= item.BeratBadan,
                                    SedangProses = null,
                                    SedangProsesPC = null,
                                    AlasanBatal = null,
                                    UserID_Batal = null,
                                    ObatPulang = false,
                                    Pending = false,
                                    AlasanPending= null,
                                    UserID_Pending = null,
                                };
                                s.SIMtrResep.Add(model);

                                #region Detail
                                foreach (var x in item.Detail_List)
                                {
                                    var blokasi = s.mBarangLokasiNew.FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                                    var d = new SIMtrResepDetail()
                                    {
                                        NoResep = id,
                                        Barang_ID = x.Model.Barang_ID,
                                        Satuan = x.Model.Satuan,
                                        Qty = x.Model.Qty,
                                        Harga_Satuan = x.Model.Harga,
                                        Disc_Persen = 0,
                                        Stok = blokasi.Qty_Stok,
                                        KomisiDokter = 0,
                                        THT = 0,
                                        Racik = x.Model.Racik,
                                        Plafon = 0,
                                        KelebihanPLafon = 0,
                                        KelasID = null,
                                        KTP = null,
                                        JenisKerjasamaID = null,
                                        PerusahaanID = null,
                                        DokterID = null,
                                        SectionID = null,
                                        PPN = 0,
                                        DosisID = 0,
                                        JenisBarangID = 0,
                                        Embalase = 0,
                                        JasaRacik = 0,
                                        Dosis = x.Model.AturanPakai,
                                        QtyRacik = item.QtyRacik,
                                        QtyHasilRacik = 0,
                                        TermasukPaket = false,
                                        //NomorUrut = 0
                                    };
                                    s.SIMtrResepDetail.Add(d);
                                }
                                #endregion
                                var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoRegistrasi);
                                simtrreg.StatusResep = "MASUK";

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Resep Create {id}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string nobukti)
        {
            ResepViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_SIMtrResep.FirstOrDefault(x => x.NoResep == nobukti);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<ResepViewModel>(m);
                    item.KodePaket = m.KodePaket;
                    //item.NamaPaket = m.paket
                    item.QtyRacik = m.QtyPuyer;
                    item.ObatRacik = m.Puyer ?? false;
                    item.SatuanPuyer = m.SatuanPuyer;
                    item.Jam_View = m.Jam.ToString("HH:mm");
                    item.Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy");
                    item.Jumlah_View = m.Jumlah.ToMoney();
                    var d = s.Pelayanan_SIMtrResepDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoResep == nobukti).ToList();
                    item.Detail_List = new ListDetail<ResepDetailViewModel>();
                    foreach (var x in d)
                    {
                        var n = IConverter.Cast<ResepDetailViewModel>(x);
                        n.Jumlah_View = ((decimal)(x.Jumlah ?? 0)).ToMoney();
                        n.Harga_View = x.Harga.ToMoney();
                        item.Detail_List.Add(false, n);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== D E T A I L - O P T I K

        [HttpGet]
        [ActionName("DetailOptik")]
        public ActionResult DetailOptik(string nobukti)
        {
            ResepOptikViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.BILLOPTIK_RESEP.FirstOrDefault(x => x.No_Bukti == nobukti);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<ResepOptikViewModel>(m);
                    item.Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy");
                    var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                    if (dokter != null)
                        item.NamaDokter = dokter.NamaDOkter;
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string nobukti)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrResep.FirstOrDefault(x => x.NoResep == nobukti && x.Realisasi == false);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        try
                        {
                            s.SIMtrResepDetail.RemoveRange(s.SIMtrResepDetail.Where(x => x.NoResep == nobukti));
                            s.SIMtrResep.Remove(m);
                            result = new ResultSS(s.SaveChanges());

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"Resep delete {nobukti}"
                            };
                            UserActivity.InsertUserActivity(userActivity);
                            dbContextTransaction.Commit();
                        }
                        catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                    }
                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E L E T E - O P T I K

        [HttpPost]
        public string DeleteOptik(string nobukti)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.BILLOPTIK_RESEP.FirstOrDefault(x => x.No_Bukti == nobukti && x.Realisasi == false);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        try
                        {
                            s.BILLOPTIK_RESEP.Remove(m);
                            result = new ResultSS(s.SaveChanges());

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"Resep delete {nobukti}"
                            };
                            UserActivity.InsertUserActivity(userActivity);
                            dbContextTransaction.Commit();
                        }
                        catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                    }
                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    IQueryable<Pelayanan_ViewResep> proses = s.Pelayanan_ViewResep;
                    proses = proses.Where($"{nameof(Pelayanan_SIMtrResep.NoRegistrasi)}=@0", filter[0]);
                    proses = proses.Where($"{nameof(Pelayanan_SIMtrResep.SectionID)}=@0", sectionid);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ResepViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== O T H E R
      
        #endregion

        #region ===== T A B L E - P A K E T - B H P

        [HttpPost]
        public string ListPaket(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_Data_PaketObat_Result> proses = s.Pelayanan_Data_PaketObat(filter[3], filter[2]).AsQueryable();
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PaketResepViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string DetailPaket(string kode, string section, string noreg, int nomor)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_PaketObat.FirstOrDefault(x => x.KodePaket == kode);
                    var d = s.Pelayanan_PaketObatDetail.Where(x => x.KodePaket == kode).ToList();
                    var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    foreach (var x in d)
                    {
                        var y = s.GetHargaObatNew_WithStok(simtrreg.JenisKerjasamaID, "XX", (simtrreg.PasienKTP ? 1 : 0), x.Barang_ID, (int?)simtrreg.CustomerKerjasamaID, section, 0).FirstOrDefault();
                        x.Harga = y.TglHargaBaru == DateTime.Today ? y.Harga_Baru : y.Harga_Lama;
                    }
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Header = h,
                        Data = d.ConvertAll(x => new
                        {
                            Kode = x.KodePaket,
                            NamaPaket = x.NamaPaket,
                            Kode_Barang = x.Kode_Barang,
                            Barang_ID = x.Barang_ID,
                            Nama_Barang = x.Nama_Barang,
                            AturanPakai = x.AturanPakai,
                            Satuan_Stok = x.Nama_Satuan,
                            Qty = x.Qty,
                            Dosis = x.Dosis,
                            Harga_Jual = x.Harga ?? 0,
                            Harga_Jual_View = (x.Harga ?? 0).ToMoney(),
                            Jumlah_View = ((x.Harga ?? 0) * (decimal)(x.Qty)).ToMoney(),
                            Qty_Stok = x.QtyStok ?? 0
                        })
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}