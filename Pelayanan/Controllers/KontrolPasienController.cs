﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    public class KontrolPasienController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg, string section, int nomor)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;
            return PartialView();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new KontrolPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var simtrrj = s.VW_DataPasienReg.FirstOrDefault(x => x.NoReg == item.NoReg && x.SectionID == item.SectionAsalID);

                        item.NoBukti = s.AutoNumber_Pelayanan_PostKontrol().FirstOrDefault();
                        item.TglInput = DateTime.Today;
                        item.JamInput = DateTime.Now;
                        item.NRM = simtrrj.NRM;
                        item.Nama = simtrrj.NamaPasien;
                        item.Alamat = simtrrj.Alamat;
                        item.JenisKelamin = simtrrj.JenisKelamin;
                        item.Umur = $"{simtrrj.UmurThn} Tahun {simtrrj.UmurBln} Bulan {simtrrj.UmurHr} Hari";
                        var m = IConverter.Cast<SIMtrFormKontrolPasien>(item);
                        if (m.JadwalManual ?? false) m.DokterID = item.DokterID_Manual;
                        s.SIMtrFormKontrolPasien.Add(m);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"KontrolPasien Create {m.NoBukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            KontrolPasienViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_ViewKontrolPasien.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<KontrolPasienViewModel>(m);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            KontrolPasienViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_ViewKontrolPasien.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<KontrolPasienViewModel>(m);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new KontrolPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.SIMtrFormKontrolPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        if (model == null) throw new Exception("Data Tidak ditemukan");

                        TryUpdateModel(model);
                        if (model.JadwalManual ?? false) model.DokterID = item.DokterID_Manual;

                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"KontrolPasien Edit {model.NoBukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string id)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrFormKontrolPasien.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    s.SIMtrFormKontrolPasien.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"KontrolPasien delete {id}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_ViewKontrolPasien> proses = s.Pelayanan_ViewKontrolPasien;
                    if (filter[24] != "True" && !string.IsNullOrEmpty(filter[23]) && !string.IsNullOrEmpty(filter[22]))
                    {
                        proses = proses.Where("TglInput >= @0", DateTime.Parse(filter[22]).Date);
                        proses = proses.Where("TglInput <= @0", DateTime.Parse(filter[23]).Date);
                    }
                    proses = proses.Where($"{nameof(Pelayanan_ViewKontrolPasien.NoReg)}=@0", filter[0]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<KontrolPasienViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Kontrol_tgl==null ? "" : x.Kontrol_tgl.Value.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListJadwalPraktek(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_ViewJadwalPraktrek> proses = s.Pelayanan_ViewJadwalPraktrek;
                    if (IFilter.F_DateTime(filter[0]) != null) proses = proses.Where($"{nameof(Pelayanan_ViewJadwalPraktrek.Tanggal)}=@0", IFilter.F_DateTime(filter[0]).Value.Date);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Pelayanan_ViewJadwalPraktrek.NamaSection)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Pelayanan_ViewJadwalPraktrek.NamaDOkter)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Pelayanan_ViewJadwalPraktrek.Keterangan)}.Contains(@0)", filter[3]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<JadwalPraktekViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    m.ForEach(x => x.Tanggal_Input = x.Tanggal.ToString("yyyy-MM-dd"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}