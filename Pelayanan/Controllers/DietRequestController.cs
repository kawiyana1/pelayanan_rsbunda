﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class DietRequestController : Controller
    {
        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg, int nomor)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            var item = new DietRequestViewModel() { Noreg = noreg, Nomor = nomor };
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new DietRequestViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var pasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == item.Noreg && x.Nomor == item.Nomor);

                                #region == VALIDATION
                                foreach (var y in item.Detail_List.Where(x => x.Remove == false))
                                {
                                    var c = s.VW_FnB_RequestDietDetail.FirstOrDefault(x => x.NoReg == item.Noreg && x.Tanggal == item.UntukTanggalDari && x.KodeShift == y.Model.Kode_Shift && x.SectionID == pasien.SectionID);
                                    var shift = s.FNB_mShift.FirstOrDefault(r => r.Kode == y.Model.Kode_Shift);
                                    if (c != null)
                                    {
                                        if (c.Realisasi == true) throw new Exception("Request Diet, shif " + shift.Nama.ToLower() + " sudah direalisasi");
                                        if (c.Realisasi == false || c.Realisasi == null) throw new Exception("Request Diet, shift " + shift.Nama.ToLower() + " sudah dibuat");
                                    }
                                }
                                #endregion
                                var m = IConverter.Cast<FNB_trDietRequest>(item);
                                m.No = s.AutoNumber_Pelayanan_SIMtrDietRequest().FirstOrDefault();
                                m.Id_Section = pasien.SectionID;
                                m.UsesId = "WEB";
                                s.FNB_trDietRequest.Add(m);

                                var kamar = "";
                                if(pasien.Kamar != null && pasien.Kamar != "")
                                {
                                    var gt_kamar = s.Vw_Kamar.FirstOrDefault(x => x.NoKamar == pasien.Kamar);
                                    if (gt_kamar != null) kamar = gt_kamar.NamaKamar + " / " + pasien.NoBed;
                                }


                                if (item.Detail_List == null) item.Detail_List = new ListDetail<DietRequestDetailViewModel>();
                                if (item.Detail_List.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                #region Detail
                                foreach(var x in item.Detail_List.Where(x => x.Remove == false))
                                {
                                    var i = new FNB_trDietRequestDetail()
                                    {
                                        No_DietRequest = m.No,
                                        Kode_Shift = x.Model.Kode_Shift,
                                        Kode_Makanan = x.Model.Kode_Makanan,
                                        Kode_Produk = x.Model.Kode_Produk,
                                        Qty = x.Model.Qty,
                                        Keterangan = x.Model.Keterangan,
                                        Snack = false,
                                        Realisasi = false,
                                        Batal = false,
                                        NoKamar = kamar,
                                    };
                                    s.FNB_trDietRequestDetail.Add(i);
                                }
                                #endregion

                                result = new ResultSS(s.SaveChanges());

                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Diet Create {m.No}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex){ dbContextTransaction.Rollback(); throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex)
                            {
                                if (ex.InnerException != null) ex = ex.InnerException;
                                if (ex.InnerException != null) ex = ex.InnerException;
                                dbContextTransaction.Rollback();
                                if (ex.Message.IndexOf("Violation of PRIMARY KEY constraint") == 0)
                                    throw new Exception("Detail tidak boleh sama");
                                else
                                    throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result,-1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion


    }
}