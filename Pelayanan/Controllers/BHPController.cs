﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class BHPController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg, string section, int nomor)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;
            using (var s = new SIM_Entities())
            {
                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                if (m == null) return HttpNotFound();
                ViewBag.DokterID = m.DokterID;
                ViewBag.NamaDokter = m.NamaDOkter;
            }
            return PartialView();
            //return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new BHPViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        #region Validation
                        if (item.Detail_List == null) { throw new Exception("Detail tidak boleh kosong"); }

                        item.Detail_List.RemoveAll(x => x.Remove);
                        decimal jumlah = 0;
                        foreach (var x in item.Detail_List)
                        {
                            //var b2 = s.mBarang.FirstOrDefault(y => y.Barang_ID == x.Model.Barang_Id);
                            var b2 = s.Pelayanan_GetListObat(item.SectionID, item.NoReg).FirstOrDefault(y => y.Barang_ID == x.Model.Barang_Id);
                            //qty * (harga - ((disc * harga) / 100))
                            var _harga = b2.Harga_Jual ?? 0;
                            var disc = decimal.Parse(x.Model.Disc_Persen.ToString());
                            var qty = decimal.Parse(x.Model.Qty.ToString());
                            var _subJumlah = qty * (_harga - ((disc * _harga) / 100));
                            jumlah += _subJumlah;
                        }
                        var datareg = s.VW_DataPasienReg.FirstOrDefault(x => x.NoReg == item.NoReg && x.Nomor == item.Nomor);
                        #endregion

                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var id = s.GetNoPOPNew_get(DateTime.Today).FirstOrDefault();
                                s.Pelayanan_SIMtrPOP_Insert(
                                    id,
                                    DateTime.Today,
                                    DateTime.Now,
                                    item.SectionID,
                                    jumlah,
                                    490,
                                    !string.IsNullOrEmpty(item.NamaPaket),
                                    item.PaketBHP,
                                    item.DokterID,
                                    0,
                                    datareg.NRM,
                                    item.NoReg,
                                    item.Ditagihkan,
                                    item.Nomor);

                                #region Detail
                                foreach (var x in item.Detail_List)
                                {
                                    var b = s.mBarangLokasiNew.FirstOrDefault(y => y.Barang_ID == x.Model.Barang_Id);
                                    //var b2 = s.mBarang.FirstOrDefault(y => y.Barang_ID == x.Model.Barang_Id);
                                    var b2 = s.Pelayanan_GetListObat(item.SectionID, item.NoReg).FirstOrDefault(y => y.Barang_ID == x.Model.Barang_Id);
                                    s.Pelayanan_SIMtrDetailPOP_Insert(
                                        id,
                                        x.Model.Barang_Id,
                                        b.Kode_Satuan,
                                        x.Model.Qty,
                                        x.Model.Disc_Persen,
                                        b.Qty_Stok,
                                        b2.Harga_Jual ?? 0, 
                                        0, 
                                        0, 
                                        0,
                                        item.DokterID, 
                                        0, 
                                        null,
                                        datareg.PasienKTP, 
                                        null,
                                        null, 
                                        0,
                                        item.SectionID,
                                        b2.Harga_Jual, 
                                        null, 
                                        null, 
                                        null, 
                                        null,
                                        x.Model.Keterangan
                                    );
                                }
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"BHP Create {id}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string nobukti)
        {
            BHPViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_SIMtrPOPPaket.FirstOrDefault(x => x.NoBuktiPOP == nobukti);
                    if (m == null) return HttpNotFound();
                    item = new BHPViewModel()
                    {
                        NoBukti = m.NoBuktiPOP,
                        Tanggal = m.Tanggal,
                        Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy"),
                        Jam_View = m.Jam.ToString("HH:mm"),
                        Jam = m.Jam,
                        NamaPaket = m.NamaPaket,
                        JumlahTransaksi_View = m.JumlahTransaksi.ToMoney(),
                        Ditagihkan = m.Ditagihkan ?? false,
                        DokterID = m.DokterID,
                        JumlahTransaksi = m.JumlahTransaksi,
                        NoReg = m.NoReg,
                        SectionID = m.SectionID,
                        DokterNama = m.NamaDOkter,
                        PaketBHP = m.PaketObat
                    };
                    var d = s.SIMtrPOPPaket_Detail.Where(x => x.NoBuktiPOP == nobukti).ToList();
                    item.Detail_List = new ListDetail<BHPDetailViewModel>();
                    foreach (var x in d)
                    {
                        var ket = s.SIMtrDetailPOP.Where(xa => xa.NoBuktiPOP == m.NoBuktiPOP && xa.Barang_Id == x.Barang_Id).FirstOrDefault();
                        item.Detail_List.Add(false, new BHPDetailViewModel()
                        {
                            Barang_Id = x.Barang_Id,
                            Disc_Persen = x.Disc_Persen,
                            HargaSatuan = x.HargaSatuan,
                            HargaSatuan_View = x.HargaSatuan.ToMoney(),
                            KodeBarang = x.Kode_Barang,
                            NamaBarang = x.Nama_Barang,
                            Qty = x.Qty,
                            Satuan = x.Satuan_Stok,
                            Jumlah_View = ((decimal)(x.HargaTotal ?? 0)).ToMoney(),
                            Keterangan = ket.Keterangan
                        });
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string nobukti)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrPOP.FirstOrDefault(x => x.NoBuktiPOP == nobukti);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        try
                        {
                            s.Pelayanan_SIMtrPOP_Delete(nobukti);
                            result = new ResultSS(s.SaveChanges());

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"BHP delete {nobukti}"
                            };
                            UserActivity.InsertUserActivity(userActivity);
                            dbContextTransaction.Commit();
                        }
                        catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                    }
                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_SIMtrPOPPaket> proses = s.Pelayanan_SIMtrPOPPaket;
                    proses = proses.Where($"{nameof(Pelayanan_SIMtrPOPPaket.NoReg)}=@0", filter[0]);
                    proses = proses.Where($"{nameof(Pelayanan_SIMtrPOPPaket.SectionID)}=@0", filter[2]);
                    proses = proses.Where($"{nameof(Pelayanan_SIMtrPOPPaket.Batal)}=@0", false);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => new BHPViewModel()
                    {
                        NoBukti = x.NoBuktiPOP,
                        Tanggal = x.Tanggal,
                        Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"),
                        Jam_View = x.Jam.ToString("HH:mm"),
                        Jam = x.Jam,
                        NamaPaket = x.NamaPaket,
                        JumlahTransaksi_View = x.JumlahTransaksi.ToMoney(),
                        Ditagihkan = x.Ditagihkan ?? false
                    });
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E - B A R A N G

        [HttpPost]
        public string ListBarang(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_GetListObat_Result> proses = s.Pelayanan_GetListObat(filter[8], filter[10]);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Kode_Barang)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.NamaBarang)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Satuan_Stok)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Qty_Stok)}=@0", IFilter.F_Decimal(filter[3]));
                    proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Nama_Sub_Kategori)}.Contains(@0)", filter[4]);
                    proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Nama_Kategori)}.Contains(@0)", filter[5]);
                    proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Aktif)}=@0", true);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Nama_Kategori)}.Contains(@0)", filter[6].ToUpper() == "Y");
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Harga_Jual)}=@0", IFilter.F_Decimal(filter[7]));
                    proses = proses.Take(10);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<BarangBHPViewModel>(x));
                    m.ForEach(x => x.Harga_Jual_View = (x.Harga_Jual ?? 0).ToMoney());
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E - P A K E T - B H P

        [HttpPost]
        public string ListPaketBHP(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_GetPaketBHP_Result> proses = s.Pelayanan_GetPaketBHP(filter[3]).AsQueryable();
                    proses = proses.Where($"{nameof(Pelayanan_GetPaketBHP_Result.Kode)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Pelayanan_GetPaketBHP_Result.NamaPaket)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(Pelayanan_GetPaketBHP_Result.Ditagihkan)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PaketBHPViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string DetailPaketBHP(string section, string kode)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    var d = s.Pelayanan_GetPaketBHPDetail(section).Where(x => x.Kode == kode).ToList();
                    return JsonConvert.SerializeObject(new {
                        IsSuccess = true,
                        Data = d.ConvertAll(x => new
                        {
                            Kode = x.Kode,
                            NamaPaket = x.NamaPaket,
                            SectionID = x.SectionID,
                            SectionName = x.SectionName,
                            Barang_ID = x.Barang_ID,
                            Kode_Barang = x.Kode_Barang,
                            Nama_Barang = x.Nama_Barang,
                            Nama_Kategori = x.Nama_Kategori,
                            Satuan_Stok = x.Satuan_Stok,
                            Harga_Jual = x.Harga_Jual,
                            Harga_Jual_View = (x.Harga_Jual ?? 0).ToMoney(),
                            Jumlah_View = ((x.Harga_Jual ?? 0) * (decimal)(x.Qty ?? 0)).ToMoney(),
                            Qty = x.Qty,
                            Ditagihkan = x.Ditagihkan,
                            Qty_Stok = x.Qty_Stok
                        })
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== R E P O R T

        public ActionResult ExportPDF(string id)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Reports"), $"SIM_Rpt_BillingBHP.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_BillingBHP", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoBill", id));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"SIM_Rpt_BillingBHP;1"].SetDataSource(ds[i].Tables[0]);


                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion
    }
}