﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Pelayanan.Controllers
{
    public class RadiologiController : Controller
    {
        public string LoadTemplate(string id)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    var model = s.Pelayanan_TemplateBacaRad.FirstOrDefault(x => x.TemplateName == id);
                    if (model == null) throw new Exception("Template tidak ditemukan");
                    return new JavaScriptSerializer().Serialize(new
                    {
                        IsSuccess = true,
                        Data = model
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #region ===== I N P U T - H A S I L - B A C A

        [HttpGet]
        [ActionName("InputHasilBaca")]
        public ActionResult InputHasilBaca_Get(string nobukti)
        {
            InputHasilBacaRadiologiViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                    if (m == null) return HttpNotFound();
                    var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.SupplierPengirimID);
                    item = new InputHasilBacaRadiologiViewModel()
                    {
                        KepadaYth = dokter == null? "" : dokter.NamaDOkter,
                        NoBukti = nobukti
                    };
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost, ValidateInput(false)]
        [ActionName("InputHasilBaca")]
        [ValidateAntiForgeryToken]
        public string InputHasilBaca_Post()
        {
            try
            {
                var item = new InputHasilBacaRadiologiViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        if (item.TemplateBaru)
                        {
                            var has = s.SIMtrBacaHasilRontgen.FirstOrDefault(x => x.TemplateName == item.TemplateName);
                            if (has != null)
                            {
                                if (has.TemplateName.ToUpper() == item.TemplateName.ToUpper())
                                {
                                    throw new Exception("Nama sudah digunakan");
                                }
                            }
                        }

                        var datapasien = s.Pelayanan_GetDataBacaRadiologi.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        if (datapasien == null) throw new Exception("GetDataBacaRadiologi tidak ditemukan nobukti." + item.NoBukti);
                        var simtrrj = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        if (simtrrj== null) throw new Exception("SIMtrRJ tidak ditemukan nobukti." + item.NoBukti);
                        var no = s.AutoNumber_Pelayanan_SIMtrBacaHasilRontgen().FirstOrDefault();
                        var nomor = s.SIMtrBacaHasilRontgen.Where(x => x.NoBil == datapasien.NoBukti).Count();
                        var model = new SIMtrBacaHasilRontgen()
                        {
                            No = no,
                            TglInput = DateTime.Now,
                            NoBil = datapasien.NoBukti,
                            Nomor = nomor,
                            NoReg = simtrrj.RegNo,
                            NRM= datapasien.NRM,
                            Nama = datapasien.NamaPasien,
                            Alamat = datapasien.Alamat,
                            Umur =datapasien.UmurThn ?? 0,
                            UmurBln = datapasien.UmurBln,
                            UmurHr = datapasien.Hari ?? 0,
                            Kepada = datapasien.DokterID,
                            KepadaDi = null,
                            Jawaban = "-",
                            DokterRadiologi = item.DokterId,
                            User_ID = 1103,
                            Batal = false,
                            Pemeriksaan = item.KeteranganKlinis,
                            JawabanHeader = null,
                            JawabanFooter = null,
                            JawabanBody = item.KotakInputBesar,
                            TemplateName = item.TemplateBaru ? item.TemplateName : null,
                            Judul = null,
                            Kesan = null,
                            Kelamin = datapasien.JenisKelamin
                        };
                        s.SIMtrBacaHasilRontgen.Add(model);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"InputHasilBaca {model.No}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (DbEntityValidationException ex) { return JsonHelper.JsonMsgError(StaticModel.DbEntityValidationExceptionToString(ex)); }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== R E P O R T

        public ActionResult ExportPDF(string nomor)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Reports"), $"Rpt_HasilBacaRadiologi.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_HasilBacaRadiologi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@No", nomor));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_HasilBacaRadiologi;1"].SetDataSource(ds[i].Tables[0]);


                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

    }
}