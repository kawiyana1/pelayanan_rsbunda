﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Pelayanan.Controllers
{
    public class LABController : Controller
    {
        #region ===== I N P U T - H A S I L - B A C A

        [HttpGet]
        [ActionName("InputHasilLab")]
        public ActionResult InputHasilLab_Get(string nobukti)
        {
            var item = new InputHasilBacaLabViewModel();
            try
            {
                using (var s = new SIM_Entities())
                {
                    var servicelab = s.PelayananGetHasilLab(nobukti).ToList();

                    item.NoBukti = nobukti;
                    item.Detail = new List<InputHasilBacaLabDetailViewModel>();
                    foreach (var x in servicelab)
                    {
                        var hasil = "";
                        var ms = s.Pelayanan_trHasilLab.FirstOrDefault(xx => xx.NoBukti == item.NoBukti && xx.TestID == x.TestID);
                        if (ms != null) hasil = ms.NilaiHasil;

                        var d = new InputHasilBacaLabDetailViewModel()
                        {
                            Flag = "",
                            HasilPemeriksaan = hasil,
                            Kategori = x.Kategori,
                            Keterangan  = "",
                            NilaiRujukan = x.NilaiRujukan,
                            Satuan = x.Satuan,
                            Tampilkan = true,
                            TestID = x.TestID,
                            NamaTest = x.NamaTest
                        };
                        item.Detail.Add(d);
                    }
                    var m = s.Pelayanan_trHasilLab.FirstOrDefault(xx => xx.NoBukti == item.NoBukti);
                    if (m != null) { ViewBag.Print = true; } else { ViewBag.Print = false; }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("InputHasilLab")]
        [ValidateAntiForgeryToken]
        public string InputHasilLab_Post()
        {
            try
            {
                var item = new InputHasilBacaLabViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        foreach (var x in item.Detail)
                        {
                            var m = s.Pelayanan_trHasilLab.FirstOrDefault(xx => xx.NoBukti == item.NoBukti && xx.TestID == x.TestID);
                            if (m != null)
                            {
                                m.NilaiHasil = x.HasilPemeriksaan;
                            }
                            else
                            {
                                var inputHasilBaca = s.Pelayanan_SPInsertHasilLab(
                                    item.NoBukti,
                                    x.TestID,
                                    x.NamaTest,
                                    x.Satuan,
                                    x.NilaiRujukan,
                                    x.Kategori,
                                    x.HasilPemeriksaan
                                );
                            }
                        }
                        s.SaveChanges();

                        result = new ResultSS(1);
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"InputHasilBaca {item.NoBukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (DbEntityValidationException ex) { return JsonHelper.JsonMsgError(StaticModel.DbEntityValidationExceptionToString(ex)); }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        [ActionName("UpdateHasilLab")]
        [ValidateAntiForgeryToken]
        public string UpdateHasilLab_Post()
        {
            try
            {
                var item = new InputHasilBacaLabViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        foreach (var x in item.Detail)
                        {
                            var m = s.Pelayanan_trHasilLab.FirstOrDefault(xx => xx.NoBukti == item.NoBukti && xx.TestID == x.TestID);
                            if (m != null)
                            {
                                m.NilaiHasil = x.HasilPemeriksaan;
                            }
                            else
                            {
                                var inputHasilBaca = s.Pelayanan_SPInsertHasilLab(
                                    item.NoBukti,
                                    x.TestID,
                                    x.NamaTest,
                                    x.Satuan,
                                    x.NilaiRujukan,
                                    x.Kategori,
                                    x.HasilPemeriksaan
                                );
                            }
                        }
                        s.SaveChanges();
                        result = new ResultSS(1);
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"InputHasilBaca {item.NoBukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (DbEntityValidationException ex) { return JsonHelper.JsonMsgError(StaticModel.DbEntityValidationExceptionToString(ex)); }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        public ActionResult ReportHasilLaboratorium(string nobukti)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Reports"), $"SIM_Rpt_ReportHasilLab.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_ReportHasilLab", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoBukti", nobukti));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"SIM_Rpt_ReportHasilLab;1"].SetDataSource(ds[i].Tables[0]);


                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
    
        #region ==== R E P O R T

        public ActionResult HasilLab(string id)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Reports"), $"Print_Hasil_Lab.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("LIS_Rpt_HasilTesLIS", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoBuktiLab", id));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"LIS_Rpt_HasilTesLIS;1"].SetDataSource(ds[i].Tables[0]);


                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion
    }
}