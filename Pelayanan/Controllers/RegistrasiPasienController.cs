﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;

namespace Pelayanan.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class RegistrasiPasienController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["TipePelayanan"].Value == "RJ") {
                return RedirectToAction("Index","RegistrasiPasienRJ");
            }
            return View();
        }

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string noreg, int nomor, string alasan)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor && x.SudahPeriksa == false);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    m.Batal = true;
                    m.Out = true;
                    m.AlasanBatal = alasan;
                    result = new ResultSS(s.SaveChanges());
                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Batal SIMtrDataRegPasien noreg:{noreg} nomor:{nomor}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        
        #endregion
    }
}