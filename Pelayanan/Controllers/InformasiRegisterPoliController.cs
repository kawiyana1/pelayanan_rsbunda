﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    public class InformasiRegisterPoliController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    //Noreg-Tanggal-Nrm-NamaPasien-JenisKelamin-Alamat-TipePasien-Dokter-StatusPeriksa
                    IQueryable<Pelayanan_ViewInformasiRegisterPoli> proses = s.Pelayanan_ViewInformasiRegisterPoli;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Pelayanan_ViewInformasiRegisterPoli.NoReg)}.Contains(@0)", filter[0]);
                    if (IFilter.F_DateTime(filter[1]) != null) proses = proses.Where($"{nameof(Pelayanan_ViewInformasiRegisterPoli.Tanggal)}=@0", IFilter.F_DateTime(filter[1]).Value.Date);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Pelayanan_ViewInformasiRegisterPoli.NRM)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Pelayanan_ViewInformasiRegisterPoli.NamaPasien)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Pelayanan_ViewInformasiRegisterPoli.JenisKelamin)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Pelayanan_ViewInformasiRegisterPoli.Alamat)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(Pelayanan_ViewInformasiRegisterPoli.JenisKerjasama)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(Pelayanan_ViewInformasiRegisterPoli.Nama_Supplier)}.Contains(@0)", filter[7]);
                    if (!string.IsNullOrEmpty(filter[8])) proses = proses.Where($"{nameof(Pelayanan_ViewInformasiRegisterPoli.SudahPeriksa)}=@0", filter[8] == "1");
                    if (!string.IsNullOrEmpty(filter[9])) proses = proses.Where($"{nameof(Pelayanan_ViewInformasiRegisterPoli.SectionName)}.Contains(@0)", filter[9]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<InformasiRegisterPoliViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}