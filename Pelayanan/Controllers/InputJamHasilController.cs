﻿using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;
using System.Data.Entity.Validation;

namespace Pelayanan.Controllers
{
    public class InputJamHasilController : Controller
    {
        // GET: InputJamHasil
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, string noreg, int view = 0)
        {
            var model = new InputJamHasilViewModel();
            using (var sim = new SIM_Entities())
            {

                var t = sim.trInformasiJamPemeriksaan.FirstOrDefault(x => x.NoBukti == id);
                if (t != null)
                {
                    model = IConverter.Cast<InputJamHasilViewModel>(t);
                    model.NoBukti = id;
                }
                else
                {
                    model.NoBukti = id;
                }

            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
            {
                return PartialView(model);
            }
            {
                return View(model);
            }
        }


        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]

        public string CreatePost()
        {
            try
            {
                var item = new InputJamHasilViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var model = s.trInformasiJamPemeriksaan.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                                //var section = Request.Cookies["SectionIDPelayanan"].Value;
                                if (model == null)
                                {
                                    s.trInformasiJamPemeriksaan_insert(item.NoBukti, item.Diambil, item.Dikerjakan, item.Selesai, item.Diserahkan);
                                }
                                else { 
                                    s.trInformasiJamPemeriksaan_update(item.NoBukti, item.Diambil, item.Dikerjakan, item.Selesai, item.Diserahkan);
                                }

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Hasil Jam Input Cerate {item.NoBukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}