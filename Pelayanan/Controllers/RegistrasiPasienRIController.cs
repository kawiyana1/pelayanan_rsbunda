﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.Linq.Dynamic;
using System.Web.Helpers;
using Pelayanan.Entities.SIM;
using System.Data.SqlClient;
using Pelayanan.Models;

namespace Pelayanan.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class RegistrasiPasienRIController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_DataRegPasienRI> p = s.Pelayanan_DataRegPasienRI;
                    if (filter[24] != "True" && !string.IsNullOrEmpty(filter[23]) && !string.IsNullOrEmpty(filter[22]))
                    {
                        p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).Date);
                        p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]).Date);
                    }
                    p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.SectionID)}.Contains(@0)", Request.Cookies["SectionIDPelayanan"].Value);
                    if (filter[25] == "1")
                    {
                        p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.StatusBayar)}=@0 AND {nameof(Pelayanan_DataRegPasienRI.SudahPeriksa)}=@1", "Belum", false);
                        p = p.Where(e => e.Out == false);
                        p = p.Where(e => e.Active == true);
                        p = p.Where(e => e.Dimutasikan == false);
                    }
                    else if (filter[25] == "2")
                    {
                        p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.StatusBayar)}=@0 AND {nameof(Pelayanan_DataRegPasienRI.SudahPeriksa)}=@1", "Belum", true);
                        p = p.Where(e => e.Out == false);
                        p = p.Where(e => e.Active == true);
                        p = p.Where(e => e.Dimutasikan == false);
                    }
                    else if (filter[25] == "3")
                    {
                        p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.StatusBayar)}=@0 AND {nameof(Pelayanan_DataRegPasienRI.SudahPeriksa)}=@1", "Sudah Bayar", true);
                        p = p.Where(e => e.Out == false);
                        p = p.Where(e => e.Active == true);
                        p = p.Where(e => e.Dimutasikan == false);
                    }
                    else if (filter[25] == "4")
                    {
                        p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.StatusBayar)}=@0 AND {nameof(Pelayanan_DataRegPasienRI.SudahPeriksa)}=@1", "Sudah Bayar", false);
                        p = p.Where(e => e.Out == false);
                        p = p.Where(e => e.Active == true);
                        p = p.Where(e => e.Dimutasikan == false);
                    }
                    else if (filter[25] == "5")
                    {
                        p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.StatusBayar)}=@0", "Belum");
                        p = p.Where(e => e.Out == false);
                        p = p.Where(e => e.Active == true);
                        p = p.Where(e => e.Dimutasikan == false);
                    }
                    //if (!string.IsNullOrEmpty(filter[0]))
                    //    p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.NoAntri)} = @0", IFilter.F_short(filter[0]));
                    p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.NoReg)}.Contains(@0)", filter[0]);
                    p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.NRM)}.Contains(@0)", filter[6]);
                    p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.NamaPasien)}.Contains(@0)", filter[7]);

                    if (!string.IsNullOrEmpty(filter[1]))  p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.Kamar)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2]))  p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.NoBed)}.Contains(@0)", filter[2]);

                    if (!string.IsNullOrEmpty(filter[8]))  p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.JenisKerjasama)}.Contains(@0)", filter[8]);
                    var eks = filter[9];
                    if (!string.IsNullOrEmpty(filter[9]))  p = p.Where(x => x.EstimasiSisa.ToString().Contains(eks));

                    var totalcount = p.Count();
                    var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, null, totalcount, pageIndex);
                    var datas = new List<RegistrasiPasienViewModel>();
                    foreach (var x in models.ToList())
                    {
                        var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                        m.Jam_View = x.Jam == null ? "" : x.Jam.Value.ToString("HH\":\"mm");
                        m.Tanggal_View = x.Tanggal == null ? "" : x.Tanggal.Value.ToString("dd/MM/yyyy");
                        m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                        m.EstimasiSisa_View = (x.EstimasiSisa == null ? "0" : x.EstimasiSisa.Value.ToMoney());

                        var kamar = "";
                        if (x.Kamar != null && x.Kamar != "")
                        {
                            var gt_kamar = s.mKamar.FirstOrDefault(e => e.NoKamar == x.Kamar);
                            if (gt_kamar != null) kamar = gt_kamar.NamaKamar;
                        }
                        m.Kamar = kamar;

                        var pasienkhusus = s.Vw_PasienBermasalah.OrderByDescending(z => z.TglInput).FirstOrDefault(z => z.NRM == x.NRM);
                        if (pasienkhusus != null)
                        {
                            m.Emoticon = pasienkhusus.Emoticon;
                        }

                        datas.Add(m);
                    }
                    result.Data = datas;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}