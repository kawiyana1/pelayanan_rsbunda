﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Pelayanan.Controllers
{
    public class PosisiPasienController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_Informasi_PosisiPasien> proses = s.Pelayanan_Informasi_PosisiPasien;
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PosisiPasien.NoReg)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PosisiPasien.NRM)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PosisiPasien.NamaPasien)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PosisiPasien.Alamat)}.Contains(@0)", filter[3]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PosisiPasien.TipePasien)}.Contains(@0)", filter[4]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PosisiPasien.PosisiTerakhir)}.Contains(@0)", filter[5]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PosisiPasienViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}