﻿    using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Pelayanan.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class BillingController : Controller
    {
        #region ===== I N D E X
        [HttpGet]
        public ActionResult Index(string noreg, int nomor)
        {
            RegistrasiPasienViewModel model;
            using (var s = new SIM_Entities())
            {
                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                if (m == null) return HttpNotFound();
                model = IConverter.Cast<RegistrasiPasienViewModel>(m);
                model.NoReg = m.NoReg;
                model.Nomor = m.Nomor;
                model.TglReg_View = model.TglReg.ToString("dd/MM/yyyy");
                model.Tanggal_View = model.Tanggal.ToString("dd/MM/yyyy");
                model.Jam_View = model.Jam.ToString("HH\":\"mm");
                ViewBag.AuditData = (m.AuditData);
            }
            return View(model);
        }

        private bool HelperInsert(SIM_Entities s, string nRM, string sectionID, string dokterID)
        {
            var tgl = DateTime.Today.ToString("yyyy-MM-dd");
            s.UpdateKunjunganPasien(nRM, tgl, sectionID, dokterID);
            s.SaveChanges();
            return true;
        }
        #endregion

        #region ===== R I N C I A N - B I A Y A

        [HttpGet]
        public ActionResult RincianBiaya(string noreg)
        {
            ViewBag.NoReg = noreg;
            return PartialView();
        }

        [HttpPost]
        public string ListRincianBiaya(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<GetDetailRincianBiaya_Estimasi_Result> proses = s.GetDetailRincianBiaya_Estimasi(filter[11], (int?)null);
                    if (!string.IsNullOrEmpty(filter[12]))
                        proses = proses.Where($"{nameof(GetDetailRincianBiaya_Estimasi_Result.KategoriBiaya)}=@0", filter[12]);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Estimasi_Result.NoBukti)}.Contains(@0)", filter[0]);
                    if (IFilter.F_DateTime(filter[1]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Estimasi_Result.Tanggal)}=@0", IFilter.F_DateTime(filter[1]));
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Estimasi_Result.JenisBiaya)}.Contains(@0)", filter[2]);
                    if (IFilter.F_Decimal(filter[3]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Estimasi_Result.Qty)}=@0", (double)IFilter.F_Decimal(filter[3]));
                    if (IFilter.F_Decimal(filter[4]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Estimasi_Result.Nilai)}=@0", IFilter.F_Decimal(filter[4]));
                    if (IFilter.F_Decimal(filter[5]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Estimasi_Result.Disc)}=@0", (double)IFilter.F_Decimal(filter[5]));
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Estimasi_Result.SectionName)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Estimasi_Result.DokterName)}.Contains(@0)", filter[7]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<DetailRincianBiayaViewModel>(x));
                    var grandtotal = proses.Sum(x => x.Nilai);
                    var totaldiskon = proses.Sum(x => x.Disc);
                    var totalqty = proses.Sum(x => x.Qty);
                    foreach (var x in m)
                    {
                        x.TotalDisk = totaldiskon ?? 0;
                        x.TotalQty = totalqty ?? 0;
                        x.GrandTotal = grandtotal ?? 0;
                        x.GrandTotal_View = x.GrandTotal.ToMoney();
                        x.Nilai_View = ((decimal)(x.Nilai ?? 0)).ToMoney();
                        x.Tanggal_View = x.Tanggal == null ? "" : x.Tanggal.Value.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string getTotalDetailBiaya(string noreg)
        {
            try
            {
                decimal total = 0;
                using (var s = new SIM_Entities())
                {
                    var l = s.GetDetailRincianBiaya(noreg, (int?)null).ToList();

                    foreach (var x in l)
                    {
                        total += (decimal)x.Nilai * (decimal)x.Qty;
                    }
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = total.ToMoney(),
                    Message = "Success.."
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== RI RJ
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg, string section, int nomor)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;  
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;

            checkInsertTask(noreg);

            var item = new BillingInsertViewModel();
            RegistrasiPasienViewModel model;
            using (var s = new SIM_Entities())
            {
                ViewBag.ShowKomponen = checkOtorisasiShowKomponen(s);

                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                if (m == null) return HttpNotFound();
                model = IConverter.Cast<RegistrasiPasienViewModel>(m);
                ViewBag.NRM = m.NRM;
                ViewBag.NamaPasien = m.NamaPasien;
                ViewBag.JenisKelamin = m.JenisKelamin;
                ViewBag.DokterID = m.DokterID;
                ViewBag.NamaDokter = m.NamaDOkter;
                ViewBag.PenandaKunjunganPertanggal = m.PenandaKunjunganPertanggal;

                item = autoShowJasa(item, s, m);
                ViewBag.LoginType = Request.Cookies["LoginTipe"].Value.ToUpper();

                //ViewBag.AutoJasa = false;
                //if (Request.Cookies["LoginTipe"].Value.ToUpper() == "RJ") {
                //    var _autojasa = s.Pelayanan_CekJasaAuto(m.NoReg).FirstOrDefault();
                //    if (_autojasa != null) ViewBag.AutoJasa = _autojasa == "Belum";
                //}

            }
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        var nobuktiRJ = "";
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var userid = User.Identity.GetUserId();
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);

                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                checkDetailKomponen(item);

                                var nobukti = s.Pelayanan_Insert_SIMtrRJ_RIRJ(
                                    simtrregistrasi.RawatInap,
                                    item.noreg,
                                    item.nomor,
                                    simtrregistrasi.JenisKerjasamaID,
                                    item.tanggal,
                                    item.tanggal + item.jam,
                                    item.section,
                                    item.dokter,
                                    simtrregistrasi.KdKelas,
                                    simtrregistrasi.NRM,
                                    item.adaobat,
                                    item.audit,
                                    userid,
                                    item.mcu,
                                    item.cob
                                 ).FirstOrDefault();

                                nobuktiRJ = nobukti;
                                //var m = new SIMtrRJ()
                                //{
                                //    NoBukti = nobukti,
                                //    RawatInap = simtrregistrasi.RawatInap,
                                //    RegNo = item.noreg,
                                //    Nomor = item.nomor,
                                //    JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID,
                                //    Tanggal = item.tanggal,
                                //    Jam = item.tanggal + item.jam,
                                //    SectionID = item.section,
                                //    DokterID = item.dokter,
                                //    KdKelas = simtrregistrasi.KdKelas,
                                //    UserID = 1103,
                                //    NRM = simtrregistrasi.NRM,
                                //    Status = "O",
                                //    MCU = false,
                                //    Batal = false,
                                //    AdaObat = item.adaobat,
                                //    Audit = item.audit,
                                //    UserIDWeb = userid
                                //};
                                //s.SIMtrRJ.Add(m);
                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count != 0)
                                {
                                    foreach (var x in item.detail)
                                    {
                                        var i = new SIMtrRJTransaksi()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KelasID = simtrregistrasi.KdKelas,
                                            Qty = (double)x.qty,
                                            Tarif = x.tarif,
                                            Jam = item.tanggal + item.jam,
                                            UserID = 1103,
                                            NRM = simtrregistrasi.NRM,
                                            Waktu = item.tanggal + item.jam,
                                            PasienKTP = simtrregistrasi.PasienKTP,
                                            DokterID = x.dokter,
                                            Disc = (double)x.diskon,
                                            Keterangan = insertKeteranganInvoiceJasa(s, x.jasaid, simtrregistrasi.KdKelas, 1, x.dokter, item.noreg, item.nomor),
                                    };
                                        s.SIMtrRJTransaksi.Add(i);
                                        foreach (var y in x.detail)
                                        {
                                            var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                            var j = new SIMtrRJTransaksiDetail()
                                            {
                                                NoBukti = nobukti,
                                                Nomor = nomorbukti,
                                                JasaID = x.jasaid,
                                                KomponenID = y.komponenid,
                                                Harga = y.harga,
                                                KelompokAkun = komponenbiaya.KelompokAkun,
                                                PostinganKe = "GL",
                                                HargaOrig = y.harga,
                                                Disc = (double)y.diskon
                                            };
                                            s.SIMtrRJTransaksiDetail.Add(j);
                                        }
                                        nomorbukti++;

                                    }
                                }

                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                simtrdataregpasien.SudahPeriksa = true;
                                if (Request.Cookies["LoginTipe"].Value == "RJ")
                                {
                                    simtrdataregpasien.PX_Keluar_Sembuh = true;
                                    simtrdataregpasien.PxKeluar_Pulang = true;
                                }

                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Create {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();

                                //insert auto BHP jasa
                                insetAutoBHPJasa(s, nobuktiRJ);


                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                //if(ex.Message == "")
                                throw new ArgumentNullException("connection", "Masih ada transaksi, silahkan simpan ulang");
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                                throw new Exception(ex.Message);
                            }
                            catch (SqlException ex)
                            {
                                throw new Exception(ex.Message+" - "+ex.InnerException);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public ActionResult Detail(string nobukti)
        {
            if (Request.Cookies["LoginTipe"].Value.ToUpper() == "OK")
            {
                RedirectToAction("DetailOK", new { nobukti = nobukti });
            }
            BillingInsertViewModel model;
            using (var s = new SIM_Entities())
            {
                ViewBag.ShowKomponen = checkOtorisasiShowKomponen(s);

                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    dokter = m.DokterID,
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    mcu = m.MCU == null ? false : m.MCU,
                    cob = (bool) (m.COB == null ? false : m.COB),
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                    detail = new List<BillingInsertDetailViewModel>()
                };

                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string nobukti)
        {
            if (Request.Cookies["LoginTipe"].Value.ToUpper() == "OK")
            {
                RedirectToAction("DetailOK", new { nobukti = nobukti });
            }
            BillingInsertViewModel model;
            using (var s = new SIM_Entities())
            {
                ViewBag.ShowKomponen = checkOtorisasiShowKomponen(s);
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();

                ViewBag.Noreg = m.RegNo;
                ViewBag.Section = m.SectionID;
                ViewBag.Nomor = m.Nomor;

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    dokter = m.DokterID,
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    nobukti = m.NoBukti,
                    cob = (bool)(m.COB == null ? false : m.COB),
                    mcu = m.MCU == null ? false : m.MCU,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                    detail = new List<BillingInsertDetailViewModel>()
                };

                var detailid = 0;
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public string Edit_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Delete
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                if (simtrregistrasi.StatusBayar != "Belum") throw new Exception("Pasien sudah melakukan pembayaran");
                               
                                if (simtrregistrasi.CloseAdmin == true) throw new Exception("Data Pasien sudah terclosing admin");

                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat diubah karena sudah di audit");
                                
                                var nobukti = SIMtrRJ.NoBukti;
                                var userid = User.Identity.GetUserId();
                                #endregion

                                checkDetailKomponen(item);

                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }
                                var SIMtrOperasiAnastesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrOperasiAnastesi != null)
                                {
                                    s.SIMtrOperasiAnastesi.RemoveRange(SIMtrOperasiAnastesi);
                                }
                                var SIMtrOperasiInstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == item.nobukti);
                                if (SIMtrOperasiInstrumen != null)
                                {
                                    s.SIMtrOperasiInstrumen.RemoveRange(SIMtrOperasiInstrumen);
                                }
                                #endregion


                                SIMtrRJ.NoBukti = nobukti;
                                SIMtrRJ.RegNo = item.noreg;
                                SIMtrRJ.JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID;
                                SIMtrRJ.Tanggal = item.tanggal;
                                SIMtrRJ.Jam = item.tanggal + item.jam;
                                SIMtrRJ.SectionID = item.section;
                                SIMtrRJ.DokterID = item.dokter;
                                SIMtrRJ.KdKelas = simtrregistrasi.KdKelas;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.MCU = item.mcu;
                                SIMtrRJ.COB = item.cob;
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.Audit = item.audit;
                                SIMtrRJ.UserIDWeb = userid;
                                SIMtrRJ.AdaObat = item.adaobat;
                                
                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.tanggal + item.jam,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.tanggal + item.jam,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon,
                                        Keterangan = insertKeteranganInvoiceJasa(s, x.jasaid, simtrregistrasi.KdKelas, 1, x.dokter, item.noreg, item.nomor),
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }

                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                //var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                //simtrdataregpasien.SudahPeriksa = true;
                                //if (Request.Cookies["LoginTipe"].Value == "RJ")
                                //{
                                //    simtrdataregpasien.PX_Keluar_Sembuh = true;
                                //    simtrdataregpasien.PxKeluar_Pulang = true;
                                //}
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Edit {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();

                                //insert auto BHP jasa
                                insetAutoBHPJasa(s, nobukti);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== OK
        [HttpGet]
        [ActionName("CreateOK")]
        public ActionResult CreateOK_Get(string noreg, string section, int nomor)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;

            checkInsertTask(noreg);

            RegistrasiPasienViewModel model;
            using (var s = new SIM_Entities())
            {
                ViewBag.LoginType = Request.Cookies["LoginTipe"].Value.ToUpper();
                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var reg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                if (m == null) return HttpNotFound();
                model = IConverter.Cast<RegistrasiPasienViewModel>(m);
                ViewBag.NRM = m.NRM;
                ViewBag.NamaPasien = m.NamaPasien;
                ViewBag.JenisKelamin = m.JenisKelamin;
                ViewBag.SectionAsalID = m.SectionAsalID;
                ViewBag.SectionAsalName = sectionasal.SectionName;
                ViewBag.PenanggungNama = reg.PenanggungNama;
                ViewBag.PenanggungAlamat = reg.PenanggungAlamat;
                ViewBag.PenanggungHubungan = reg.PenanggungHubungan;
            }
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("CreateOK")]
        public string CreateOK_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg && x.Nomor == item.nomor);
                                var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                checkDetailKomponen(item);

                                var userid = User.Identity.GetUserId();

                                var rj = s.Pelayanan_Insert_SIMtrRJ_OK(
                                    simtrregistrasi.RawatInap,
                                    item.noreg,
                                    item.nomor,
                                    simtrregistrasi.JenisKerjasamaID,
                                    item.tanggal,
                                    item.tanggal + item.jam,
                                    item.section,
                                    item.dokter,
                                    item.kelas,
                                    simtrregistrasi.NRM,
                                    item.adaobat,
                                    item.audit,
                                    userid,
                                    item.jenisanatesi,  
                                    item.cito,
                                    item.spesialis,
                                    item.ruangok,
                                    item.kategorioperasi,
                                    item.jenisluka,
                                    item.dokterpengirim,
                                    item.dokter,
                                    item.opetatorpelaksasa,
                                    item.durante,
                                    item.dokterasisten,
                                    item.dokterasisten2,
                                    item.dokteranak,
                                    item.dokteranastesi,
                                    simtrreg.PenanggungNama,
                                    simtrreg.PenanggungAlamat,
                                    simtrreg.PenanggungHubungan,
                                    reg.SectionAsalID,
                                    item.mcu,
                                    item.cob
                                 ).FirstOrDefault();

                                var nobukti = rj;
                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.tanggal + item.jam,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.tanggal + item.jam,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon,
                                        Keterangan = insertKeteranganInvoiceJasa(s, x.jasaid, simtrregistrasi.KdKelas, int.Parse(item.kategorioperasi), x.dokter, item.noreg, item.nomor),
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }
                                if (item.asistenanastesi == null) item.asistenanastesi = new List<SelectListItemViewModel>();
                                foreach (var x in item.asistenanastesi)
                                {
                                    s.SIMtrOperasiAnastesi.Add(new SIMtrOperasiAnastesi() { NoBukti = nobukti, DokterAsistenAnas = x.Value, D = x.Check1 });
                                }
                                if (item.instrumen == null) item.instrumen = new List<SelectListItemViewModel>();
                                foreach (var x in item.instrumen)
                                {
                                    s.SIMtrOperasiInstrumen.Add(new SIMtrOperasiInstrumen() { NoBUkti = nobukti, DokterInstrumenID = x.Value, D = x.Check1 });
                                }
                                if (item.onloop == null) item.onloop = new List<SelectListItemViewModel>();
                                foreach (var x in item.onloop)
                                {
                                    s.SIMtrOperasiOnLoop.Add(new SIMtrOperasiOnLoop() { NoBukti = nobukti, DokterOnLoopID = x.Value, D = x.Check1 });
                                }

                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                simtrdataregpasien.SudahPeriksa = true;
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing OK Create {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();

                                //insert auto BHP jasa
                                insetAutoBHPJasa(s, nobukti);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public ActionResult DetailOK(string nobukti)
        {
            var tl = Request.Cookies["LoginTipe"].Value.ToUpper();
            BillingInsertViewModel model;
            using (var s = new SIM_Entities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.OK_DokterBedahID,
                    opetatorpelaksasa = m.OK_DokterBedahID2,
                    durante = m.OK_DokterDuranteID,
                    dokterasisten = m.OK_DOkterAssBedah,
                    dokterasisten2 = m.OK_DOkterAssBedah2,
                    dokteranak = m.OK_DokterANak,
                    dokteranastesi = m.OK_DokterAnasID,
                    cito = m.Cyto ?? false,
                    spesialis = m.SpesialisasiID,
                    cob = (bool)(m.COB == null ? false : m.COB),
                    mcu = m.MCU == null ? false : m.MCU,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                    asistenanastesi = new List<SelectListItemViewModel>(),
                    instrumen = new List<SelectListItemViewModel>(),
                    onloop = new List<SelectListItemViewModel>()
                };
                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter, Check1 = x.D });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter, Check1 = x.D });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter, Check1 = x.D });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);

                if (spesialisasi != null) ViewBag.SpesialisasiName = spesialisasi.SpesialisName;
                if (ruangok != null) ViewBag.RuangOKName = ruangok.RuangOK;

                ViewBag.PenanggungNama = m.OK_PenanggungNama;
                ViewBag.PenanggungAlamat = m.OK_PenanggungAlamat;
                ViewBag.PenanggungHubungan = m.OK_PenanggungHub;
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                ViewBag.JenisAnatesiName = jenisanatesi == null ? "" : jenisanatesi.Deskripsi;
                ViewBag.RuangOKName = ruangok == null ? "" : ruangok.RuangOK;
                var kategorioperasi = s.SIMmKategoriOperasi.FirstOrDefault(x => x.KategoriID == m.OK_KategoriOperasiID);
                ViewBag.KategoriOperasiName = kategorioperasi == null ? "" : kategorioperasi.KategoriName;
                var jenisluka = s.SIMmJenisLuka.FirstOrDefault(x => x.KodeLuka == m.JenisLukaID);
                ViewBag.JenisLukaName = jenisluka == null ? "" : jenisluka.Deskripsi;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;

                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Operator = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.opetatorpelaksasa);
                ViewBag.OperatorPelaksana = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.durante);
                ViewBag.Durante = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten);
                ViewBag.DokterAsisten = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten2);
                ViewBag.DokterAsisten2 = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranak);
                ViewBag.DokterAnak = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranastesi);
                ViewBag.Anastesi = dokter != null ? dokter.NamaDOkter : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("EditOK")]
        public ActionResult EditOK_Get(string nobukti)
        {
            var tl = Request.Cookies["LoginTipe"].Value.ToUpper();
            BillingInsertViewModel model;
            using (var s = new SIM_Entities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                ViewBag.Noreg = m.RegNo;
                ViewBag.Section = m.SectionID;
                ViewBag.Nomor = m.Nomor;

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    nobukti = m.NoBukti,
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.OK_DokterBedahID,
                    opetatorpelaksasa = m.OK_DokterBedahID2,
                    durante = m.OK_DokterDuranteID,
                    dokterasisten = m.OK_DOkterAssBedah,
                    dokterasisten2 = m.OK_DOkterAssBedah2,
                    dokteranak = m.OK_DokterANak,
                    dokteranastesi = m.OK_DokterAnasID,
                    cito = m.Cyto ?? false,
                    kelas = m.KdKelas,
                    jenisanatesi = m.OK_JenisAnastesi,
                    spesialis = m.SpesialisasiID,
                    ruangok = m.RuangOK,
                    cob = (bool)(m.COB == null ? false : m.COB),
                    mcu = m.MCU == null ? false : m.MCU,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                    kategorioperasi = m.OK_KategoriOperasiID.ToString(),
                    asistenanastesi = new List<SelectListItemViewModel>(),
                    instrumen = new List<SelectListItemViewModel>(),
                    onloop = new List<SelectListItemViewModel>()
                };
                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter, Check1 = x.D });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter, Check1 = x.D });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter, Check1 = x.D });
                }

                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);

                if (spesialisasi != null) ViewBag.SpesialisasiName = spesialisasi.SpesialisName;
                if (ruangok != null) ViewBag.RuangOKName = ruangok.RuangOK;

                ViewBag.PenanggungNama = m.OK_PenanggungNama;
                ViewBag.PenanggungAlamat = m.OK_PenanggungAlamat;
                ViewBag.PenanggungHubungan = m.OK_PenanggungHub;
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                ViewBag.JenisAnatesiName = jenisanatesi == null ? "" : jenisanatesi.Deskripsi;
                ViewBag.RuangOKName = ruangok == null ? "" : ruangok.RuangOK;
                var kategorioperasi = s.SIMmKategoriOperasi.FirstOrDefault(x => x.KategoriID == m.OK_KategoriOperasiID);
                ViewBag.KategoriOperasiName = kategorioperasi == null ? "" : kategorioperasi.KategoriName;
                var jenisluka = s.SIMmJenisLuka.FirstOrDefault(x => x.KodeLuka == m.JenisLukaID);
                ViewBag.JenisLukaName = jenisluka == null ? "" : jenisluka.Deskripsi;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;

                var detailid = 0;
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Operator = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.opetatorpelaksasa);
                ViewBag.OperatorPelaksana = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.durante);
                ViewBag.Durante = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten);
                ViewBag.DokterAsisten = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten2);
                ViewBag.DokterAsisten2 = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranak);
                ViewBag.DokterAnak = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranastesi);
                ViewBag.Anastesi = dokter != null ? dokter.NamaDOkter : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("EditOK")]
        public string EditOK_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {

                                #region Delete
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                if (simtrregistrasi.StatusBayar != "Belum") throw new Exception("Pasien sudah melakukan pembayaran");

                                if (simtrregistrasi.CloseAdmin == true) throw new Exception("Data Pasien sudah terclosing admin");

                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat diubah karena sudah di audit");

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg);

                                var nobukti = SIMtrRJ.NoBukti;
                                var userid = User.Identity.GetUserId();
                                #endregion

                                checkDetailKomponen(item);

                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }
                                var SIMtrOperasiAnastesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrOperasiAnastesi != null)
                                {
                                    s.SIMtrOperasiAnastesi.RemoveRange(SIMtrOperasiAnastesi);
                                }
                                var SIMtrOperasiInstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == item.nobukti);
                                if (SIMtrOperasiInstrumen != null)
                                {
                                    s.SIMtrOperasiInstrumen.RemoveRange(SIMtrOperasiInstrumen);
                                }
                                var SIMtrOperasiOnLoop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrOperasiOnLoop != null)
                                {
                                    s.SIMtrOperasiOnLoop.RemoveRange(SIMtrOperasiOnLoop);
                                }
                                #endregion


                                SIMtrRJ.JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID;
                                SIMtrRJ.Tanggal = item.tanggal;
                                SIMtrRJ.Jam = item.tanggal + item.jam;
                                SIMtrRJ.SectionID = item.section;
                                SIMtrRJ.DokterID = item.dokter;
                                SIMtrRJ.KdKelas = item.kelas;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.SectionAsalID = reg.SectionAsalID;
                                SIMtrRJ.OK_JenisAnastesi = item.jenisanatesi;
                                SIMtrRJ.Cyto = item.cito;
                                SIMtrRJ.SpesialisasiID = item.spesialis;
                                SIMtrRJ.RuangOK = item.ruangok;
                                SIMtrRJ.OK_KategoriOperasiID = string.IsNullOrEmpty(item.kategorioperasi) ? (byte?)null : 1;
                                SIMtrRJ.JenisLukaID = item.jenisluka;
                                SIMtrRJ.DokterPengirimID = item.dokterpengirim;
                                SIMtrRJ.OK_DokterBedahID = item.dokter;
                                SIMtrRJ.OK_DokterBedahID2 = item.opetatorpelaksasa;
                                SIMtrRJ.OK_DokterDuranteID = item.durante;
                                SIMtrRJ.OK_DOkterAssBedah = item.dokterasisten;
                                SIMtrRJ.OK_DOkterAssBedah2 = item.dokterasisten2;
                                SIMtrRJ.OK_DokterANak = item.dokteranak;
                                SIMtrRJ.OK_DokterAnasID = item.dokteranastesi;
                                SIMtrRJ.OK_PenanggungNama = simtrregistrasi.PenanggungNama;
                                SIMtrRJ.OK_PenanggungAlamat = simtrregistrasi.PenanggungAlamat;
                                SIMtrRJ.OK_PenanggungHub = simtrregistrasi.PenanggungHubungan;
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.MCU = item.mcu;
                                SIMtrRJ.COB = item.cob;
                                SIMtrRJ.Audit = item.audit;
                                SIMtrRJ.AdaObat = item.adaobat;
                                SIMtrRJ.UserIDWeb = userid;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.tanggal + item.jam,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.tanggal + item.jam,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon,
                                        Keterangan = insertKeteranganInvoiceJasa(s, x.jasaid, simtrregistrasi.KdKelas, int.Parse(item.kategorioperasi), x.dokter, item.noreg, item.nomor),
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }
                                if (item.asistenanastesi == null) item.asistenanastesi = new List<SelectListItemViewModel>();
                                foreach (var x in item.asistenanastesi)
                                {
                                    s.SIMtrOperasiAnastesi.Add(new SIMtrOperasiAnastesi() { NoBukti = nobukti, DokterAsistenAnas = x.Value, D = x.Check1 });
                                }
                                if (item.instrumen == null) item.instrumen = new List<SelectListItemViewModel>();
                                foreach (var x in item.instrumen)
                                {
                                    s.SIMtrOperasiInstrumen.Add(new SIMtrOperasiInstrumen() { NoBUkti = nobukti, DokterInstrumenID = x.Value, D = x.Check1 });
                                }
                                if (item.onloop == null) item.onloop = new List<SelectListItemViewModel>();
                                foreach (var x in item.onloop)
                                {
                                    s.SIMtrOperasiOnLoop.Add(new SIMtrOperasiOnLoop() { NoBukti = nobukti, DokterOnLoopID = x.Value, D = x.Check1 });
                                }

                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                //var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                //simtrdataregpasien.SudahPeriksa = true;
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing OK Edit {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();

                                //insert auto BHP jasa
                                insetAutoBHPJasa(s, nobukti);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== VK
        [HttpGet]
        [ActionName("CreateVK")]
        public ActionResult CreateVK_Get(string noreg, string section, int nomor)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;

            checkInsertTask(noreg);
            RegistrasiPasienViewModel model;
            using (var s = new SIM_Entities())
            {
                ViewBag.LoginType = Request.Cookies["LoginTipe"].Value.ToUpper();
                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var reg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                if (m == null) return HttpNotFound();
                model = IConverter.Cast<RegistrasiPasienViewModel>(m);
                ViewBag.NRM = m.NRM;
                ViewBag.NamaPasien = m.NamaPasien;
                ViewBag.JenisKelamin = m.JenisKelamin;
                ViewBag.SectionAsalID = m.SectionAsalID;
                ViewBag.SectionAsalName = sectionasal.SectionName;
                ViewBag.PenanggungNama = reg.PenanggungNama;
                ViewBag.PenanggungAlamat = reg.PenanggungAlamat;
                ViewBag.PenanggungHubungan = reg.PenanggungHubungan;
            }
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("CreateVK")]
        public string CreateVK_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg && x.Nomor == item.nomor);
                                var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                checkDetailKomponen(item);

                                var userid = User.Identity.GetUserId();

                                var rj = s.Pelayanan_Insert_SIMtrRJ_VK(
                                    simtrregistrasi.RawatInap,
                                    item.noreg,
                                    item.nomor,
                                    simtrregistrasi.JenisKerjasamaID,
                                    item.tanggal,
                                    item.tanggal + item.jam,
                                    item.section,
                                    item.dokter,
                                    item.kelas,
                                    simtrregistrasi.NRM,
                                    item.adaobat,
                                    item.audit,
                                    userid,
                                    reg.SectionAsalID,
                                    simtrreg.PenanggungNama,
                                    simtrreg.PenanggungAlamat,
                                    simtrreg.PenanggungHubungan,
                                    item.dokter,
                                    item.opetatorpelaksasa,
                                    item.durante,
                                    item.dokterasisten,
                                    item.dokteranak,
                                    item.dokteranastesi,
                                    item.dokter,
                                    item.mcu,
                                    item.cob
                                ).FirstOrDefault();

                                var nobukti = rj;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.tanggal + item.jam,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.tanggal + item.jam,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon,
                                        Keterangan = insertKeteranganInvoiceJasa(s, x.jasaid, simtrregistrasi.KdKelas, 1, x.dokter, item.noreg, item.nomor),
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    decimal sumHarga = 0;
                                    if(x.detail == null) throw new Exception("Detail komponen masih kosong");
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        sumHarga += y.harga;
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }

                                if (item.asistenanastesi == null) item.asistenanastesi = new List<SelectListItemViewModel>();
                                foreach (var x in item.asistenanastesi)
                                {
                                    s.SIMtrOperasiAnastesi.Add(new SIMtrOperasiAnastesi() { NoBukti = nobukti, DokterAsistenAnas = x.Value });
                                }
                                if (item.instrumen == null) item.instrumen = new List<SelectListItemViewModel>();
                                foreach (var x in item.instrumen)
                                {
                                    s.SIMtrOperasiInstrumen.Add(new SIMtrOperasiInstrumen() { NoBUkti = nobukti, DokterInstrumenID = x.Value });
                                }

                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                simtrdataregpasien.SudahPeriksa = true;
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Edit {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();

                                //insert auto BHP jasa
                                insetAutoBHPJasa(s, nobukti);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public ActionResult DetailVK(string nobukti)
        {
            var tl = Request.Cookies["LoginTipe"].Value.ToUpper();
            BillingInsertViewModel model;
            using (var s = new SIM_Entities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.OK_DokterBedahID,
                    opetatorpelaksasa = m.OK_DokterBedahID2,
                    durante = m.OK_DokterDuranteID,
                    dokterasisten = m.OK_DOkterAssBedah,
                    dokterasisten2 = m.OK_DOkterAssBedah2,
                    dokteranak = m.OK_DokterANak,
                    dokteranastesi = m.OK_DokterAnasID,
                    cito = m.Cyto ?? false,
                    cob = (bool)(m.COB == null ? false : m.COB),
                    mcu = m.MCU == null ? false : m.MCU,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                    asistenanastesi = new List<SelectListItemViewModel>(),
                    instrumen = new List<SelectListItemViewModel>(),
                    onloop = new List<SelectListItemViewModel>()
                };
                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);
                ViewBag.PenanggungNama = m.OK_PenanggungNama;
                ViewBag.PenanggungAlamat = m.OK_PenanggungAlamat;
                ViewBag.PenanggungHubungan = m.OK_PenanggungHub;
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                ViewBag.JenisAnatesiName = jenisanatesi == null ? "" : jenisanatesi.Deskripsi;
                ViewBag.RuangOKName = ruangok == null ? "" : ruangok.RuangOK;
                var kategorioperasi = s.SIMmKategoriOperasi.FirstOrDefault(x => x.KategoriID == m.OK_KategoriOperasiID);
                ViewBag.KategoriOperasiName = kategorioperasi == null ? "" : kategorioperasi.KategoriName;
                var jenisluka = s.SIMmJenisLuka.FirstOrDefault(x => x.KodeLuka == m.JenisLukaID);
                ViewBag.JenisLukaName = jenisluka == null ? "" : jenisluka.Deskripsi;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;

                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Operator = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.opetatorpelaksasa);
                ViewBag.OperatorPelaksana = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.durante);
                ViewBag.Durante = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten);
                ViewBag.DokterAsisten = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten2);
                ViewBag.DokterAsisten2 = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranak);
                ViewBag.DokterAnak = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranastesi);
                ViewBag.Anastesi = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterpengirim);
                ViewBag.Pengirim = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranastesi);
                ViewBag.Anastesi = dokter != null ? dokter.NamaDOkter : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("EditVK")]
        public ActionResult EditVK_Get(string nobukti)
        {
            var tl = Request.Cookies["LoginTipe"].Value.ToUpper();
            BillingInsertViewModel model;
            using (var s = new SIM_Entities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                ViewBag.Noreg = m.RegNo;
                ViewBag.Section = m.SectionID;
                ViewBag.Nomor = m.Nomor;

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    nobukti = m.NoBukti,
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = (int)m.Nomor,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.OK_DokterBedahID,
                    opetatorpelaksasa = m.OK_DokterBedahID2,
                    durante = m.OK_DokterDuranteID,
                    dokterasisten = m.OK_DOkterAssBedah,
                    dokterasisten2 = m.OK_DOkterAssBedah2,
                    dokteranak = m.OK_DokterANak,
                    dokteranastesi = m.OK_DokterAnasID,
                    cito = m.Cyto ?? false,
                    cob = (bool)(m.COB == null ? false : m.COB),
                    mcu = m.MCU == null ? false : m.MCU,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                    asistenanastesi = new List<SelectListItemViewModel>(),
                    instrumen = new List<SelectListItemViewModel>(),
                    onloop = new List<SelectListItemViewModel>()
                };
                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);
                ViewBag.PenanggungNama = m.OK_PenanggungNama;
                ViewBag.PenanggungAlamat = m.OK_PenanggungAlamat;
                ViewBag.PenanggungHubungan = m.OK_PenanggungHub;
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                ViewBag.JenisAnatesiName = jenisanatesi == null ? "" : jenisanatesi.Deskripsi;
                ViewBag.RuangOKName = ruangok == null ? "" : ruangok.RuangOK;
                var kategorioperasi = s.SIMmKategoriOperasi.FirstOrDefault(x => x.KategoriID == m.OK_KategoriOperasiID);
                ViewBag.KategoriOperasiName = kategorioperasi == null ? "" : kategorioperasi.KategoriName;
                var jenisluka = s.SIMmJenisLuka.FirstOrDefault(x => x.KodeLuka == m.JenisLukaID);
                ViewBag.JenisLukaName = jenisluka == null ? "" : jenisluka.Deskripsi;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;

                var detailid = 0;
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Operator = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.opetatorpelaksasa);
                ViewBag.OperatorPelaksana = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.durante);
                ViewBag.Durante = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten);
                ViewBag.DokterAsisten = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten2);
                ViewBag.DokterAsisten2 = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranak);
                ViewBag.DokterAnak = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranastesi);
                ViewBag.Anastesi = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterpengirim);
                ViewBag.Pengirim = dokter != null ? dokter.NamaDOkter : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("EditVK")]
        public string EditVK_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Delete
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                if (simtrregistrasi.StatusBayar != "Belum") throw new Exception("Pasien sudah melakukan pembayaran");

                                if (simtrregistrasi.CloseAdmin == true) throw new Exception("Data Pasien sudah terclosing admin");

                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat diubah karena sudah di audit");

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg);

                                var nobukti = SIMtrRJ.NoBukti;
                                var userid = User.Identity.GetUserId();
                                #endregion

                                checkDetailKomponen(item);


                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }
                                var SIMtrOperasiAnastesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrOperasiAnastesi != null)
                                {
                                    s.SIMtrOperasiAnastesi.RemoveRange(SIMtrOperasiAnastesi);
                                }
                                var SIMtrOperasiInstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == item.nobukti);
                                if (SIMtrOperasiInstrumen != null)
                                {
                                    s.SIMtrOperasiInstrumen.RemoveRange(SIMtrOperasiInstrumen);
                                }
                                #endregion

                                SIMtrRJ.KdKelas = item.kelas;
                                SIMtrRJ.Nomor = item.nomor;
                                SIMtrRJ.OK_PenanggungNama = simtrregistrasi.PenanggungNama;
                                SIMtrRJ.OK_PenanggungAlamat = simtrregistrasi.PenanggungAlamat;
                                SIMtrRJ.OK_PenanggungHub = simtrregistrasi.PenanggungHubungan;
                                SIMtrRJ.OK_DokterBedahID = item.dokter;
                                SIMtrRJ.OK_DokterBedahID2 = item.opetatorpelaksasa;
                                SIMtrRJ.OK_DokterDuranteID = item.durante;
                                SIMtrRJ.OK_DOkterAssBedah = item.dokterasisten;
                                SIMtrRJ.OK_DokterANak = item.dokteranak;
                                SIMtrRJ.OK_DokterAnasID = item.dokteranastesi;
                                SIMtrRJ.DokterID = item.dokter;
                                SIMtrRJ.JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID;
                                SIMtrRJ.Tanggal = item.tanggal;
                                SIMtrRJ.Jam = item.tanggal + item.jam;
                                SIMtrRJ.SectionID = item.section;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.DokterPengirimID = item.dokterpengirim;
                                SIMtrRJ.MCU = item.mcu;
                                SIMtrRJ.COB = item.cob;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.tanggal + item.jam,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.tanggal + item.jam,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon,
                                        Keterangan = insertKeteranganInvoiceJasa(s, x.jasaid, simtrregistrasi.KdKelas, 1, x.dokter, item.noreg, item.nomor),
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }
                                if (item.asistenanastesi == null) item.asistenanastesi = new List<SelectListItemViewModel>();
                                foreach (var x in item.asistenanastesi)
                                {
                                    s.SIMtrOperasiAnastesi.Add(new SIMtrOperasiAnastesi() { NoBukti = nobukti, DokterAsistenAnas = x.Value });
                                }
                                if (item.instrumen == null) item.instrumen = new List<SelectListItemViewModel>();
                                foreach (var x in item.instrumen)
                                {
                                    s.SIMtrOperasiInstrumen.Add(new SIMtrOperasiInstrumen() { NoBUkti = nobukti, DokterInstrumenID = x.Value });
                                }

                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                simtrdataregpasien.SudahPeriksa = true;
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Edit {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();

                                //insert auto BHP jasa
                                insetAutoBHPJasa(s, nobukti);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== LAB
        [HttpGet]
        [ActionName("CreateLAB")]
        public ActionResult CreateLAB_Get(string noreg, string section, int nomor)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;

            checkInsertTask(noreg);

            var model = new BillingInsertViewModel();
            model.detail = new List<BillingInsertDetailViewModel>();
            using (var s = new SIM_Entities())
            {
                ViewBag.ShowKomponen = checkOtorisasiShowKomponen(s);

                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var reg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                if (m == null) return HttpNotFound();

                ViewBag.NRM = m.NRM;
                ViewBag.NamaPasien = m.NamaPasien;
                ViewBag.JenisKelamin = m.JenisKelamin;
                ViewBag.SectionAsalID = m.SectionAsalID;
                ViewBag.SectionAsalName = sectionasal.SectionName;
                ViewBag.PenanggungNama = reg.PenanggungNama;
                ViewBag.PenanggungAlamat = reg.PenanggungAlamat;
                ViewBag.PenanggungHubungan = reg.PenanggungHubungan;
                ViewBag.Keterangan = reg.Keterangan;
                ViewBag.Nomor = m.Nomor;
                ViewBag.NoReg = m.NoReg;
                ViewBag.Section = m.SectionID;
                var d = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                ViewBag.DokterPengirim = d != null ? d.NamaDOkter : "";

                if ((m.Memo != null || m.Memo == "") && (m.NoBill == null || m.NoBill == ""))
                {
                    var detailid = 0;
                    var jj = s.EMR_listOrderPenunjang_DetailJasa_PerNobukti(m.Memo).ToList();
                    var order = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == m.Memo);
                    if (order != null) ViewBag.IndikasiKlinis = order.DiagnosaIndikasi;
                    foreach (var x in jj)
                    {
                        var mJasa = s.SIMmListJasa.FirstOrDefault(xx => xx.JasaID == x.JasaID);
                        var h = s.GetTarifBiaya_Global(x.JasaID, m.DokterID, (x.Cyto == true ? 1 : 0), 1, sectionasal.SectionID, noreg, "1").FirstOrDefault();
                        var detail = new BillingInsertDetailViewModel()
                        {
                            diskon = 0,
                            jasaid = x.JasaID,
                            jasanama = mJasa.JasaName,
                            qty = (int)x.Qty,
                            tarif = (decimal)h.Harga,
                            dokter = m.DokterID == null ? "" : m.DokterID,
                            namadokter = d != null ? d.NamaDOkter : "",
                            id = detailid,
                            detail = new List<BillingInsertDetailJasaViewModel>()
                        };
                        detailid++;
                        
                        var dd = s.GetDetailKomponenTarif_Global(h.ListHargaID, h.CustomerKerjasamaID, h.TarifBaru, reg.JenisKerjasamaID, "1", "1").ToList();
                        foreach (var q in dd)
                        {
                            detail.detail.Add(new BillingInsertDetailJasaViewModel()
                            {
                                komponenid = q.KomponenBiayaID,
                                komponennama = q.KomponenName,
                                harga = (decimal)q.HargaBaru,
                                diskon = (decimal) 0,
                                diskonnilai = 0,
                                jumlah = (decimal)q.HargaBaru
                            });
                        }

                        model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                        model.detail.Add(detail);
                    }
                }

            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("CreateLAB")]
        public string CreateLAB_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg && x.Nomor == item.nomor);
                                var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                checkDetailKomponen(item);

                                var userid = User.Identity.GetUserId();

                                var rj = s.Pelayanan_Insert_SIMtrRJ_LAB(
                                    simtrregistrasi.RawatInap,
                                    item.noreg,
                                    item.nomor,
                                    simtrregistrasi.JenisKerjasamaID,
                                    item.tanggal,
                                    item.tanggal + item.jam,
                                    item.section,
                                    item.dokter,
                                    item.kelas,
                                    simtrregistrasi.NRM,
                                    item.adaobat,
                                    item.audit,
                                    userid,
                                    reg.SectionAsalID,
                                    reg.Keterangan,
                                    reg.DokterID,
                                    item.dokteranalis,
                                    item.dirujuk,
                                    item.dirujuk ? item.dirujukventor : "",
                                    item.dirujuk ? item.tanggaldirujukkirim : (DateTime?)null,
                                    item.dirujuk ? item.tanggaldirujukditerima : (DateTime?)null,
                                    item.dirujuk ? item.alasandirujuk : string.Empty,
                                    item.mcu,
                                    item.cob
                                ).FirstOrDefault();

                                var nobukti = rj;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.tanggal + item.jam,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.tanggal + item.jam,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon,
                                        Keterangan = insertKeteranganInvoiceJasa(s, x.jasaid, simtrregistrasi.KdKelas, 1, x.dokter, item.noreg, item.nomor),
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }

                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                simtrdataregpasien.SudahPeriksa = true;
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Create {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();

                                //insert auto BHP jasa
                                insetAutoBHPJasa(s, nobukti);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public ActionResult DetailLAB(string nobukti)
        {
            var tl = Request.Cookies["LoginTipe"].Value.ToUpper();
            BillingInsertViewModel model;
            using (var s = new SIM_Entities())
            {
                ViewBag.ShowKomponen = checkOtorisasiShowKomponen(s);

                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.DokterID,
                    dirujuk = m.Dirujuk ?? false,
                    dokteranalis = m.AnalisID,
                    tanggaldirujukditerima = m.TglTerima,
                    tanggaldirujukkirim = m.TglKirim,
                    dirujukventor = m.DirujukVendorID,
                    alasandirujuk = m.AlasanDirujukID,
                    cob = (bool)(m.COB == null ? false : m.COB),
                    mcu = m.MCU == null ? false : m.MCU,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                };
                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.SupplierPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;
                ViewBag.Keterangan = m.Keterangan;

                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Dokter = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranalis);
                ViewBag.Radiografer = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dirujukventor);
                ViewBag.Vendor = dokter != null ? dokter.NamaDOkter : "";

                var alasandirujuk = s.SIMmAlasanDirujuk.FirstOrDefault(x => x.AlasanDirujukID == model.alasandirujuk);
                ViewBag.Alasan = alasandirujuk != null ? alasandirujuk.AlasanDirujuk : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("EditLAB")]
        public ActionResult EditLAB_Get(string nobukti)
        {
            var tl = Request.Cookies["LoginTipe"].Value.ToUpper();
            BillingInsertViewModel model;
            using (var s = new SIM_Entities())
            {
                ViewBag.ShowKomponen = checkOtorisasiShowKomponen(s);

                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                ViewBag.Noreg = m.RegNo;
                ViewBag.Section = m.SectionID;
                ViewBag.Nomor = m.Nomor;

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nobukti = m.NoBukti,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.DokterID,
                    dirujuk = m.Dirujuk ?? false,
                    dokteranalis = m.AnalisID,
                    tanggaldirujukditerima = m.TglTerima,
                    tanggaldirujukkirim = m.TglKirim,
                    dirujukventor = m.DirujukVendorID,
                    alasandirujuk = m.AlasanDirujukID,
                    cob = (bool)(m.COB == null ? false : m.COB),
                    mcu = m.MCU == null ? false : m.MCU,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                };
                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.SupplierPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;
                ViewBag.Keterangan = m.Keterangan;

                var detailid = 0;
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Dokter = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranalis);
                ViewBag.Radiografer = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dirujukventor);
                ViewBag.Vendor = dokter != null ? dokter.NamaDOkter : "";

                var alasandirujuk = s.SIMmAlasanDirujuk.FirstOrDefault(x => x.AlasanDirujukID == model.alasandirujuk);
                ViewBag.Alasan = alasandirujuk != null ? alasandirujuk.AlasanDirujuk : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("EditLAB")]
        public string EditLAB_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Delete
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                if (simtrregistrasi.StatusBayar != "Belum") throw new Exception("Pasien sudah melakukan pembayaran");

                                if (simtrregistrasi.CloseAdmin == true) throw new Exception("Data Pasien sudah terclosing admin");

                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat diubah karena sudah di audit");

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg);

                                var nobukti = SIMtrRJ.NoBukti;
                                var userid = User.Identity.GetUserId();
                                #endregion

                                checkDetailKomponen(item);

                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }
                                var SIMtrOperasiAnastesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrOperasiAnastesi != null)
                                {
                                    s.SIMtrOperasiAnastesi.RemoveRange(SIMtrOperasiAnastesi);
                                }
                                var SIMtrOperasiInstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == item.nobukti);
                                if (SIMtrOperasiInstrumen != null)
                                {
                                    s.SIMtrOperasiInstrumen.RemoveRange(SIMtrOperasiInstrumen);
                                }
                                #endregion

                                SIMtrRJ.Keterangan = reg.Keterangan;
                                SIMtrRJ.SupplierPengirimID = reg.DokterID;
                                SIMtrRJ.DokterID = item.dokter;
                                SIMtrRJ.AnalisID = item.dokteranalis;
                                SIMtrRJ.Dirujuk = item.dirujuk;
                                SIMtrRJ.DirujukVendorID = item.dirujuk ? item.dirujukventor : "";
                                SIMtrRJ.TglKirim = item.dirujuk ? item.tanggaldirujukkirim : (DateTime?)null;
                                SIMtrRJ.TglTerima = item.dirujuk ? item.tanggaldirujukditerima : (DateTime?)null;
                                SIMtrRJ.JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID;
                                SIMtrRJ.AlasanDirujukID = item.dirujuk ? item.alasandirujuk : string.Empty;
                                SIMtrRJ.Tanggal = item.tanggal;
                                SIMtrRJ.Jam = item.tanggal + item.jam;
                                SIMtrRJ.SectionID = item.section;
                                SIMtrRJ.KdKelas = simtrregistrasi.KdKelas;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.UserIDWeb = userid;
                                SIMtrRJ.MCU = item.mcu;
                                SIMtrRJ.COB = item.cob;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.tanggal + item.jam,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.tanggal + item.jam,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }

                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Edit {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();

                                //insert auto BHP jasa
                                insetAutoBHPJasa(s, nobukti);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== RAD
        [HttpGet]
        [ActionName("CreateRAD")]
        public ActionResult CreateRAD_Get(string noreg, string section, int nomor)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;

            checkInsertTask(noreg);

            var model = new BillingInsertViewModel();
            model.detail = new List<BillingInsertDetailViewModel>();
            using (var s = new SIM_Entities())
            {
                ViewBag.ShowKomponen = checkOtorisasiShowKomponen(s);

                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var reg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                if (m == null) return HttpNotFound();

                ViewBag.NRM = m.NRM;
                ViewBag.NamaPasien = m.NamaPasien;
                ViewBag.JenisKelamin = m.JenisKelamin;
                ViewBag.SectionAsalID = m.SectionAsalID;
                ViewBag.SectionAsalName = sectionasal.SectionName;
                ViewBag.PenanggungNama = reg.PenanggungNama;
                ViewBag.PenanggungAlamat = reg.PenanggungAlamat;
                ViewBag.PenanggungHubungan = reg.PenanggungHubungan;
                ViewBag.Keterangan = reg.Keterangan;
                var d = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                ViewBag.DokterPengirim = d != null ? d.NamaDOkter : "";

                if ((m.Memo != null || m.Memo == "") && (m.NoBill == null || m.NoBill == ""))
                {
                    var detailid = 0;
                    var jj = s.EMR_listOrderPenunjang_DetailJasa_PerNobukti(m.Memo).ToList();
                    var order = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == m.Memo);
                    if (order != null) ViewBag.IndikasiKlinis = order.DiagnosaIndikasi;
                    foreach (var x in jj)
                    {
                        var mJasa = s.SIMmListJasa.FirstOrDefault(xx => xx.JasaID == x.JasaID);
                        if (mJasa != null)
                        {
                            var h = s.GetTarifBiaya_Global(x.JasaID, m.DokterID, (x.Cyto == true ? 1 : 0), 1, sectionasal.SectionID, noreg, "1").FirstOrDefault();
                            var detail = new BillingInsertDetailViewModel()
                            {
                                diskon = 0,
                                jasaid = x.JasaID,
                                jasanama = mJasa.JasaName,
                                qty = (int)x.Qty,
                                tarif = (decimal)h.Harga,
                                dokter = m.DokterID == null ? "" : m.DokterID,
                                namadokter = d != null ? d.NamaDOkter : "",
                                id = detailid,
                                detail = new List<BillingInsertDetailJasaViewModel>()
                            };
                            detailid++;

                            var dd = s.GetDetailKomponenTarif_Global(h.ListHargaID, h.CustomerKerjasamaID, h.TarifBaru, reg.JenisKerjasamaID, "1", "1").ToList();
                            foreach (var q in dd)
                            {
                                detail.detail.Add(new BillingInsertDetailJasaViewModel()
                                {
                                    komponenid = q.KomponenBiayaID,
                                    komponennama = q.KomponenName,
                                    harga = (decimal)q.HargaBaru,
                                    diskon = (decimal)0,
                                    diskonnilai = 0,
                                    jumlah = (decimal)q.HargaBaru
                                });
                            }

                            model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                            model.detail.Add(detail);
                        }
                    }
                }

            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("CreateRAD")]
        public string CreateRAD_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg && x.Nomor == item.nomor);
                                var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                checkDetailKomponen(item);

                                var userid = User.Identity.GetUserId();

                                var rj = s.Pelayanan_Insert_SIMtrRJ_RAD(
                                    simtrregistrasi.RawatInap,
                                    item.noreg,
                                    item.nomor,
                                    simtrregistrasi.JenisKerjasamaID,
                                    item.tanggal,
                                    item.tanggal + item.jam,
                                    item.section,
                                    item.dokter,
                                    item.kelas,
                                    simtrregistrasi.NRM,
                                    item.adaobat,
                                    item.audit,
                                    userid,
                                    reg.SectionAsalID,
                                    reg.Keterangan,
                                    reg.DokterID,
                                    item.dokterpengirim,
                                    item.dokteranalis,
                                    item.dirujuk,
                                    item.dirujuk ? item.dirujukventor : "",
                                    item.dirujuk ? item.tanggaldirujukkirim : (DateTime?)null,
                                    item.dirujuk ? item.tanggaldirujukditerima : (DateTime?)null,
                                    item.dirujuk ? item.alasandirujuk : string.Empty,
                                    item.mcu,
                                    item.cob
                                ).FirstOrDefault();

                                var nobukti = rj;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.tanggal + item.jam,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.tanggal + item.jam,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon,
                                        Keterangan = insertKeteranganInvoiceJasa(s, x.jasaid, simtrregistrasi.KdKelas, 1, x.dokter, item.noreg, item.nomor),
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }

                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                simtrdataregpasien.SudahPeriksa = true;
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Create {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();

                                //insert auto BHP jasa
                                insetAutoBHPJasa(s, nobukti);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public ActionResult DetailRAD(string nobukti)
        {
            var tl = Request.Cookies["LoginTipe"].Value.ToUpper();
            BillingInsertViewModel model;
            using (var s = new SIM_Entities())
            {
                ViewBag.ShowKomponen = checkOtorisasiShowKomponen(s);

                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.DokterID,
                    dirujukventor = m.DirujukVendorID,
                    alasandirujuk = m.AlasanDirujukID,
                    dirujuk = m.Dirujuk ?? false,
                    dokteranalis = m.AnalisID,
                    tanggaldirujukditerima = m.TglTerima,
                    tanggaldirujukkirim = m.TglKirim,
                    cob = (bool)(m.COB == null ? false : m.COB),
                    mcu = m.MCU == null ? false : m.MCU,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                };
                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.SupplierPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;
                ViewBag.Keterangan = m.Keterangan;

                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Dokter = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranalis);
                ViewBag.Radiografer = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dirujukventor);
                ViewBag.Vendor = dokter != null ? dokter.NamaDOkter : "";

                var alasandirujuk = s.SIMmAlasanDirujuk.FirstOrDefault(x => x.AlasanDirujukID == model.alasandirujuk);
                ViewBag.Alasan = alasandirujuk != null ? alasandirujuk.AlasanDirujuk : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("EditRAD")]
        public ActionResult EditRAD_Get(string nobukti)
        {
            var tl = Request.Cookies["LoginTipe"].Value.ToUpper();
            BillingInsertViewModel model;
            using (var s = new SIM_Entities())
            {
                ViewBag.ShowKomponen = checkOtorisasiShowKomponen(s);

                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                ViewBag.Noreg = m.RegNo;
                ViewBag.Section = m.SectionID;
                ViewBag.Nomor = m.Nomor;

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    nobukti = m.NoBukti,
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.SupplierPengirimID,
                    dokter = m.DokterID,
                    dirujuk = m.Dirujuk ?? false,
                    dokteranalis = m.AnalisID,
                    dirujukventor = m.DirujukVendorID,
                    tanggaldirujukditerima = m.TglTerima,
                    tanggaldirujukkirim = m.TglKirim,
                    cob = (bool)(m.COB == null ? false : m.COB),
                    mcu = m.MCU == null ? false : m.MCU,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                };
                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.SupplierPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;
                ViewBag.Keterangan = m.Keterangan;

                var detailid = 0;
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Dokter = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterpengirim);
                ViewBag.DokterPengirim = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranalis);
                ViewBag.Radiografer = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dirujukventor);
                ViewBag.Vendor = dokter != null ? dokter.NamaDOkter : "";

                var alasandirujuk = s.SIMmAlasanDirujuk.FirstOrDefault(x => x.AlasanDirujukID == model.alasandirujuk);
                ViewBag.Alasan = alasandirujuk != null ? alasandirujuk.AlasanDirujuk : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("EditRAD")]
        public string EditRAD_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Delete
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                if (simtrregistrasi.StatusBayar != "Belum") throw new Exception("Pasien sudah melakukan pembayaran");

                                if (simtrregistrasi.CloseAdmin == true) throw new Exception("Data Pasien sudah terclosing admin");

                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat diubah karena sudah di audit");

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg);

                                var nobukti = SIMtrRJ.NoBukti;
                                var userid = User.Identity.GetUserId();
                                #endregion

                                checkDetailKomponen(item);

                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }
                                var SIMtrOperasiAnastesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrOperasiAnastesi != null)
                                {
                                    s.SIMtrOperasiAnastesi.RemoveRange(SIMtrOperasiAnastesi);
                                }
                                var SIMtrOperasiInstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == item.nobukti);
                                if (SIMtrOperasiInstrumen != null)
                                {
                                    s.SIMtrOperasiInstrumen.RemoveRange(SIMtrOperasiInstrumen);
                                }
                                #endregion

                                SIMtrRJ.Keterangan = reg.Keterangan;
                                SIMtrRJ.SupplierPengirimID = reg.DokterID;
                                SIMtrRJ.DokterID = item.dokter;
                                SIMtrRJ.AnalisID = item.dokteranalis;
                                SIMtrRJ.Dirujuk = item.dirujuk;
                                SIMtrRJ.DirujukVendorID = item.dirujuk ? item.dirujukventor : "";
                                SIMtrRJ.TglKirim = item.dirujuk ? item.tanggaldirujukkirim : (DateTime?)null;
                                SIMtrRJ.TglTerima = item.dirujuk ? item.tanggaldirujukditerima : (DateTime?)null;
                                SIMtrRJ.JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID;
                                SIMtrRJ.AlasanDirujukID = item.dirujuk ? item.alasandirujuk : string.Empty;
                                SIMtrRJ.Tanggal = item.tanggal;
                                SIMtrRJ.Jam = item.tanggal + item.jam;
                                SIMtrRJ.SectionID = item.section;
                                SIMtrRJ.KdKelas = simtrregistrasi.KdKelas;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.Audit = item.audit;
                                SIMtrRJ.AdaObat = item.adaobat;
                                SIMtrRJ.UserIDWeb = userid;
                                SIMtrRJ.MCU = item.mcu;
                                SIMtrRJ.COB = item.cob;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.tanggal + item.jam,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.tanggal + item.jam,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon,
                                        Keterangan = insertKeteranganInvoiceJasa(s, x.jasaid, simtrregistrasi.KdKelas, 1, x.dokter, item.noreg, item.nomor),
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }

                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                simtrdataregpasien.SudahPeriksa = true;
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Create {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();

                                //insert auto BHP jasa
                                insetAutoBHPJasa(s, nobukti);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== HD
        [HttpGet]
        [ActionName("CreateHD")]
        public ActionResult CreateHD_Get(string noreg, string section, int nomor)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;

            checkInsertTask(noreg);

            RegistrasiPasienViewModel model;
            using (var s = new SIM_Entities())
            {
                ViewBag.ShowKomponen = checkOtorisasiShowKomponen(s);

                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var reg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                if (m == null) return HttpNotFound();
                model = IConverter.Cast<RegistrasiPasienViewModel>(m);
                ViewBag.NRM = m.NRM;
                ViewBag.NamaPasien = m.NamaPasien;
                ViewBag.JenisKelamin = m.JenisKelamin;
                ViewBag.SectionAsalID = m.SectionAsalID;
                ViewBag.SectionAsalName = sectionasal.SectionName;
                ViewBag.PenanggungNama = reg.PenanggungNama;
                ViewBag.PenanggungAlamat = reg.PenanggungAlamat;
                ViewBag.PenanggungHubungan = reg.PenanggungHubungan;
                ViewBag.Keterangan = reg.Keterangan;
                var d = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                ViewBag.DokterPengirim = d != null ? d.NamaDOkter : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("CreateHD")]
        public string CreateHD_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg && x.Nomor == item.nomor);
                                var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                checkDetailKomponen(item);

                                var userid = User.Identity.GetUserId();

                                var rj = s.Pelayanan_Insert_SIMtrRJ_HD(
                                    simtrregistrasi.RawatInap,
                                    item.noreg,
                                    item.nomor,
                                    simtrregistrasi.JenisKerjasamaID,
                                    item.tanggal,
                                    item.tanggal + item.jam,
                                    item.section,
                                    item.dokter,
                                    item.kelas,
                                    simtrregistrasi.NRM,
                                    item.adaobat,
                                    item.audit,
                                    userid,
                                    reg.SectionAsalID,
                                    item.dokterpengirim,
                                    item.jenisanatesi,
                                    item.cito,
                                    item.dokter,
                                    item.dokter,
                                    item.mcu,
                                    item.cob
                                ).FirstOrDefault();
                               
                                var nobukti = rj;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.tanggal + item.jam,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.tanggal + item.jam,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon,
                                        Keterangan = insertKeteranganInvoiceJasa(s, x.jasaid, simtrregistrasi.KdKelas, 1, x.dokter, item.noreg, item.nomor),
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }

                                if (item.asistenanastesi == null) item.asistenanastesi = new List<SelectListItemViewModel>();
                                foreach (var x in item.asistenanastesi)
                                {
                                    s.SIMtrOperasiAnastesi.Add(new SIMtrOperasiAnastesi() { NoBukti = nobukti, DokterAsistenAnas = x.Value });
                                }
                                if (item.instrumen == null) item.instrumen = new List<SelectListItemViewModel>();
                                foreach (var x in item.instrumen)
                                {
                                    s.SIMtrOperasiInstrumen.Add(new SIMtrOperasiInstrumen() { NoBUkti = nobukti, DokterInstrumenID = x.Value });
                                }

                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                simtrdataregpasien.SudahPeriksa = true;
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Create {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();

                                //insert auto BHP jasa
                                insetAutoBHPJasa(s, nobukti);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public ActionResult DetailHD(string nobukti)
        {
            var tl = Request.Cookies["LoginTipe"].Value.ToUpper();
            BillingInsertViewModel model;
            using (var s = new SIM_Entities())
            {
                ViewBag.ShowKomponen = checkOtorisasiShowKomponen(s);

                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.OK_DokterBedahID,
                    opetatorpelaksasa = m.OK_DokterBedahID2,
                    durante = m.OK_DokterDuranteID,
                    dokterasisten = m.OK_DOkterAssBedah,
                    dokterasisten2 = m.OK_DOkterAssBedah2,
                    dokteranak = m.OK_DokterANak,
                    dokteranastesi = m.OK_DokterAnasID,
                    cito = m.Cyto ?? false,
                    cob = (bool)(m.COB == null ? false : m.COB),
                    mcu = m.MCU == null ? false : m.MCU,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                    asistenanastesi = new List<SelectListItemViewModel>(),
                    instrumen = new List<SelectListItemViewModel>(),
                    onloop = new List<SelectListItemViewModel>()
                };
                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);
                ViewBag.PenanggungNama = m.OK_PenanggungNama;
                ViewBag.PenanggungAlamat = m.OK_PenanggungAlamat;
                ViewBag.PenanggungHubungan = m.OK_PenanggungHub;
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                ViewBag.JenisAnatesiName = jenisanatesi == null ? "" : jenisanatesi.Deskripsi;
                ViewBag.RuangOKName = ruangok == null ? "" : ruangok.RuangOK;
                var kategorioperasi = s.SIMmKategoriOperasi.FirstOrDefault(x => x.KategoriID == m.OK_KategoriOperasiID);
                ViewBag.KategoriOperasiName = kategorioperasi == null ? "" : kategorioperasi.KategoriName;
                var jenisluka = s.SIMmJenisLuka.FirstOrDefault(x => x.KodeLuka == m.JenisLukaID);
                ViewBag.JenisLukaName = jenisluka == null ? "" : jenisluka.Deskripsi;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;

                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Operator = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.opetatorpelaksasa);
                ViewBag.OperatorPelaksana = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.durante);
                ViewBag.Durante = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten);
                ViewBag.DokterAsisten = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten2);
                ViewBag.DokterAsisten2 = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranak);
                ViewBag.DokterAnak = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranastesi);
                ViewBag.Anastesi = dokter != null ? dokter.NamaDOkter : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("EditHD")]
        public ActionResult EditHD_Get(string nobukti)
        {
            var tl = Request.Cookies["LoginTipe"].Value.ToUpper();
            BillingInsertViewModel model;
            using (var s = new SIM_Entities())
            {
                ViewBag.ShowKomponen = checkOtorisasiShowKomponen(s);

                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                ViewBag.Noreg = m.RegNo;
                ViewBag.Section = m.SectionID;
                ViewBag.Nomor = m.Nomor;

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    nobukti = m.NoBukti,
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.OK_DokterBedahID,
                    opetatorpelaksasa = m.OK_DokterBedahID2,
                    durante = m.OK_DokterDuranteID,
                    dokterasisten = m.OK_DOkterAssBedah,
                    dokterasisten2 = m.OK_DOkterAssBedah2,
                    dokteranak = m.OK_DokterANak,
                    dokteranastesi = m.OK_DokterAnasID,
                    cito = m.Cyto ?? false,
                    spesialis = m.SpesialisasiID,
                    kelas = m.KdKelas,
                    cob = (bool)(m.COB == null ? false : m.COB),
                    mcu = m.MCU == null ? false : m.MCU,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                    asistenanastesi = new List<SelectListItemViewModel>(),
                    instrumen = new List<SelectListItemViewModel>(),
                    onloop = new List<SelectListItemViewModel>()
                };
                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                model.jenisanatesi = jenisanatesi.IDAnastesi;
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);
                ViewBag.PenanggungNama = m.OK_PenanggungNama;
                ViewBag.PenanggungAlamat = m.OK_PenanggungAlamat;
                ViewBag.PenanggungHubungan = m.OK_PenanggungHub;
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                ViewBag.JenisAnatesiName = jenisanatesi == null ? "" : jenisanatesi.Deskripsi;
                ViewBag.RuangOKName = ruangok == null ? "" : ruangok.RuangOK;
                var kategorioperasi = s.SIMmKategoriOperasi.FirstOrDefault(x => x.KategoriID == m.OK_KategoriOperasiID);
                ViewBag.KategoriOperasiName = kategorioperasi == null ? "" : kategorioperasi.KategoriName;
                var jenisluka = s.SIMmJenisLuka.FirstOrDefault(x => x.KodeLuka == m.JenisLukaID);
                ViewBag.JenisLukaName = jenisluka == null ? "" : jenisluka.Deskripsi;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;

                var detailid = 0;
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Operator = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.opetatorpelaksasa);
                ViewBag.OperatorPelaksana = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.durante);
                ViewBag.Durante = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten);
                ViewBag.DokterAsisten = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten2);
                ViewBag.DokterAsisten2 = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranak);
                ViewBag.DokterAnak = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranastesi);
                ViewBag.Anastesi = dokter != null ? dokter.NamaDOkter : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("EditHD")]
        public string EditHD_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Delete
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                if (simtrregistrasi.StatusBayar != "Belum") throw new Exception("Pasien sudah melakukan pembayaran");

                                if (simtrregistrasi.CloseAdmin == true) throw new Exception("Data Pasien sudah terclosing admin");

                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat diubah karena sudah di audit");

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg);

                                var nobukti = SIMtrRJ.NoBukti;
                                var userid = User.Identity.GetUserId();
                                #endregion

                                checkDetailKomponen(item);

                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }
                                var SIMtrOperasiAnastesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrOperasiAnastesi != null)
                                {
                                    s.SIMtrOperasiAnastesi.RemoveRange(SIMtrOperasiAnastesi);
                                }
                                var SIMtrOperasiInstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == item.nobukti);
                                if (SIMtrOperasiInstrumen != null)
                                {
                                    s.SIMtrOperasiInstrumen.RemoveRange(SIMtrOperasiInstrumen);
                                }
                                #endregion

                                SIMtrRJ.KdKelas = item.kelas;
                                SIMtrRJ.DokterPengirimID = item.dokterpengirim;
                                SIMtrRJ.OK_JenisAnastesi = item.jenisanatesi;
                                SIMtrRJ.Cyto = item.cito;
                                SIMtrRJ.OK_DokterBedahID = item.dokter;
                                SIMtrRJ.DokterID = item.dokter;
                                SIMtrRJ.SupplierPengirimID = reg.DokterID;
                                SIMtrRJ.JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID;
                                SIMtrRJ.Tanggal = item.tanggal;
                                SIMtrRJ.Jam = item.tanggal + item.jam;
                                SIMtrRJ.SectionID = item.section;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.AdaObat = item.adaobat;
                                SIMtrRJ.Audit = item.audit;
                                SIMtrRJ.UserIDWeb = userid;
                                SIMtrRJ.MCU = item.mcu;
                                SIMtrRJ.COB = item.cob;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.tanggal + item.jam,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.tanggal + item.jam,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon,
                                        Keterangan = insertKeteranganInvoiceJasa(s, x.jasaid, simtrregistrasi.KdKelas, 1, x.dokter, item.noreg, item.nomor),
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }

                                if (item.asistenanastesi == null) item.asistenanastesi = new List<SelectListItemViewModel>();
                                foreach (var x in item.asistenanastesi)
                                {
                                    s.SIMtrOperasiAnastesi.Add(new SIMtrOperasiAnastesi() { NoBukti = nobukti, DokterAsistenAnas = x.Value });
                                }
                                if (item.instrumen == null) item.instrumen = new List<SelectListItemViewModel>();
                                foreach (var x in item.instrumen)
                                {
                                    s.SIMtrOperasiInstrumen.Add(new SIMtrOperasiInstrumen() { NoBUkti = nobukti, DokterInstrumenID = x.Value });
                                }

                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                simtrdataregpasien.SudahPeriksa = true;
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Create {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();

                                //insert auto BHP jasa
                                insetAutoBHPJasa(s, nobukti);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string nobukti)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                    m.Batal = true;
                    result = new ResultSS(s.SaveChanges());
                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Billing batal {m.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var nomor = int.Parse(filter[9]);
                    IQueryable<Pelayanan_ListBilling> proses = s.Pelayanan_ListBilling.Where(x => x.Nomor == nomor);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(Pelayanan_ListBilling.NoReg)}=@0", filter[0]);
                    proses = proses.Where($"{nameof(Pelayanan_ListBilling.SectionID)}=@0", Request.Cookies["SectionIDPelayanan"].Value);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ListBillingViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TotalBiaya_View = ((decimal)(x.TotalBiaya ?? 0)).ToMoney();
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListJasa(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_LookupTarif_Result> proses = s.Pelayanan_LookupTarif(filter[10] == "True" ? "" : Request.Cookies["SectionIDPelayanan"].Value);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.JasaID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.JasaName)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.Kategori)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<Pelayanan_LookupTarif_Result>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string GetTarif(string jasa, string dokter, string section, string noreg, int? kategorioperasi, int? cito)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    var p = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    var c = s.SIMmSection.FirstOrDefault(x => x.SectionID == section);
                    var h = s.GetTarifBiaya_Global(jasa, dokter, cito, (kategorioperasi == null ? 1 : kategorioperasi), section, noreg, "1").FirstOrDefault();
                    var m = new {
                        jasa = jasa,
                        dokter = dokter,
                        cito = cito,
                        kategorioperasi = kategorioperasi,
                        section = section,
                        noreg = noreg
                    };
                    if (h == null) throw new Exception($"GetTarifBiaya_Global tidak ditemukan " +  m.ToString() );
                    var d = s.GetDetailKomponenTarif_Global(h.ListHargaID, h.CustomerKerjasamaID, h.TarifBaru, p.JenisKerjasamaID, "1", c.UnitBisnisID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Detail = d,
                        Tarif = h.Harga ?? 0
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== M U T A S I - P A S I E N

        [HttpGet]
        [ActionName("MutasiPasien")]
        public ActionResult MutasiPasien_Get(string noreg, int nomor)
        {
            MutasiPasienViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var dimutasi = s.SIMtrDataRegPasien.Where(x => x.Dimutasikan == true && x.Active == false && x.Out == true && x.NoReg == noreg && x.Nomor == nomor).FirstOrDefault(); 
                    if (dimutasi != null)
                    {
                        ViewBag.Dimutasikan = true;
                        ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
                        if (Request.IsAjaxRequest())
                            return PartialView();
                        else
                            return View();
                    }
                    var m = s.Pelayanan_DataRegPasienRI.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                    if (m == null) return HttpNotFound();
                    var n = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                    if (n == null) return HttpNotFound();
                    var section = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionID);
                    item = new MutasiPasienViewModel()
                    {
                        DariTipePasien = m.JenisKerjasama,
                        DariKelas = m.NamaKelas,
                        DariSection = section != null ? section.SectionName : "",
                        DariKamar = m.Kamar,
                        DariNoBed = m.NoBed,
                        TipePasienId = n.JenisPasienID ?? 0,
                        TipePasienName = m.JenisKerjasama,
                        SectionId = m.SectionID,
                        KelasAsalId = n.KelasAsalID,
                        KelasPelayananId = n.KdKelasPelayananSection,
                        Nomor = nomor,
                        NoReg = noreg
                    };
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("MutasiPasien")]
        public string MutasiPasien_Post()
        {
            try
            {
                var item = new MutasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        s.Pelayanan_MutasiRuangan(DateTime.Today, DateTime.Now, item.NoReg, item.SectionId, item.NoKamar, item.NoBed, item.KelasPelayananId, item.TipePasienId, item.TitipKelas, item.Nomor, item.KelasAsalId, item.RawatGabung);
                        result = new ResultSS(s.SaveChanges());
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Uptasi Pasien {item.NoReg} {item.Nomor}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { if (ex.InnerException != null) ex = ex.InnerException; return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListKamarDetail(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_Get_Kamar> proses = s.Pelayanan_Get_Kamar;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Pelayanan_Get_Kamar.NoKamar)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Pelayanan_Get_Kamar.NoBed)}.Contains(@0)", filter[1]);
                    if (filter[10] == "True")
                    {
                        proses = proses.Where($"{nameof(mKamarDetail.Status)}=@0 OR Status=@1", "AV", "FD");
                    }
                    else
                    {
                        proses = proses.Where($"{nameof(mKamarDetail.Status)}=@0", "AV");
                    }
                    proses = proses.Where($"{nameof(Pelayanan_Get_Kamar.SectionID)}=@0", filter[11]);
                    proses = proses.Where($"{nameof(Pelayanan_Get_Kamar.KelasID)}=@0", filter[12]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<KamarDetailViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion 

        #region ===== U P D A T E - K E L A S

        [HttpGet]
        [ActionName("UpdateKelas")]
        public ActionResult UpdateKelas_Get(string noreg, int nomor)
        {
            UpdateKelasViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                    if (m == null) return HttpNotFound();
                    item = new UpdateKelasViewModel()
                    {
                        KelasAsalID = m.KelasAsalID,
                        KelasPelayananID = m.KelasID,
                        TitipKelas = m.Titip ?? false
                    };
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("UpdateKelas")]
        public string UpdateKelas_Post()
        {
            try
            {
                var item = new UpdateKelasViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        s.Pelayanan_UpdateKelas(item.NoReg, item.Nomor, item.KelasPelayananID, item.KelasAsalID, item.TitipKelas);
                        result = new ResultSS(s.SaveChanges());
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Uptasi Pasien {item.NoReg} {item.Nomor}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { if (ex.InnerException != null) ex = ex.InnerException; return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        [ActionName("UpdateKelasRI")]
        public ActionResult UpdateKelasRI_Get(string noreg)
        {
            UpdateKelasViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    if (m == null) return HttpNotFound();
                    item = new UpdateKelasViewModel()
                    {
                        Reg_KdKelasPertanggungan = m.KdKelasPertanggungan,
                        Reg_KdKelasAsal = m.KdKelasAsal,
                        Reg_KdKelas = m.KdKelas,
                        TitipKelas = m.TitipKelas == null ? false : m.TitipKelas
                    };
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("UpdateKelasRI")]
        public string UpdateKelasRI_Post()
        {
            try
            {
                var item = new UpdateKelasViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var kelas = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                        if (kelas != null)
                        {
                            kelas.KdKelasPertanggungan = item.Reg_KdKelasPertanggungan;
                            kelas.KdKelasAsal = item.Reg_KdKelasAsal;
                            kelas.KdKelas = item.Reg_KdKelas;
                            kelas.TitipKelas = item.TitipKelas;
                        }
                        s.SaveChanges();
                        result = new ResultSS(1);
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Uptasi Pasien {item.NoReg} {item.Nomor}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { if (ex.InnerException != null) ex = ex.InnerException; return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region===== C U S T O M   F U N C T I O N

        public string checkInsertTask(string noreg)
        {
            using (var s = new SIM_Entities())
            {
                s.BPJS_Task_Insert(
                noreg,
                "TaskNo_4",
                DateTime.Now,
                DateTime.Now,
                DateTime.Now
                );
                s.SaveChanges();
            }
            return "";
        }

        public string insertKeteranganInvoiceJasa(SIM_Entities service, string jasaid, string kelasid, int kategoriid, string dokterid, string noreg, int nomor)
        {
            var keterangan = "";
            string kategori = "", kelas = "", dokter = "", kamar = "", kdkamar = "";
            kategoriid = kategoriid == null ? 1 : kategoriid;

            var simtrlistjasa = service.SIMmListJasa.FirstOrDefault(e => e.JasaID == jasaid);
            if (simtrlistjasa != null)
            {
                if(simtrlistjasa.WithDokter == true)
                {
                    if(dokterid == "XX")
                    {
                        throw new Exception("Jasa "+ simtrlistjasa.JasaName+" Harus dengan Dokter/Petugas.");
                    }
                }

                if (simtrlistjasa.Keterangan != null)
                {

                    var datareg = service.SIMtrDataRegPasien.FirstOrDefault(e => e.NoReg == noreg && e.Nomor == nomor);
                    if (datareg != null) kdkamar = datareg.Kamar;

                    var master_kamar = service.Vw_Kamar.FirstOrDefault(e => e.NoKamar == kdkamar);
                    if (master_kamar != null) kamar = "Kamar : " + master_kamar.NamaKamar;

                    var master_kelas = service.SIMmKelas.FirstOrDefault(e => e.KelasID == kelasid);
                    if (master_kelas != null) kelas = "Kelas : " + master_kelas.NamaKelas;

                    var master_kategori = service.SIMmKategoriOperasi.FirstOrDefault(e => e.KategoriID == kategoriid);
                    if (master_kategori != null) kategori = "Katrgori Operasi : " + master_kategori.KategoriName;

                    var master_dokter = service.mDokter.FirstOrDefault(e => e.DokterID == dokterid);
                    if (master_dokter != null) dokter = master_dokter.NamaDOkter;

                    if (simtrlistjasa.Keterangan == "Kategori Operasi dan Kelas")
                    {
                        keterangan = kategori + " " + kelas;
                    }
                    else if (simtrlistjasa.Keterangan == "Kategori Operasi Saja")
                    {
                        keterangan = kategori;
                    }
                    else if (simtrlistjasa.Keterangan == "Kelas dan Nama Dokter")
                    {
                        keterangan = kelas + " " + dokter;
                    }
                    else if (simtrlistjasa.Keterangan == "Kelas dan No Kamar")
                    {
                        keterangan = kelas + " " + kamar;
                    }
                    else if (simtrlistjasa.Keterangan == "Kelas Saja")
                    {
                        keterangan = kelas;
                    }
                    else if (simtrlistjasa.Keterangan == "Nama Dokter Saja")
                    {
                        keterangan = dokter;
                    }
                    else if (simtrlistjasa.Keterangan == "Tanpa Keterangan")
                    {
                        keterangan = "";
                    }
                }
            }
            return keterangan;
        }

        public BillingInsertViewModel autoShowJasa(BillingInsertViewModel model, SIM_Entities service, VW_DataPasienReg_Pelayanan m)
        {
            var section_id = Request.Cookies["SectionIDPelayanan"].Value;
            var autojasa = service.SIMmListJasaSection.Where(e => e.SectionID == section_id).ToList();

            model.detail = new List<BillingInsertDetailViewModel>();
            foreach (var e in autojasa)
            {
                var detailid = 0;
                var autojasa_section = service.SIMmListJasa.Where(u => u.JasaID == e.JasaID && u.AutoSystemRI == true && u.Aktif == true).ToList();
                foreach(var x in autojasa_section)
                {
                    var h = service.GetTarifBiaya_Global(x.JasaID, m.DokterID, 0, 1, m.SectionAsalID, m.NoReg, "1").FirstOrDefault();
                    var d = service.mDokter.FirstOrDefault(r => r.DokterID == m.DokterID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = 0,
                        jasaid = x.JasaID,
                        jasanama = x.JasaName,
                        qty = (int)1,
                        tarif = (decimal)h.Harga,
                        dokter = m.DokterID == null ? "" : m.DokterID,
                        namadokter = d != null ? d.NamaDOkter : "",
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;

                    var dd = service.GetDetailKomponenTarif_Global(h.ListHargaID, h.CustomerKerjasamaID, h.TarifBaru, m.JenisKerjasamaID, "1", "1").ToList();
                    foreach (var q in dd)
                    {
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            komponenid = q.KomponenBiayaID,
                            komponennama = q.KomponenName,
                            harga = (decimal)q.HargaBaru,
                            diskon = (decimal)0,
                            diskonnilai = 0,
                            jumlah = (decimal)q.HargaBaru
                        });
                    }

                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    model.detail.Add(detail);
                }

            }
            return model;
        }

        private bool checkOtorisasiShowKomponen(SIM_Entities sim)
        {
            var r = false;
            var userid = User.Identity.GetUserId();
            var check_user = sim.mUserOtorisasiKhusus.FirstOrDefault(e => e.KriteriaOtorisasi == "VIEW KOMPONEN TARIF PELAYANAN" && e.UserIDWeb == userid);
            if(check_user != null)
            {
                r = true;
            }
            return r;
        }

        private bool insetAutoBHPJasa(SIM_Entities s, string nobukti)
        {
            s.Pelayanan_SIMtrPOPAutoJasa_Insert(nobukti);
            s.SaveChanges();
            return true;
        }

        private bool checkDetailKomponen(BillingInsertViewModel item)
        {
            
            int error = 0;
            if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
            if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
            foreach (var x in item.detail)
            {
                decimal sumHarga = 0;
                if (x.detail == null) throw new Exception("Detail komponen masih kosong");

                foreach (var y in x.detail)
                {
                    sumHarga += y.harga;
                }
                error += (x.tarif == sumHarga) ? 0 : 1;
            }
            if (error > 0)
            {
                throw new Exception("Harga detail tidak sama dengan harga header");
            }
            return true;
        }

        [HttpGet]
        public string check_DataReg(string noreg, int number)
        {
            try
            {
                bool stts;
                using (var s = new SIM_Entities())
                {
                    var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == number);
                    if (m == null)
                    {
                        stts = false;
                    }
                    else
                    {
                        if(m.AuditData == true)
                        {
                            stts = true;
                        }
                        else
                        {
                            stts = false;
                        }
                    }
                    
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = stts,
                    Data = "",
                    Message = ""
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }


        [HttpGet]
        public string checkAuditData(string noreg, int number)
        {
            try
            {
                var pesan = "";
                using (var s = new SIM_Entities())
                {
                    pesan = s.CekAuditData(noreg, number).FirstOrDefault();
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = "",
                    Message = pesan
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string get_nilaiEstimasi(string noreg)
        {
            try
            {
                var nilai = "";
                using (var s = new SIM_Entities())
                {
                    var pesan = s.Pelayanan_DataRegPasienRI.FirstOrDefault(x => x.NoReg == noreg);
                    if(pesan != null)
                    {
                        nilai = pesan.EstimasiSisa.Value.ToMoney();
                    }
                    else
                    {
                        nilai = "0";
                    }
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = nilai,
                    Message = ""
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== R E P O R T

        public ActionResult Nota(string id)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Reports"), $"Rpt_NotaPelayanan.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_NotaPelayanan", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoBukti", id));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_NotaPelayanan;1"].SetDataSource(ds[i].Tables[0]);


                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion
    }
}