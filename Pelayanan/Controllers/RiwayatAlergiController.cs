﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class RiwayatAlergiController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index(string nrm, string statusbayar)
        {
            ViewBag.NRM = nrm;
            ViewBag.StatusBayar = statusbayar;
            return PartialView();
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string nrm)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new RiwayatAlergiViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {

                        var val = s.SIMtrRiwayatAlergi.Where(x => x.NRM == item.NRM && x.DeskripsiAlergi == item.DeskripsiAlergi).FirstOrDefault();
                        if(val != null)
                        {
                            throw new Exception("Deskripsi alergi tidak boleh sama");
                        }

                        item.TglInput = DateTime.Now;
                        var m = IConverter.Cast<SIMtrRiwayatAlergi>(item);
                        s.SIMtrRiwayatAlergi.Add(m);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"RiwayatAlergi Create {m.NRM} {m.DeskripsiAlergi}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
        
        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string nrm, string deskripsi)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrRiwayatAlergi.FirstOrDefault(x => x.NRM == nrm & x.DeskripsiAlergi == deskripsi);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    s.SIMtrRiwayatAlergi.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"RiwayatAlergi delete {m.NRM} {m.DeskripsiAlergi}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMtrRiwayatAlergi> proses = s.SIMtrRiwayatAlergi;
                    if (filter[0] != null)
                        proses = proses.Where($"{nameof(SIMtrRiwayatAlergi.NRM)}=@0", filter[0]);
                    proses = proses.Where($"{nameof(SIMtrRiwayatAlergi.DeskripsiAlergi)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<RiwayatAlergiViewModel>(x));
                    foreach (var x in m)
                    {
                        var _m = s.mBarang.FirstOrDefault(y => y.Barang_ID == x.BarangID);
                        if (_m != null) x.BarangNama = _m.Nama_Barang;
                        x.TglInput_View = x.TglInput.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}