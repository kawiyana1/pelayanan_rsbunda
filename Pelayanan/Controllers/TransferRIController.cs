﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    public class TransferRIController : Controller
    {
        #region ===== C R E A T E
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg, int nomor)
        {
            var model = new TransferRIViewModel();
            using (var s = new SIM_Entities())
            {
                var checks = s.Pelayanan_CheckMutasiPasienRI(noreg, nomor).FirstOrDefault();
                if (checks != null)
                {
                    model = IConverter.Cast<TransferRIViewModel>(checks);
                    model.StatusTransfer = true;
                }
                else
                {
                    var m = s.VW_Registrasi_WEBREGISTRASI.FirstOrDefault(x => x.NoReg == noreg);
                    if (m != null)
                    {
                        model = IConverter.Cast<TransferRIViewModel>(m);
                        model.SectionAsalID = Request.Cookies["SectionIDPelayanan"].Value;
                        model.StatusTransfer = false;
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            var item = new TransferRIViewModel();
            try
            {
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.VW_Registrasi_WEBREGISTRASI.FirstOrDefault(x => x.NoReg == item.NoReg);

                        var insert = s.Pelayanan_SIMtrDataRegPasien_Insert(
                            item.NoReg,
                            DateTime.Today,
                            DateTime.Now,
                            model.JenisKerjasamaID,
                            item.SectionAsalID,
                            item.SectionID,
                            item.KelasID,
                            item.KelasPelayanan,
                            item.Titip,
                            item.Dokter_ID,
                            1,
                            false,
                            item.NoKamar,
                            item.NoBed,
                            true,
                            DateTime.Now,
                            false,
                            "",
                            model.UmurThn,
                            model.UmurBln,
                            model.UmurHr,
                            item.RawatGabung,
                            false,
                            null,
                            true,
                            "",
                            item.NoSep
                            );

                        result = new ResultSS(1, insert);
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Mutasi Create {item.NoReg}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion


        #region ==== K A M A R
        [HttpPost]
        public string ListKamar(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var SectionBayi = ConfigurationManager.AppSettings["SectionBayi"];
                    IQueryable<Vw_Kamar> proses = s.Vw_Kamar;
                    string salID = filter[2];
                    if (salID != SectionBayi)
                    {
                        if (filter[5] == "1")
                        {
                            proses = s.Vw_Kamar.Where(x => (x.Status == "FD" || x.Status == "AV") && x.SalID == salID);
                        }
                        else
                        {
                            proses = s.Vw_Kamar.Where(x => (x.Status == "AV") && x.SalID == salID);
                        }
                        proses = proses.Where($"{nameof(Vw_Kamar.KelasID)}.Contains(@0)", filter[4]);
                    }
                    else
                    {
                        proses = s.Vw_Kamar.Where(x => x.SalID == salID);
                    }
                    proses = proses.Where($"{nameof(Vw_Kamar.NamaKamar)}.Contains(@0)", filter[10]);
                    proses = proses.Where($"{nameof(Vw_Kamar.NoKamar)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Vw_Kamar.NoBed)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[3]))
                    {
                        proses = proses.Where(x => x.NoLantai == (byte?)IFilter.F_int(filter[3]));
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<KamarViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion
    }
}