﻿using CrystalDecisions.CrystalReports.Engine;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pelayanan.Entities;
using Pelayanan.Entities.SIM;
using Pelayanan.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Controllers
{
    public class DietController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id, int shift)
        {
            DietViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_Informasi_RequestDiet.FirstOrDefault(x => x.NoBukti == id);
                    var d = s.Pelayanan_Informasi_RequestDiet_Detail.Where(x => x.NoBukti == id && x.Kode_Shift == shift).ToList();
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<DietViewModel>(m);
                    item.TglInput_View = m.TglInput.ToString("dd/MM/yyyy");
                    item.Detail_List = new ListDetail<DietDetailViewModel>();
                    foreach(var x in d){
                        var detail = IConverter.Cast<DietDetailViewModel>(x);
                        detail.Shift = x.Kode_Shift.ToString();
                        detail.IDDiet = x.IDDiet;
                        item.Detail_List.Add(false, detail);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new DietViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var model = s.FNB_trDietRequest.FirstOrDefault(x => x.No == item.NoBukti);
                                if (model == null) throw new Exception("Data Tidak ditemukan");

                                #region Validation
                                if (item.Detail_List == null)
                                    item.Detail_List = new ListDetail<DietDetailViewModel>();
                                item.Detail_List.RemoveAll(x => x.Remove);
                                if (item.Detail_List.Count == 0) throw new Exception("Detail Tidak Boleh Kosong");
                                #endregion

                                #region Detail
                                var kamar = "";
                                var _shifts = Int32.Parse(item.Detail_List[0].Model.Shift);
                                var getolddata = s.FNB_trDietRequestDetail.FirstOrDefault(x => x.No_DietRequest == item.NoBukti && x.Kode_Shift == _shifts);
                                if (getolddata != null)
                                {
                                    kamar = getolddata.NoKamar;
                                }

                                var new_list = item.Detail_List.Select(y => y.Model.IDDiet).ToList();
                                var real_list = s.FNB_trDietRequestDetail.Where(x => x.No_DietRequest == item.NoBukti).ToList();
                                // delete | delete where (real_list not_in new_list)
                                s.FNB_trDietRequestDetail.RemoveRange(real_list.Where(x => !new_list.Contains(x.No_DietRequest) && x.Kode_Shift == _shifts));
                                // add | add where (new_list not_in raal_list)
                                foreach (var x in item.Detail_List.Where(x => !real_list.Select(y => y.No_DietRequest).Contains(x.Model.IDDiet)).ToList())
                                {
                                    x.Model.NoBukti = item.NoBukti;
                                    var m = IConverter.Cast<FNB_trDietRequestDetail>(x.Model);
                                    var _shift = Int32.Parse(x.Model.Shift);
                                    var _x = s.FNB_mShift.FirstOrDefault(z => z.Kode == _shift);
                                    m.No_DietRequest = item.NoBukti;
                                    m.Kode_Makanan = x.Model.Kode_Makanan;
                                    m.Kode_Produk = x.Model.IDDiet;
                                    m.Qty = (int)x.Model.Jumlah;
                                    m.Keterangan = x.Model.Keterangan;
                                    m.Kode_Shift = _shift;
                                    m.NoKamar = kamar;
                                    s.FNB_trDietRequestDetail.Add(m);
                                }
                                //// edit | where (raal_list in new_list)
                                //foreach (var x in real_list.Where(x => new_list.Contains(x.No_DietRequest)).ToList())
                                //{
                                //    var n = item.Detail_List.FirstOrDefault(y => y.Model.IDDiet == x.No_DietRequest);
                                //    var _shift = Int32.Parse(n.Model.Shift);
                                //    var _x = s.FNB_mShift.FirstOrDefault(z => z.Kode == _shift);
                                //    x.Kode_Shift = _x.Kode;
                                //    x.Qty = (int)n.Model.Jumlah;
                                //    x.Keterangan = n.Model.Keterangan;
                                //}
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Service Edit {item.NoBukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                if (ex.InnerException != null) ex = ex.InnerException;
                                if (ex.InnerException != null) ex = ex.InnerException;
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string id, int shift)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.FNB_trDietRequestDetail.FirstOrDefault(x => x.No_DietRequest == id && x.Kode_Shift == shift);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    m.Batal = true;
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Diet delete {id}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id, int shift)
        {
            DietViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    //var m = s.FNB_trDietRequest.Where(x => x.NoBukti == id).FirstOrDefault();
                    //var m = s.Pelayanan_Informasi_DietReuqest.Where(x => x.NoBukti == id).FirstOrDefault();
                    var m = s.Pelayanan_Informasi_RequestDiet.Where(x => x.NoBukti == id).FirstOrDefault();
                    //var ss = s.Pelayanan_Informasi_RequestDiet.Where($"{nameof(Pelayanan_Informasi_RequestDiet.NoBukti)}.Contains(@0)", "1812DRQ000060").ToArray();
                    //var m = ss.FirstOrDefault();
                    var d = s.Pelayanan_Informasi_RequestDiet_Detail.Where(x => x.NoBukti == id && x.Kode_Shift == shift).ToList();
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<DietViewModel>(m);
                    item.TglInput_View = m.TglInput.ToString("dd/MM/yyyy");
                    item.Detail_List = new ListDetail<DietDetailViewModel>();
                    d.ForEach(x => item.Detail_List.Add(false, IConverter.Cast<DietDetailViewModel>(x)));
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<VW_FnB_RequestDietDetail> proses = s.VW_FnB_RequestDietDetail;
                    if (filter[10] != "True" && !string.IsNullOrEmpty(filter[11]) && !string.IsNullOrEmpty(filter[12]))
                    {
                        proses = proses.Where("TglInput >= @0", DateTime.Parse(filter[11]));
                        proses = proses.Where("TglInput <= @0", DateTime.Parse(filter[12]).AddDays(1));
                    }
                    proses = proses.Where($"{nameof(VW_FnB_RequestDietDetail.SectionID)}=@0", Request.Cookies["SectionIDPelayanan"].Value);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(VW_FnB_RequestDietDetail.NoBukti)}.Contains(@0)", filter[0]);
                    //proses = proses.Where($"{nameof(Pelayanan_Informasi_RequestDiet.Tanggal)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(VW_FnB_RequestDietDetail.NRM)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(VW_FnB_RequestDietDetail.NamaPasien)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(VW_FnB_RequestDietDetail.NoKamar)}.Contains(@0)", filter[4]);
                    if (filter[5] != "")
                    {
                        var shift = Int32.Parse(filter[5]); 
                        proses = proses.Where(x => x.KodeShift == shift);
                    }
                    if (filter[7] != "")
                    {
                        var status = (bool)(filter[7] == "1") ? true : false;
                        proses = proses.Where(x => x.Realisasi == status);
                    }
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(VW_FnB_RequestDietDetail.NamaPasien)}.Contains(@0)", filter[3]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<listDietDetailViewModel>(x));
                    foreach (var x in m) { 
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"); 
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListDetail(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmDaftarDiet> proses = s.SIMmDaftarDiet;
                    proses = proses.Where($"{nameof(DaftarDietViewModel.IDDiet)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(DaftarDietViewModel.NamaDiet)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(DaftarDietViewModel.Keterangan)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<DaftarDietViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}