﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class SensusHarianViewModel
    {
        public string SectionID { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string Tanggal_View2 { get; set; }
        public short UserID { get; set; }
        public int JmlBed { get; set; }
        public double BOR { get; set; }
        public Nullable<int> JmlBedTerisi { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }

        public ListDetail<SensusHarianDetailViewModel> DetailKamar_List { get; set; }
        public ListDetail<SensusHarianDetailViewModel> DetailPasienLama_List { get; set; }
        public ListDetail<SensusHarianDetailViewModel> DetailPasienBaru_List { get; set; }
        public ListDetail<SensusHarianDetailViewModel> DetailPasienKeluar_List { get; set; }
    }

    public class SensusHarianDetailViewModel
    {
        public string SectionID { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string NoReg { get; set; }
        public string NoKamar { get; set; }
        public string NoBed { get; set; }
        public string DokterRawatID { get; set; }
        public string SpesialisID { get; set; }
        public string StatusPasien { get; set; }
        public bool RawatBersama { get; set; }
        public bool OK { get; set; }
        public Nullable<bool> RawatBersamaBayi { get; set; }
        public string KelasID { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string KelasAsalID { get; set; }
        public Nullable<bool> TitipKelas { get; set; }
    }
}