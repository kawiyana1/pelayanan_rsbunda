﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class SKLViewModel
    {
        public string NoSKL { get; set; }
        [DataType(DataType.Date)]
        [Required]
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        [DataType(DataType.Time)]
        [Required]
        public System.DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public string NRM_Suami { get; set; }
        public string NamaSuami { get; set; }
        public short UmurSuami_Thn { get; set; }
        public byte UmurSuami_Bln { get; set; }
        public string Pekerjaan_Suami { get; set; }
        public string Pekerjaan_Istri { get; set; }
        public string Alamat_Istri { get; set; }
        public DateTime? TglReg_Istri { get; set; }
        public string TglReg_Istri_View { get; set; }
        [Required]
        public string NRM_Istri { get; set; }
        public string Nama_Istri { get; set; }
        public short UmurIstri_Thn { get; set; }
        public byte UmurIstri_Bln { get; set; }
        public string NamaAnak { get; set; }
        [DataType(DataType.Date)]
        [Required]
        public System.DateTime TglLahir { get; set; }
        public string TglLahir_View { get; set; }
        [Required]
        [DataType(DataType.Time)]
        public System.DateTime JamLahir { get; set; }
        public string JamLahir_View { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        public double Berat { get; set; }
        [Required]
        public double Panjang { get; set; }
        public string ALamat { get; set; }
        public Nullable<short> UserID { get; set; }
        [Required]
        public string NoReg { get; set; }
        public string DokterID { get; set; }
        public string Nrm { get; set; }
        public Nullable<System.DateTime> TglMasuk { get; set; }
        public string Keterangan { get; set; }
        public Nullable<int> AnakKe { get; set; }
        public string FileNamePicture { get; set; }
        public string DokterPenolong { get; set; }
        public string DokterPenolongNama { get; set; }
        public string Golda { get; set; }
        public string NoKamar { get; set; }
        public bool Hidup { get; set; }
        public bool LahirNormal { get; set; }
        public bool LahirSC { get; set; }
        public bool Prematur { get; set; }
        public string SectionID { get; set; }
        public string KdKelas { get; set; }
        public string KdKelasAsal { get; set; }
        public Nullable<bool> TitipKelas { get; set; }
        public string JenisPenolong { get; set; }
        public string NoRegAnak { get; set; }
        public bool Kembar { get; set; }
        public Nullable<int> KembarAnakKe { get; set; }
        public string NoIdentitas { get; set; }
        public string AlamatAyah { get; set; }
        public string Alamat_Suami { get; set; }
        public string No_Identitas_Suami { get; set; }
        public Nullable<int> WukuID { get; set; }
        public string WukuIDNama { get; set; }
        public Nullable<int> PancawaraID { get; set; }
        public string PancawaraIDNama { get; set; }

    }

    public class DataNoRegSKLViewModel
    {
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Pekerjaan { get; set; }
        public string JenisKelamin { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string TglLahir_View { get; set; }
        public string NoReg { get; set; }
        public System.DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public Nullable<int> UmurThn { get; set; }
        public Nullable<int> UmurBln { get; set; }
        public Nullable<bool> SKL { get; set; }
    }
}