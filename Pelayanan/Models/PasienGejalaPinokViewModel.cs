﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class PasienGejalaPinokViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public System.DateTime Jam { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string JenisKerjasama { get; set; }
        public string JenisKasus { get; set; }
        public string IndikasiKasus { get; set; }
        public string Keterangan { get; set; }
    }
}