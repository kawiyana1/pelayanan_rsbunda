﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class MemoViewModel
    {
        public string NOReg { get; set; }
        public string NoUrut { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public System.DateTime Jam { get; set; }
        public string SectionID { get; set; }
        public string Memo { get; set; }
        public short User_ID { get; set; }
    }
}