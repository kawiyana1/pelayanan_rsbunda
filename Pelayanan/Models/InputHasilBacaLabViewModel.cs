﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class InputHasilBacaLabViewModel
    {
        public string NoBukti { get; set; }
        public string NoReg { get; set; }
        public DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NRM { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public DateTime TglLahir { get; set; }
        public string TglLahir_View { get; set; }
        public string Umur { get; set; }
        public string JenisKelamin { get; set; }
        public bool Publish { get; set; }
        public bool Baru { get; set; }

        public List<InputHasilBacaLabDetailViewModel> Detail { get; set; }
    }

    public class InputHasilBacaLabDetailViewModel
    {
        public string Kategori { get; set; }
        public string TestID { get; set; }
        public string NamaTest { get; set; }
        public string HasilPemeriksaan { get; set; }
        public string Satuan { get; set; }
        public string NilaiRujukan { get; set; }
        public string Flag { get; set; }
        public string Keterangan { get; set; }
        public bool Tampilkan { get; set; }
    }
}