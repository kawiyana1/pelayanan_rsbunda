﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class ICDViewModel
    {
        public string KodeICD { get; set; }
        public string SectionID { get; set; }
        public string Descriptions { get; set; }
        public string DiagnosaHC { get; set; }
        public string IDGroupICD { get; set; }
        public string Kode_RL { get; set; }
        public string Gender { get; set; }
        public string Age_Range { get; set; }
        public Nullable<bool> Morfologi { get; set; }

        public string KDTdk { get; set; }
        public string Tindakan { get; set; }
    }
}