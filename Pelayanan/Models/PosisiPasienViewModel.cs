﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class PosisiPasienViewModel
    {
        public string NRM { get; set; }
        public string NoReg { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string TipePasien { get; set; }
        public string PosisiTerakhir { get; set; }
    }
}