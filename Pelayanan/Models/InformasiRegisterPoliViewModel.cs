﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class InformasiRegisterPoliViewModel
    {
        public string SectionName { get; set; }
        public string NoReg { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public System.DateTime Jam { get; set; }
        public System.DateTime TglReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Nama_Customer { get; set; }
        public string Alamat { get; set; }
        public bool SudahPeriksa { get; set; }
        public string DokterID { get; set; }
        public string Nama_Supplier { get; set; }
        public string NamaKelas { get; set; }
        public string NoKartu { get; set; }
        public string NoBill { get; set; }
        public bool Batal { get; set; }
    }
}