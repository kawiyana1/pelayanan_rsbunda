﻿using iHos.MVC.Property;
using Pelayanan.Entities.UserActivity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public static class UserActivity
    {
        public static void InsertUserActivity(UserActivityModel model)
        {
            try
            {
                using (var s = new UserActivityEntities())
                {
                    var result = s.aPelayanan.Add(new aPelayanan()
                    {
                        Id_User = model.Id_User ?? "Anonymous",
                        UserFullName = "",
                        InputDate = model.InputDate,
                        IP = model.IP,
                        OS = model.OS,
                        Browser = model.Browser,
                        Activity = model.Activity
                    });
                    var x = s.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex) { throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
    }
}