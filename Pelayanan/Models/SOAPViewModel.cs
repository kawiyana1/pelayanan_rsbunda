﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class SOAPViewModel
    {
        public int Nomor { get; set; }

        [Display(Name = "Tanggal")]
        public DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }

        [Display(Name = "Tanggal Input")]
        public DateTime TanggalInput { get; set; }
        public string TanggalInput_View { get; set; }

        [Display(Name = "Nama Section")]
        public string SectionName { get; set; }

        [Required]
        [Display(Name = "Dokter")]
        public string DokterID { get; set; }

        [Display(Name = "Dokter/Perawat")]
        public string SOAPDokterID { get; set; }

        [Display(Name = "Nama Dokter/Perawat")]
        public string NamaDOkter { get; set; }

        [Required]
        [Display(Name = "Subject")]
        public string S { get; set; }

        [Required]
        [Display(Name = "Objective")]
        public string O { get; set; }

        [Required]
        [Display(Name = "Assessment")]
        public string A { get; set; }

        [Required]
        [Display(Name = "Plan")]
        public string P { get; set; }

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
    }
}