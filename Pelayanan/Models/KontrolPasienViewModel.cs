﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class KontrolPasienViewModel
    {
        public string Tanggal_View { get; set; }
        public string SectionAsalID { get; set; }

        public string NoBukti { get; set; }
        public string NoReg { get; set; }
        public Nullable<System.DateTime> TglInput { get; set; }
        public Nullable<System.DateTime> JamInput { get; set; }
        public Nullable<System.DateTime> Kontrol_tgl { get; set; }
        public string SectionID { get; set; }
        public Nullable<int> WaktuID { get; set; }
        public string IndikasiKontrol { get; set; }
        public string NRM { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string JenisKelamin { get; set; }
        public string Umur { get; set; }
        public string DiagnosaAkhir { get; set; }
        public string Obat { get; set; }
        public string DokterID { get; set; }
        public string DokterID_Manual { get; set; }
        public Nullable<short> UserID { get; set; }
        public string DiinputOleh { get; set; }
        public string Keterangan { get; set; }
        public string Indikasi { get; set; }
        public string DokterPenanggungJawab { get; set; }
        public string KedatanganPasien { get; set; }
        public Nullable<System.DateTime> TglDatang { get; set; }
        public Nullable<System.DateTime> JamDatang { get; set; }
        public string NamaDOkter { get; set; }
        public string KeteranganWaktu { get; set; }
        public string SectionNama { get; set; }
        public bool JadwalManual { get; set; }
    }
}