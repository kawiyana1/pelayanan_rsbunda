﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class InputJamHasilViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Diambil { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Dikerjakan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Selesai { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Diserahkan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> TglRujukan { get; set; }
    }
}