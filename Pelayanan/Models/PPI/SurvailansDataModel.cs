﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models.PPI 
{
    public class SurvailansDataModel
    {
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string Tanggal_View2 { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string UserIDWeb { get; set; }
        public string UserName { get; set; }
        public string UserCreate { get; set; }
        public ListDetail<SurveilensDetailDataModel> Detail_List { get; set; }
        public List<SurveilensDetailDataModel> Detail_List2 { get; set; }
    }

    public class SurveilensDetailDataModel
    {
        public int Nomor { get; set; }
        public string SectionID { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Noreg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Jenis_Kelamin { get; set; }
        public string Umur { get; set; }
        public string PasienBaru { get; set; }
        public Nullable<int> Umur_Th { get; set; }
        public Nullable<int> Umur_Bln { get; set; }
        public Nullable<double> Suhu { get; set; }
        public string KodeICD { get; set; }
        public string KetICD { get; set; }
        public bool Tind_VEN { get; set; }
        public bool Tind_UC { get; set; }
        public bool Tind_IVL { get; set; }
        public bool Tind_CVL { get; set; }
        public bool Tind_ETT { get; set; }
        public bool Tind_TB { get; set; }
        public bool Infek_VAP { get; set; }
        public bool Infek_HAP { get; set; }
        public bool Infek_ISK { get; set; }
        public bool Infek_IADP { get; set; }
        public bool Infek_PLEB { get; set; }
        public bool Infek_DEK { get; set; }
        public string Kultur_Sample { get; set; }
        public string Kultur_Ket { get; set; }
        public string Antibiotik { get; set; }
        public List<SurveilensDetailDetailDataModel> AR { get; set; }
    }

    public class SurveilensDetailDetailDataModel 
    {
        public int Id { get; set; }
        public string Nama { get; set; }
        public string Dosis { get; set; }
    }
}