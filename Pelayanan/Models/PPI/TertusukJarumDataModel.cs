﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models.PPI
{
    public class TertusukJarumDataModel
    {
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string Tanggal_View2 { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string UserIDWeb { get; set; }
        public string UserName { get; set; }
        public string UserCreate { get; set; }
        public ListDetail<TertusukJarumDetailDataModel> Detail_List { get; set; }
    }

    public class TertusukJarumDetailDataModel
    {
        public int Nomor { get; set; }
        public string SectionID { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string KodePetugas { get; set; }
        public string NamaPetugas { get; set; }
        public string TempatTugas { get; set; }
        public string TempatKejadian { get; set; }
        public string Kronologi { get; set; }
        public string PenangananDengan { get; set; }
        public string Keterangan { get; set; }
    }
}