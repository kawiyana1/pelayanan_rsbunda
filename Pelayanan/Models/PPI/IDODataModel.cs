﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models.PPI
{
    public class IDODataModel
    {
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string Tanggal_View2 { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string UserIDWeb { get; set; }
        public string UserName { get; set; }
        public string UserCreate { get; set; }
        public ListDetail<IDODetailDataModel> Detail_List { get; set; }
        public List<IDODetailDataModel> Detail_List2 { get; set; }
    }

    public class IDODetailDataModel
    {
        public int Nomor { get; set; }
        public string SectionID { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Noreg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string DrBedahID { get; set; }
        public string DrBedahNama { get; set; }
        public string KodeICD { get; set; }
        public string KetICD { get; set; }
        public Nullable<System.DateTime> TglOp { get; set; }
        public Nullable<System.DateTime> TglInfeksi { get; set; }
        public string TipeOperasi { get; set; }
        public string JenisOperasi { get; set; }
        public Nullable<int> ScoreAsa { get; set; }
        public string DurasiOperasi { get; set; }
        public Nullable<int> Total { get; set; }
        public string Kultur_Sample { get; set; }
        public string Kultur_Ket { get; set; }
        public string Antibiotik_Prof { get; set; }
        public string Antibiotik_EMP { get; set; }
        public string Antibiotik_DEF { get; set; }

        public List<IDODetailAntibiotikDataModel> Prof { get; set; }
        public List<IDODetailAntibiotikDataModel> EMP { get; set; }
        public List<IDODetailAntibiotikDataModel> DEF { get; set; }
    }

    public class IDODetailAntibiotikDataModel 
    {
        public int Id { get; set; }
        public string Dosis { get; set; }
        public string Nama { get; set; }
    }
}