﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class HasilLaboratoriumViewModel
    {
        public string NoSystem { get; set; }
        public string RegNo { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string DokterPengirim { get; set; }
        public string SectionName { get; set; }
        public string KategoriTestNama { get; set; }
        public string NamaTest { get; set; }
        public string Nilai { get; set; }
        public string Satuan { get; set; }
        public string NilaiRujukan { get; set; }
        public string HasilTidakNormal_Flag { get; set; }
        public string Analis { get; set; }
        public string PenanggungJawab { get; set; }

        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string JenisPasien { get; set; }
    }
}