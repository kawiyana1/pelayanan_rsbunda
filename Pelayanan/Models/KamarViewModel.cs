﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class KamarViewModel
    {
        public string Kelas { get; set; }
        public string NoBed { get; set; }
        public string NoKamar { get; set; }
        public string SalID { get; set; }
        public byte JmlBed { get; set; }
        public byte NoLantai { get; set; }
        public bool Tambahan { get; set; }
        public string KelasID { get; set; }
        public string Status { get; set; }
        public string NamaKamar { get; set; }
    }
}