﻿using Pelayanan.Entities.SIM;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;

namespace Pelayanan.Models
{
    public static class StaticModel
    {
        public static string DbEntityValidationExceptionToString(DbEntityValidationException ex)
        {
            var msgs = new List<string>();
            foreach (var eve in ex.EntityValidationErrors)
            {
                var msg = $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                foreach (var ve in eve.ValidationErrors) { msg += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\""; }
                msgs.Add(msg);
            }
            return string.Join("\n", msgs.ToArray());
        }

        public static List<SelectListItem> SelectListJenisKerjasama
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIM_Entities())
                {
                    result = s.SIMmJenisKerjasama.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.JenisKerjasama,
                        Value = x.JenisKerjasamaID.ToString(),
                        Selected = x.JenisKerjasamaID == 3
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListKlp
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIM_Entities())
                {
                    result = s.mKlp.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Klp,
                        Value = x.Klp,
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListKelasPertanggungan
        {
            get
            {
                var result = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    result = s.SIMmKelas.Where(x => x.Active == true).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaKelas,
                        Value = x.KelasID,
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> ListShift
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.SIMmShift.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Deskripsi,
                        Value = x.IDShift.ToString()
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListShiftDeskripsi
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.FNB_mShift.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Kode.ToString()
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListProduk
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.FNB_mProduk.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Kode.ToString()
                    });
                }
                return r;
            }
        }


        public static List<SelectListItem> ListDaftarDiet
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.FNB_mProduk.Where(x => x.Kode_Kategori == 5).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Kode
                    });
                }
                r.Insert(0, new SelectListItem());
                return r;
            }
        }

        public static List<SelectListItem> ListDaftarMakanan
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.FNB_mProduk.Where(x => x.Kode_Kategori == 1).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Kode
                    });
                    r.Insert(0, new SelectListItem());
                }
                return r;
            }
        }

        public static List<SelectListItem> ListTemplateRadiologi
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.Database.SqlQuery<SelectListItem>("SELECT TemplateName AS Text, TemplateName AS Value FROM Pelayanan_TemplateBacaRad").ToList();
                }
                return r;
            }
        }

        public static List<SelectListItem> ListJenisPenolong
        {
            get
            {
                var r = new List<SelectListItem>()
                {
                    new SelectListItem() { Text ="NONE", Value = "NONE" },
                    new SelectListItem() { Text ="DOD", Value = "DOD" },
                    new SelectListItem() { Text ="BIDAN", Value = "BIDAN" },
                };
                return r;
            }
        }

        public static List<SelectListItem> ListBaikRusak
        {
            get
            {
                var r = new List<SelectListItem>()
                {
                    new SelectListItem() { Text ="BAIK", Value = "BAIK" },
                    new SelectListItem() { Text ="RUSAK", Value = "RUSAK" },
                };
                return r;
            }
        }

        public static List<SelectListItem> ListJenisLensa(string defaultvalue = "")
        {
            var r = new List<SelectListItem>()
            {
                new SelectListItem() { Text ="Dekat", Value = "Dekat" , Selected = defaultvalue == "Dekat" },
                new SelectListItem() { Text ="Jauh", Value = "Jauh" , Selected = defaultvalue == "Jauh" },
                new SelectListItem() { Text ="Bifokal", Value = "Bifokal" , Selected = defaultvalue == "Bifokal" },
                new SelectListItem() { Text ="Progresif", Value = "Progresif" , Selected = defaultvalue == "Progresif" },
            };
            return r;
        }

        public static List<SelectListItem> ListStatusKamar
        {
            get
            {
                var r = new List<SelectListItem>()
                {
                    //new SelectListItem(){ Text = "Avalaible", Value = "AV"},
                    //new SelectListItem(){ Text = "Booking", Value = "BO"},
                    new SelectListItem(){ Text = "Cleaning", Value = "CL"},
                    //new SelectListItem(){ Text = "Check Out", Value = "CO"},
                    //new SelectListItem(){ Text = "Filled", Value = "FD"},
                    new SelectListItem(){ Text = "Renovation", Value = "RV"},
                    new SelectListItem(){ Text = "Vacant Dirty", Value = "VD"},
                };
                return r;
            }
        }

        public static List<SelectListItem> ListTipePelayanan
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                }
                return r;
            }
        }


        public static List<SelectListItem> ListSectionMutasiRI
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.SIMmSection.Where(x => x.RIOnly == true && x.StatusAktif == true).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.SectionName,
                        Value = x.SectionID
                    });
                }
                return r;
            }
        }
        public static List<SelectListItem> ListJenisAnatesi
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.mJenisAnestesi.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Deskripsi,
                        Value = x.IDAnastesi
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListTipeDiagnosa
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.mTipeDiagnosa.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.TipeDiagnosa,
                        Value = x.TipeDiagnosa
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSpesialisasi
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.SIMmSpesialisasi.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.SpesialisName,
                        Value = x.SpesialisID
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListKategoriOperasi
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.SIMmKategoriOperasi.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.KategoriName,
                        Value = x.KategoriID.ToString()
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListRuangOK
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.SIMmRuangOK.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.RuangOK,
                        Value = x.RuangOKID
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListJenisLuka
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.SIMmJenisLuka.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Deskripsi,
                        Value = x.KodeLuka
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> paramReportSection
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.SIMmSection.Where(e => e.TipePelayanan == "RI" || e.TipePelayanan == "RJ" || e.TipePelayanan == "PENUNJANG" || e.TipePelayanan == "PENUNJANG2").ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.SectionName,
                        Value = x.SectionID
                    });
                }
                return r;
            }
        }

        public static SIMtrDataRegPasien ResumeMedis(string noreg, int nomor)
        {
            SIMtrDataRegPasien r;
            using (var s = new SIM_Entities())
            {
                r = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
            }
            return r;
        }

        public static SIMtrResumeMedis Emergency(string noreg, string section)
        {
            SIMtrResumeMedis r;
            using (var s = new SIM_Entities())
            {
                r = s.SIMtrResumeMedis.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
            }
            return r;
        }

        public static List<SIMtrDiagnosa> DiagnosaAwal(string noreg, string section)
        {
            List<SIMtrDiagnosa> r;
            using (var s = new SIM_Entities())
            {
                r = s.SIMtrDiagnosa.Where(x => x.NoReg == noreg && x.SectionID == section).ToList();
            }
            return r;
        }

        public static SelectListItem FindDiagnosa(string id)
        {
            SelectListItem h;
            using (var s = new SIM_Entities())
            {
                var m = s.mICD.FirstOrDefault(x => x.KodeICD == id);
                if (m == null)
                {
                    h = new SelectListItem();
                }
                else
                {
                    h = new SelectListItem()
                    {
                        Value = m.KodeICD,
                        Text = m.Descriptions
                    };
                }
            }
            return h;
        }

        public static List<SelectListItem> ListJenisKasus
        {
            get
            {
                var r = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "Pasang Kateter", Value = "Pasang Kateter"},
                    new SelectListItem(){ Text = "Pasien Operasi", Value = "Pasien Operasi"},
                    new SelectListItem(){ Text = "Pasien Tirah Baring", Value = "Pasien Tirah Baring"},
                    new SelectListItem(){ Text = "Pasang Infus", Value = "Pasang Infus"},
                };
                return r;
            }
        }

        public static SelectListItem FindDokter(string id)
        {
            SelectListItem h;
            using (var s = new SIM_Entities())
            {
                var m = s.mDokter.FirstOrDefault(x => x.DokterID == id);
                h = new SelectListItem()
                {
                    Value = m.DokterID,
                    Text = m.NamaDOkter
                };
            }
            return h;
        }

        public static List<SelectListItem> ListFilterSection
        {
            get
            {
                var r = new List<SelectListItem>();
                r.Add(new SelectListItem() { Text = "All", Value = "" });
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                    var m = s.SIMmSection.ToList();
                    foreach (var group in h)
                    {
                        var optionGroup = new SelectListGroup() { Name = group.Text };
                        foreach (var item in m.Where(x => x.TipePelayanan == group.Value).ToList())
                        {
                            r.Add(new SelectListItem()
                            {
                                Value = item.SectionName,
                                Text = item.SectionName,
                                Group = optionGroup
                            });
                        }
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSection
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                    var m = s.SIMmSection.Where(x => x.StatusAktif == true).ToList();
                    foreach (var group in h)
                    {
                        var optionGroup = new SelectListGroup() { Name = group.Text };
                        foreach (var item in m.Where(x => x.TipePelayanan == group.Value).ToList())
                        {
                            r.Add(new SelectListItem()
                            {
                                Value = item.SectionID.ToString(),
                                Text = item.SectionName,
                                Group = optionGroup
                            });
                        }
                    }
                }
                return r;
            }
        }

        public static string ListSectionCustom
        {
            get
            {
                var r = "";
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                    var m = s.SIMmSection.Where(x => x.StatusAktif == true).ToList();
                    foreach (var group in h)
                    {
                        var optionGroup = new SelectListGroup() { Name = group.Text };
                        r += $"<optgroup label=\"{group.Text}\">";
                        foreach (var item in m.Where(x => x.TipePelayanan == group.Value).ToList())
                        {
                            r += $"<option value=\"{item.SectionID}\">{item.SectionName}</option>";
                        }
                        r += $"</optgroup>";
                    }
                }
                return r;
            }
        }



        public static List<SelectListItem> ListSectionAsal
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                    var m = s.SIMmSection.Where(x => x.StatusAktif == true).ToList();
                    foreach (var group in h)
                    {
                        var optionGroup = new SelectListGroup() { Name = group.Text };
                        foreach (var item in m.Where(x => x.TipePelayanan == group.Value).ToList())
                        {
                            r.Add(new SelectListItem()
                            {
                                Value = item.SectionID.ToString(),
                                Text = item.SectionName,
                                Group = optionGroup
                            });
                        }
                    }
                    r.Insert(0, new SelectListItem()
                    {
                        Value = "0",
                        Text = "ALL",
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionJadwal
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "RJ" && x.StatusAktif == true && x.SectionID != "SEC002").ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListWaktuPraktek
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmWaktuPraktek.ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.WaktuID.ToString(),
                            Text = item.Keterangan
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionStok
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "FARMASI" || x.TipePelayanan == "GUDANG").ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListJenisKelompokObat
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmKelompokJenisObat.ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.KelompokJenis,
                            Text = item.KelompokJenis
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionPenunjang
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmSection.Where(x => (x.TipePelayanan == "PENUNJANG" || x.TipePelayanan == "PENUNJANG2") && x.StatusAktif == true).ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionRI
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmSection.Where(x => x.RIOnly == true).ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListKelas
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmKelas.Where(x => x.Active == true).ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.KelasID,
                            Text = item.NamaKelas
                        });
                    }
                    r.Insert(0, new SelectListItem()
                    {
                        Value = "",
                        Text = "",
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListAlasanDirujuk
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmAlasanDirujuk.ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.AlasanDirujukID,
                            Text = item.AlasanDirujuk
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSatuanPuyer
        {
            get
            {
                var r = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "KAPSUL", Value = "KAPSUL"},
                    new SelectListItem(){ Text = "PUYER", Value = "PUYER"},
                    new SelectListItem(){ Text = "CREAME", Value = "CREAME"},
                };
                return r;
            }
        }

        public static List<SelectListItem> ListSectionFarmasi
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    //var m = s.SIMmSection.Where(x => x.TipePelayanan == "FARMASI" & x.NoIP == GetLocalIPAddress).ToList();
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "FARMASI").ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static string GetLocalIPAddress
        {
            get
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip.ToString();
                    }
                }
                throw new Exception("No network adapters with an IPv4 address in the system!");
            }
        }

        public static List<SelectListItem> ListStatusPasienBaru
        {
            get
            {
                var r = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "", Value = ""},
                    new SelectListItem(){ Text = "PASIEN MASUK", Value = "PASIEN MASUK"},
                    new SelectListItem(){ Text = "PASIEN PINDAHAN", Value = "PASIEN PINDAHAN"},
                };
                return r;
            }
        }

        public static List<SelectListItem> ListStatusPasienKeluar
        {
            get
            {
                var r = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "", Value = ""},
                    new SelectListItem(){ Text = "PASIEN DIIZINKAN PULANG", Value = "PASIEN DIIZINKAN PULANG"},
                    new SelectListItem(){ Text = "PASIEN DIRUJUK", Value = "PASIEN DIRUJUK"},
                    new SelectListItem(){ Text = "PINDAH KE RS LAIN", Value = "PINDAH KE RS LAIN"},
                    new SelectListItem(){ Text = "PULANG APS", Value = "PULANG APS"},
                    new SelectListItem(){ Text = "PASIEN DI PINDAKAHKAN", Value = "PASIEN DI PINDAKAHKAN"},
                    new SelectListItem(){ Text = "MATI KURANG DARI 48 JAM", Value = "MATI KURANG DARI 48 JAM"},
                    new SelectListItem(){ Text = "MATI LEBIH DARI 48 JAM", Value = "MATI LEBIH DARI 48 JAM"},
                };
                return r;
            }
        }

        public static List<SelectListItem> ListKeteranganFilm
        {
            get
            {
                var r = new List<SelectListItem>()
                {
                    new SelectListItem() { Text ="BAIK", Value = "BAIK" },
                    new SelectListItem() { Text ="RUSAK", Value = "RUSAK" },
                };
                return r;
            }
        }
    }
}