﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class BillingViewModel
    {
    }

    public class ListBillingViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string NamaLengkap { get; set; }
        public string Tanggal_View { get; set; }
        public string DokterID { get; set; }
        public string Nama_Supplier { get; set; }
        public Nullable<double> TotalBiaya { get; set; }
        public string TotalBiaya_View { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public bool ProsesPayment { get; set; }
    }
}