﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class MutasiPasienViewModel
    {
        public string DariTipePasien { get; set; }
        public string DariSection { get; set; }
        public string DariKelas { get; set; }
        public string DariKamar { get; set; }
        public string DariNoBed { get; set; }

        public int TipePasienId { get; set; }
        public string TipePasienName { get; set; }
        public string SectionId { get; set; }
        public string SectionName { get; set; }
        public string KelasAsalId { get; set; }
        public string KelasAsalName { get; set; }
        public string KelasPelayananId { get; set; }
        public string KelasPelayananName { get; set; }
        [Required]
        public string NoKamar { get; set; }
        [Required]
        public string NoBed { get; set; }
        public int Nomor { get; set; }
        public string NoReg { get; set; }
        public bool TitipKelas { get; set; }
        public bool RawatGabung { get; set; }
    }
}