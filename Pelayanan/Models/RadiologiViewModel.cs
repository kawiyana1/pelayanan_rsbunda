﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class RadiologiViewModel
    {
    }

    public class InputHasilBacaRadiologiViewModel
    {
        public string NoBukti { get; set; }
        [Required]
        public string DokterId { get; set; }
        public string DokterNama { get; set; }
        public string KepadaYth { get; set; }
        public string No { get; set; }
        public string KeteranganKlinis { get; set; }
        public string KotakInputBesar { get; set; }
        public bool TemplateBaru { get; set; }
        public string TemplateName { get; set; }
        public string Template { get; set; }
    }
}