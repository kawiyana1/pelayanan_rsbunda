﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class SplitBillViewModel
    {
        public RegistrasiPasienViewModel Reg { get; set; }
    }

    public class getdetailSplitBilling
    {
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string SectionName { get; set; }
        public string NoReg { get; set; }
        public string NoBukti { get; set; }
        public Nullable<double> Total { get; set; }
        public string Total_View { get; set; }
        public Nullable<bool> SplitBill { get; set; }
    }


    public class insertSplitBilling{
        public string nobukti { get; set; }
        public bool mcu { get; set; }
    }
}