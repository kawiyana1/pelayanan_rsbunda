﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class IurBayarViewModel
    {
        public string NoReg { get; set; }

        public string SectionID { get; set; }

        public string KelasID { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglMasuk { get; set; }
        [DataType(DataType.Date)]

        public Nullable<System.DateTime> TglKeluar { get; set; }
        [DataType(DataType.Time)]

        public Nullable<System.DateTime> JamKeluar { get; set; }

        public Nullable<int> JumlahHariRawat { get; set; }

        public string DokterID { get; set; }

        public string KodeDiagnosaUtama { get; set; }
        public string NamaDiagnosaUtama { get; set; }

        public string KodeDiagnosaSkunder { get; set; }
        public string NamaDiagnosaSkunder { get; set; }

        public string KodeTindakan { get; set; }
        public string NamaKodeTindakan { get; set; }

        public Nullable<int> LamaRawat { get; set; }

        public Nullable<decimal> NilaiINACBGs { get; set; }
        public string NilaiINACBGs_View { get; set; }

        public Nullable<decimal> IURBayar { get; set; }
        public string IURBayar_View { get; set; }

        public string NamaPasien { get; set; }

        public Nullable<System.DateTime> TglLahir { get; set; }

        public string Agama { get; set; }

        public string NationalityID { get; set; }

        public string WargaNegara { get; set; }

        public string Pekerjaan { get; set; }

        public string Alamat { get; set; }

        public string Penanggung { get; set; }

        public string NRM { get; set; }

        public string SectionName { get; set; }

        public string NamaKelas { get; set; }
        public string NamaDokter { get; set; }
        public string JenisKelamin { get; set; }

        public Nullable<double> Presentase { get; set; }
    }
}