﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class FilmRadiologiDetailViewModel
    {
        public Nullable<long> ID { get; set; }
        public string Nobukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string KodeFilm { get; set; }
        public string NamaFilm { get; set; }
        public string NamaFilmNama { get; set; }
        public Nullable<int> Jumlah { get; set; }
        public string KeteranganFilm { get; set; }
        public string UserName { get; set; }
        public Nullable<bool> Batal { get; set; }
    }
}