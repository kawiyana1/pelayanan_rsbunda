﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class RiwayatAlergiViewModel
    {
        public string NRM { get; set; }
        public string DeskripsiAlergi { get; set; }
        public System.DateTime TglInput { get; set; }
        public string TglInput_View { get; set; }
        public Nullable<int> BarangID { get; set; }
        public string BarangNama { get; set; }
    }
}