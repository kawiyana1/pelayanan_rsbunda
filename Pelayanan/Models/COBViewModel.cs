﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class COBViewModel
    {
        public bool AnggotaBaru { get; set; }
        public Nullable<bool> PertanggunganKeduaIKS { get; set; }
        public string PertanggunganKeduaCompanyID { get; set; }
        public Nullable<int> PertanggunganKeduaCustomerKerjasamaID { get; set; }
        public string PertanggunganKeduaNoKartu { get; set; }
        public string NamaPerusahaan { get; set; }
        public string NamaAnggota { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string NoSJP { get; set; }
        public string KelasID { get; set; }
    }
}