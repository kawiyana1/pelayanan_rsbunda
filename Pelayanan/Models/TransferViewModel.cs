﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class TransferViewModel
    {
        public string NoReg { get; set; }
        public string SectionId { get; set; }

        public ListDetail<TransferDetailViewModel> Detail_List { get; set; }
    }

    public class TransferDetailViewModel
    {
        public string SectionTujuanId { get; set; }
        public string SectionTujuanName { get; set; }
        public string DokterId { get; set; }
        public string DokterIdSelect2 { get; set; }
        public string NamaDokter { get; set; }
        public int WaktuId { get; set; }
        public string WaktuNama { get; set; }
        public int NoUrut { get; set; }
        public string RiOnly { get; set; }
    }
}