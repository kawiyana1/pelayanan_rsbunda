﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class FIlmRadiologiViewDetail
    {
        public string Id { get; set; }
        public string NoBukti { get; set; }
        public string UserId { get; set; }

        public ListDetail<FilmRadiologiDetailViewModel> Detail_List { get; set; }
    }

}