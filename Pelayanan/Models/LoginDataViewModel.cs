﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class LoginDataViewModel
    {
        public string TipePelayanan { get; set; }
        public string TipeNamaPelayanan { get; set; }
        public string LoginTipe { get; set; }
    }
}