﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class GejalaPinokViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public System.DateTime Jam { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string JenisKasus { get; set; }
        public string IndikasiKasus { get; set; }
        public bool PositifPinok { get; set; }
        public string Keterangan { get; set; }
        public short UserID { get; set; }
        public int Nomor { get; set; }
    }
}