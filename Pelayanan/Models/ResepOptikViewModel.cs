﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class ResepOptikViewModel
    {
        public string No_Bukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string NoTelp { get; set; }
        public string JenisKelamin { get; set; }
        public Nullable<int> UmurTahun { get; set; }
        public Nullable<int> UmurBulan { get; set; }
        public Nullable<int> UmurHari { get; set; }
        public Nullable<int> JenisKerjasamaID { get; set; }
        public string DokterID { get; set; }
        public string NamaDokter { get; set; }
        public string OD_Spheris { get; set; }
        public string OD_Cylinder { get; set; }
        public string OD_Prisma { get; set; }
        public string OD_Axis { get; set; }
        public string OS_Spheris { get; set; }
        public string OS_Cylinder { get; set; }
        public string OS_Prisma { get; set; }
        public string OS_Axis { get; set; }
        public string JenisLensa { get; set; }
        public string Addition { get; set; }
        public string PupilDistance { get; set; }
        public string WarnaKaca { get; set; }
        public string Untuk { get; set; }
        public Nullable<bool> Batal { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }
        public Nullable<int> User_ID { get; set; }
        public bool Realisasi { get; set; }
        public string SectionID { get; set; }
    }
}