﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class EMRHistoriPengkajianAwalViewModel
    {
        public int No { get; set; }
        public string NRM { get; set; }
        public System.DateTime TanggalUpdate { get; set; }
        public string TanggalUpdate_View { get; set; }
        public System.DateTime JamUpdate { get; set; }
        public string JamUpdate_View { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
        public string SectionName { get; set; }
        public Nullable<int> Range { get; set; }
        public string KodeUrl { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
    }
}