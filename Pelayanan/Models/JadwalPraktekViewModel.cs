﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class JadwalPraktekViewModel
    {
        public string DokterID { get; set; }
        public string SectionID { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string Tanggal_Input { get; set; }
        public int WaktuID { get; set; }
        public string Hari { get; set; }
        public System.DateTime FromJam { get; set; }
        public System.DateTime ToJam { get; set; }
        public int JmlAntrian { get; set; }
        public bool Realisasi { get; set; }
        public Nullable<bool> Cancel { get; set; }
        public string DokterPenggantiID { get; set; }
        public Nullable<int> NoAntrianTerakhir { get; set; }
        public string NoRuang { get; set; }
        public Nullable<int> QuotaAntrian { get; set; }
        public Nullable<int> QuotaAntrianBPJS { get; set; }
        public Nullable<int> NoAntrianSaatIni { get; set; }
        public string StatusPraktek { get; set; }
        public string KeteranganStatusPraktek { get; set; }
        public Nullable<System.DateTime> PublishReservasiFromJam { get; set; }
        public Nullable<System.DateTime> PublishReservasiToJam { get; set; }
        public Nullable<System.DateTime> PublishRegistrasiFromJam { get; set; }
        public Nullable<System.DateTime> PublishRegistrasiToJam { get; set; }
        public Nullable<bool> Verified { get; set; }
        public Nullable<bool> TerimaBPJS { get; set; }
        public Nullable<bool> Pagi { get; set; }
        public Nullable<int> BPJSNoAntrianMinimal { get; set; }
        public Nullable<int> NoAntrianMaxSudahDiperiksa { get; set; }
        public string NamaDOkter { get; set; }
        public string Keterangan { get; set; }
        public string NamaSection { get; set; }
    }
}