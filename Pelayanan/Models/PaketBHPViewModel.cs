﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class PaketBHPViewModel
    {
        public string Kode { get; set; }
        public string NamaPaket { get; set; }
        public string SectionID { get; set; }
        public string Ditagihkan { get; set; }
    }
}