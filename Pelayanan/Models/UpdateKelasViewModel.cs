﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class UpdateKelasViewModel
    {
        public string NoReg { get; set; }
        public int Nomor { get; set; }
        public string KelasAsalID { get; set; }
        public string KelasPelayananID { get; set; }
        public bool TitipKelas { get; set; }

        public string Reg_KdKelasPertanggungan { get; set; }
        public string Reg_KdKelasAsal { get; set; }
        public string Reg_KdKelas { get; set; }
    }

    public class KamarDetailViewModel
    {
        public string NoKamar { get; set; }
        public string NoBed { get; set; }
        public string Status { get; set; }
        public Nullable<bool> Aktif { get; set; }
        public string Keterangan { get; set; }
        public string SectionID { get; set; }
        public string KelasID { get; set; }
        public string  NamaKamar { get; set; }
    }
}