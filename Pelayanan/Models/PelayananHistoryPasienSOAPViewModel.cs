﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pelayanan.Models
{
    public class PelayananHistoryPasienSOAPViewModel
    {
        [Display(Name = "No")]
        public int No { get; set; }

        [Display(Name = "No Reg")]
        public string NoReg { get; set; }

        [Display(Name = "Section ID")]
        public string SectionID { get; set; }

        [Display(Name = "Nomor")]
        public int Nomor { get; set; }

        [Display(Name = "No Bukti Tindakan")]
        public string NoBuktiTindakan { get; set; }

        [Display(Name = "Tanggal")]
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }

        [Display(Name = "Tanggal Input")]
        public System.DateTime TanggalInput { get; set; }
        public string TanggalInput_View { get; set; }

        [Display(Name = "Nama Dokter")]
        public string Nama_Supplier { get; set; }

        [Display(Name = "Dokter ID")]
        public string DokterID { get; set; }

        [Display(Name = "Keluhan Utama")]
        public string KeluhanUtama { get; set; }

        [Display(Name = "Riwayat Penyakit Sekarang")]
        public string RiwayatPenyakitSekarang { get; set; }

        [Display(Name = "Riwayat Penyakit Dahulu")]
        public string RiwayatPenyakitDahulu { get; set; }

        [Display(Name = "O")]
        public string O { get; set; }

        [Display(Name = "A")]
        public string A { get; set; }

        [Display(Name = "S")]
        public string S { get; set; }

        [Display(Name = "P")]
        public string P { get; set; }
    }
}